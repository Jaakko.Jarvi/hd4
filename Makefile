# CURRENT = peek_iterator
# CURRENT = lexing-tools
# CURRENT = constraint-builder
# CURRENT = parsing
# CURRENT = observing
# CURRENT = deferred
# CURRENT = graph-algorithms
# CURRENT = adjacency-list-graph
# CURRENT = computation-graph
CURRENT = constraint-system
# CURRENT = utilities
# CURRENT = graph-concepts

define INFOTEXT
Targets:
  help         - display this text
  all 		   - build all project files
  docs		   - generate documentation files
  flow	       - typecheck with flow (yarn flow check)
  sources      - extract JS-files from org-folder
  testfiles    - extract JS-test-files from org-folder
  lib          - Strip flow types from JS-files
  bundle       - bundle hotdrink for production
  bundle-dev   - bundle hotdrink for development
  test         - run tests
  test1	       - run one test (specified by the Makefile)
  debugtests   - debug the tests
  clean        - remove generated/build files
  hide         - hide generated files from git
  unhide       - make generated files visible to git
endef

export INFOTEXT

EMACS := emacs

.PHONY: clean test

help:
	@echo "$$INFOTEXT"

LIBRARY_ORGS = $(wildcard org/*.org)
# .tangled are just empty marker files for the make file
LIBRARY_TANGLED = $(LIBRARY_ORGS:.org=.tangled) # An `x.tangled` file for every `x.org` file
LIBRARY_HTMLS = $(LIBRARY_ORGS:.org=.html) # An `x.html` file for every `x.org` file

DOCS_ORGS = docs/tutorial/tutorial.org 
DOCS_TANGLED = $(DOCS_ORGS:.org=.tangled)
DOCS_HTMLS = $(DOCS_ORGS:.org=.html)

SOURCEDIR := src
LIBDIR := lib

# JavaScript files that are tangled from the org files
JS_TANGLED_SRC := $(shell grep "BEGIN_SRC .* :tangle .*" org/* | sed -rn 's/.* :tangle "\.\.\/(.*)" .*/\1/p' | grep src | sort | uniq | tr '\n' ' ')
JS_TANGLED_TEST := $(shell grep "BEGIN_SRC .* :tangle .*" org/* | sed -rn 's/.* :tangle "\.\.\/(.*)" .*/\1/p' | grep test | sort | uniq | tr '\n' ' ')

# JavaScript files that are just to be copied from org/ to src/
JS_COPIED_SRC := $(notdir $(wildcard org/*.js))

# Tangled and copied JavaScript files combined
JS_SRC := $(JS_TANGLED_SRC) $(JS_COPIED_SRC)
JS_TEST := $(JS_TANGLED_TEST)

# JavaScript files stripped of flow type annotations
JS_LIB := $(patsubst $(SOURCEDIR)/%.js, $(LIBDIR)/%.js, $(JS_SRC))

# Anything committed in certain directories
GEN_JS := $(shell git ls-files -- src | tr '\n' ' ')
GEN_TEST := $(shell git ls-files -- test | tr '\n' ' ')
GEN_DOCS := $(shell git ls-files -- docs | grep --invert-match \.org | tr '\n' ' ')
# ignore .org files in org-docs and subdirectories, they are not generated

# These are not committed, but they are generated
GEN_DIST := dist/hotdrink.js

$(shell mkdir -p dist lib test)

docs: $(LIBRARY_HTMLS) $(DOCS_HTMLS)

tangle: $(LIBRARY_TANGLED) $(DOCS_TANGLED)

sources: $(JS_SRC)

lib: $(JS_LIB)

testfiles: $(JS_TEST)

all: docs sources testfiles bundle-dev test

.SECONDEXPANSION:
# Below line means this: src/x.js: org/x.org
$(JS_TANGLED_SRC): org/$$(basename $$(@F)).org
	$(EMACS) --batch -l org-publish.el $< --eval "(org-tangle)"
	yarn prettier --write $@ # Prettify after tangling

.SECONDEXPANSION:
$(JS_COPIED_SRC): org/$$@
	cp $< src/
	yarn prettier --write $@ # Prettify after copying

test/%.js: org/%.org
	$(EMACS) --batch -l org-publish.el $< --eval "(org-tangle)"
	yarn prettier --write $@

%.tangled: %.org
	$(EMACS) --batch -l org-publish.el $< --eval "(org-tangle)"
	touch $@

%.html: %.org %.tangled
	$(EMACS) --batch -l org-publish.el $< --eval "(publish-org-to-html \".\")"

# The flow-typed folder has flow types for NPM-dependencies, such as Jest.
flow-typed: yarn.lock
	yarn flow-typed install
	touch flow-typed

yarn.lock: package.json
	yarn install --production=false
	touch yarn.lock

flow: $(JS_SRC) $(JS_TEST) yarn.lock flow-typed
	yarn flow check
	touch flow

lib/%.js: src/%.js yarn.lock flow test
	@echo [$^]
	yarn babel $< -d lib/ --source-maps

BUNDLE_FILE := dist/hotdrink.js

# Default bundle uses production as env to match webpacks defaults
bundle: export NODE_ENV=production
bundle: $(BUNDLE_FILE)

bundle-dev: export NODE_ENV=development
bundle-dev: $(BUNDLE_FILE)

$(BUNDLE_FILE): $(JS_LIB)
	@echo [$^]
	@echo NODE_ENV: $(NODE_ENV)
	yarn webpack --mode=$(NODE_ENV)
	touch $(BUNDLE_FILE)
	# ./generate_ts_types.sh

test1: flow
	yarn jest $(SOURCEDIR)/$(CURRENT).test.js

test: $(JS_TEST) flow
	yarn jest

debugtests: flow
	node --inspect-brk node_modules/.bin/jest --runInBand

GEN_TO_COMMIT = $(GEN_TEST)

# hide generated files from git until further notice
hide:
	git update-index --skip-worktree $(GEN_TO_COMMIT)

# unhide generated files
unhide:
	git update-index --no-skip-worktree $(GEN_TO_COMMIT)

clean:
	rm -rf *~
	rm -rf dist lib src test
	rm flow
