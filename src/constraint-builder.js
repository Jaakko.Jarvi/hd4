// @flow

export { ParseState, mkParseState, hdl, component };

import {
  ParseState,
  mkParseState,
  fail,
  failF,
  ok,
  ret,
  opt,
  pBind,
  pBind_,
  pSplice,
  oneOf,
  item,
  token,
  word,
  manyws,
  many,
  must,
  pTry,
  exactString,
  named,
  identifier,
  nat,
  pChar,
  pChars,
  failure,
  sepList,
  sat,
  lift,
  lift2,
  lift3,
  lift4,
  lift5,
  inBetween,
  parens,
  ignore,
  context,
  hdl,
  currentPos,
  eof,
} from "../src/parsing";

import { Position } from "../src/lexing-tools";
import type { ParseResult, Parser, SuccessParser } from "../src/parsing";

import * as util from "../src/utilities";
import * as cs from "../src/constraint-system";
import { hdconsole } from "../src/hdconsole";

type LEFT_type = 0;
type RIGHT_type = 1;
const LEFT: LEFT_type = 0;
const RIGHT: RIGHT_type = 1;

type LeftType<L> = { tag: LEFT_type, value: L };
type RightType<R> = { tag: RIGHT_type, value: R };
type Either<L, R> = LeftType<L> | RightType<R>;

function left<L>(v: L): LeftType<L> {
  return { tag: LEFT, value: v };
}
function right<R>(v: R): RightType<R> {
  return { tag: RIGHT, value: v };
}

// %checks do not work
export function isLeft<L, R>(e: Either<L, R>): boolean %checks {
  return e.tag === LEFT;
}
export function isRight<L, R>(e: Either<L, R>): boolean %checks {
  return e.tag === RIGHT;
}
function asLeft<L, R>(e: Either<L, R>): L {
  if (e.tag !== LEFT) throw "Either cast error: no value on left";
  return e.value;
}
function asRight<L, R>(e: Either<L, R>): R {
  if (e.tag !== RIGHT) throw e.value;
  return e.value;
}
type WithPosition<T> = { pos: Position, value: T };

type QualifiedParameterAST = WithPosition<{
  qualifiers: Array<WithPosition<string>>,
  parameterName: WithPosition<string>,
}>;

type MethodAST = {
  pos: Position,
  name: ?WithPosition<string>,
  ins: Array<QualifiedParameterAST>,
  outs: Array<QualifiedParameterAST>,
  code: WithPosition<Function>,
  promiseMask: Array<cs.MaskType>,
};

type ConstraintAST = {
  pos: Position,
  name: ?WithPosition<string>,
  variableNames: ?Set<string>,
  methods: Array<MethodAST>,
};

type VariableDeclarationAST = {
  pos: Position,
  name: string,
  isReference: boolean,
  initializer?: ?WithPosition<Function>,
};

type ComponentAST = {
  parent: ?ComponentAST,
  pos: Position,
  name: WithPosition<string>,
  variableDeclarations: Map<string, VariableDeclarationAST>,
  constraints: Array<ConstraintAST>,
  components: Array<ComponentAST>,
};
export function withPos<T>(p: Parser<T>): Parser<WithPosition<T>> {
  return lift2((cp, v) => ({ pos: cp, value: v }))(currentPos, p);
}
interface ScopeI<Key, Value> {
  get(k: Key): ?Value;
  has(k: Key): boolean;
}
class ComponentScope implements ScopeI<string, VariableDeclarationAST> {
  _component: ComponentAST;

  constructor(cmp: ComponentAST) {
    this._component = cmp;
  }

  get(key: string): ?VariableDeclarationAST {
    let m = this._component;
    while (m != null) {
      let v = m.variableDeclarations.get(key);
      if (v !== undefined) return v;
      else m = m.parent;
    }
  }
  has(key: string): boolean {
    return this.get(key) !== undefined;
  }
}
class SetScope<K> implements ScopeI<K, K> {
  _set: Set<K>;
  constructor(s: Set<K>) {
    this._set = s;
  }
  get(k: K): ?K {
    return this.has(k) ? k : undefined;
  }
  has(k: K): boolean {
    return this._set.has(k);
  }
}
export class TypeError extends Error {
  _msg: string;
  _pos: Position;

  constructor(m: string, p: Position) {
    super();
    this._msg = m;
    this._pos = p;
  }

  get message(): string {
    return this._pos.toString() + "\n" + this._msg;
  }

  set message(s: string): void {
    throw Error("Cannot set the name of ParseError");
  }
}
function checkQualifiers(
  qs: Array<WithPosition<string>>
): Either<TypeError, null> {
  if (qs.length > 1) {
    let seen: Set<string> = new Set();
    for (let q of qs) {
      if (seen.has(q.value))
        return left(
          new TypeError(`The same qualifier ${q.value} appears twice`, q.pos)
        );
      seen.add(q.value);
    }
  }
  return right(null);
}
function checkParameterList(
  pars: Array<QualifiedParameterAST>,
  scope: ScopeI<string, any>
): Either<TypeError, Set<string>> {
  let seenName: Set<string> = new Set();

  for (let par of pars) {
    // check qualifiers for duplicates
    let r = checkQualifiers(par.value.qualifiers);
    if (isLeft(r)) return left(asLeft(r));
    // check if variable name duplicate
    let pn = par.value.parameterName;
    if (!scope.has(pn.value)) {
      return left(new TypeError(`Undefined variable name ${pn.value}`, pn.pos));
    }
    if (seenName.has(pn.value))
      return left(
        new TypeError(
          `Parameter ${pn.value} appears twice in the same parameter list`,
          pn.pos
        )
      );

    seenName.add(pn.value);
  }
  return right(seenName);
}
function checkFirstMethod(
  m: MethodAST,
  scope: ScopeI<string, any>
): Either<TypeError, Set<string>> {
  let ins = checkParameterList(m.ins, scope);
  if (isLeft(ins)) return ins;

  let outs = checkParameterList(m.outs, scope);
  if (isLeft(outs)) return outs;

  return right(util.setUnion(asRight(ins), asRight(outs)));
}

function checkOtherMethod(
  m: MethodAST,
  expectedVariableNames: Set<string>
): Either<TypeError, null> {
  let scope = new SetScope(expectedVariableNames);
  let ins = checkParameterList(m.ins, scope);
  if (isLeft(ins)) return left(asLeft(ins));

  let outs = checkParameterList(m.outs, scope);
  if (isLeft(outs)) return left(asLeft(outs));

  let foundNames = util.setUnion(asRight(ins), asRight(outs));

  if (!util.setEquals(foundNames, expectedVariableNames)) {
    let missingVariables = Array.from(
      util.setDifference(expectedVariableNames, foundNames)
    ).join();
    return left(
      new TypeError(
        `All methods in the same constraint must use the same variables; this method does not refer to variable ` +
          missingVariables,
        m.pos
      )
    );
  }

  // FIXME: should test for method restriction
  return right(null);
}
function checkConstraint(
  c: ConstraintAST,
  scope: ScopeI<string, any>
): Either<TypeError, ConstraintAST> {
  if (c.methods.length === 0) {
    c.variableNames = new Set();
    return right(c);
  }
  let vs = checkFirstMethod(c.methods[0], scope);
  if (isLeft(vs)) return left(asLeft(vs));
  c.variableNames = asRight(vs);
  for (let i = 1; i < c.methods.length; ++i) {
    let r = checkOtherMethod(c.methods[i], asRight(vs));
    if (isLeft(r)) return left(asLeft(r));
  }
  return right(c);
}
function checkComponent(cmp: ComponentAST): Either<TypeError, ComponentAST> {
  for (let c of cmp.constraints) {
    let r = checkConstraint(c, new ComponentScope(cmp));
    if (isLeft(r)) return left(asLeft(r));
  }
  for (let c of cmp.components) {
    let r = checkComponent(c);
    if (isLeft(r)) {
      return left(asLeft(r));
    }
  }
  return right(cmp);
}
function jsCode(
  endMarkers: Array<string>,
  includeMarker: boolean = true
): Parser<string> {
  return (ps: ParseState) => {
    let s: string = "";
    while (true) {
      let nc: ?string = ps.peek();

      if (nc == null) {
        if (endMarkers.includes("")) return ok(s, s.length > 0);
        else return failF(() => ["<js code>"].concat(endMarkers), s.length > 0);
      }
      if (endMarkers.includes(nc)) {
        if (includeMarker) {
          item(ps);
          s += nc;
        }
        return ok(s);
      }
      if (["}", "]", ")"].includes(nc)) return fail(["not " + nc], true); // mismatched brace

      let r = oneOf(
        stringLiteral,
        templateLiteral,
        pBind(sat(isOpenBrace), (b) =>
          pBind(jsCode([closingBraceOf(b)], true), (c) => ret(b + c))
        ),
        item
      )(ps);
      if (!r.success) return r;
      s += r.value;
    }
    throw null; // dead code (needed to supress flowtype's complaint about mismatch in return type
  };
}
function isOpenBrace(c: string): boolean {
  return c == "(" || c == "{" || c == "[";
}
function closingBraceOf(c: string): string {
  switch (c) {
    case "(":
      return ")";
    case "{":
      return "}";
    case "[":
      return "]";
  }
  throw Error("HotDrink internal error: no closing brace defined for " + c);
}
function stringLiteral(ps: ParseState): ParseResult<string> {
  let r = oneOf(pChar("'"), pChar('"'))(ps);
  if (!r.success) return r;
  const quote = r.value,
    equote = "\\" + quote;
  let str = r.value;
  r = lift(
    (s, q) => str + s.join("") + q,
    many(
      oneOf(
        exactString("\\\\"),
        exactString(equote),
        sat((c) => c != quote)
      )
    ),
    pChar(quote)
  )(ps);
  r.consumed = true; // we know by now the parser has consumed
  // in case there is an error, it must be reading <eof> expecting quote
  return r;
}

function templateLiteral(ps: ParseState): ParseResult<string> {
  const quote = "`",
    equote = "\\" + quote;
  let r = pChar(quote)(ps);
  if (!r.success) return r;
  let str = r.value;
  while (true) {
    let r = must(
      oneOf(
        pChar(quote),
        exactString("${"),
        exactString("\\\\"),
        exactString(equote),
        item
      )
    )(ps); // throws if out of input

    if (r.value == quote) {
      str += quote;
      break;
    }

    if (r.value == "${") {
      str += "${";
      str += must(jsCode(["}"]))(ps).value;
      continue;
    }

    str += r.value;
  }
  return ok(str);
}
const block: Parser<string> = context(
  "<function body block>",
  token(
    lift(
      (open, code_and_close) => open + code_and_close,
      pChar("{"),
      jsCode(["}"])
    )
  )
);

const functionBody: Parser<string> = context(
  "<function body>",
  oneOf(
    block,
    pBind(token(jsCode([";"])), (body) => ret("return " + body))
  )
);
function pMethod(ps: ParseState): ParseResult<MethodAST> {
  function _met(
    pos: Position,
    name: ?WithPosition<string>,
    ins: Array<QualifiedParameterAST>,
    outs: Array<QualifiedParameterAST>,
    body: WithPosition<any>
  ) {
    // for each input argument, construct a mask that tells which qualifiers were used
    let promiseMask: Array<cs.MaskType> = ins.map((p) => {
      return util.foldl(
        (q1: string, q2: cs.MaskType) => orMasks(qualifier2mask(q1), q2),
        cs.maskNone,
        p.value.qualifiers.map((q) => q.value)
      );
    });

    const inparNames = ins.map((p) => p.value.parameterName.value);
    let code: WithPosition<Function>;
    if (typeof body.value === "string") {
      code = {
        pos: body.pos,
        value: new Function(inparNames.join(), body.value),
      };
    } else code = body; // body was a spliced expression, so body.value must be a Function already

    return { pos, name, ins, outs, code, promiseMask };
  }

  const bodyParser = withPos(
    oneOf<any>(
      lift((v, _) => v, pSplice, word(";")),
      pBind_(word("=>"), functionBody),
      block
    )
  );

  return context(
    "<method>",
    lift(
      _met,
      currentPos,
      opt(withPos(identifier), null),
      ignore(word("(")),
      qualifiedParameterList("*!"),
      ignore(word("->")),
      qualifiedParameterList(""),
      ignore(word(")")),
      bodyParser
    )
  )(ps);
}
function qualifier2mask(q: string): cs.MaskType {
  switch (q) {
    case "*":
      return cs.maskPromise;
    case "!":
      return cs.maskUpdate;
    default:
      return cs.maskNone;
  }
}

function orMasks(m1: cs.MaskType, m2: cs.MaskType): cs.MaskType {
  return ((m1: number) | (m2: number): any);
}
function qualifierList(
  qualifiers: string
): Parser<Array<WithPosition<string>>> {
  return many(token(withPos(pChars(qualifiers))));
}
function qualifiedParameterList(
  qualifiers: string = "*!"
): Parser<Array<QualifiedParameterAST>> {
  //
  return sepList(
    withPos(
      lift(
        (qs, pname) => ({ qualifiers: qs, parameterName: pname }),
        qualifierList(qualifiers),
        withPos(identifier)
      )
    ),
    word(",")
  );
}
function pVariables(
  ps: ParseState
): ParseResult<Array<VariableDeclarationAST>> {
  return inBetween(
    sepList(identifierWithOptionalInitializer, word(",")),
    word("var"),
    word(";")
  )(ps);
}
function identifierWithOptionalInitializer(
  ps: ParseState
): ParseResult<VariableDeclarationAST> {
  return pBind(currentPos, (pos) =>
    oneOf(
      lift(
        (name) => ({ pos, name, isReference: true }),
        ignore(word("&")),
        identifier
      ),
      lift(
        (name, initializer) => ({ pos, name, isReference: false, initializer }),
        identifier,
        opt(withPos(initializerExpression), undefined)
      )
    )
  )(ps);
}

function initializerExpression(ps: ParseState): ParseResult<any> {
  return pBind_(
    word("="),
    context(
      "<intitializer-expression>",
      pBind(jsCode([",", ";"], false), (e) =>
        ret(new Function("return " + e)())
      )
    )
  )(ps);
}
function pConstraint(ps: ParseState): ParseResult<ConstraintAST> {
  return context(
    "<constraint>",
    lift(
      (pos, name, methods) => ({ pos, name, variableNames: null, methods }),
      currentPos,
      ignore(word("constraint")),
      opt(withPos(identifier), null),
      ignore(word("{")),
      many(pMethod),
      ignore(word("}"))
    )
  )(ps);
}
const mkComponentAST = (pos, name, declarations) => {
  let variableDeclarations: Map<string, VariableDeclarationAST> = new Map();
  let constraints: Array<ConstraintAST> = [];
  let components: Array<ComponentAST> = [];
  let node = {
    parent: null,
    pos,
    name,
    variableDeclarations,
    constraints,
    components,
  };

  for (let decl of declarations) {
    switch (decl.tag) {
      case "variable":
        for (let vdecl of decl.value) {
          variableDeclarations.set(vdecl.name, vdecl);
        }
        break;
      case "constraint":
        constraints.push(decl.value);
        break;
      case "component":
        decl.value.parent = node;
        components.push(decl.value);
        break;
    }
  }
  return node;
};

const componentBody = many(
  oneOf(
    pBind(pVariables, (v) => ret({ tag: "variable", value: v })),
    pBind(pConstraint, (v) => ret({ tag: "constraint", value: v })),
    pBind(pComponent, (v) => ret({ tag: "component", value: v }))
  )
);

function topComponent(name: string): Parser<ComponentAST> {
  return (ps: ParseState): ParseResult<ComponentAST> => {
    return lift(
      mkComponentAST,
      currentPos,
      withPos(ret(name)),
      componentBody
    )(ps);
  };
}

function pComponent(ps: ParseState): ParseResult<ComponentAST> {
  return context(
    "<component>",
    lift(
      mkComponentAST,
      currentPos,
      ignore(word("component")),
      withPos(identifier),
      ignore(word("{")),
      componentBody,
      ignore(word("}"))
    )
  )(ps);
}
function component(strs: Array<string>, ...splices: Array<any>): cs.Component {
  let ps = hdl(strs, ...splices);
  let r = must(
    pBind_(manyws, oneOf(pComponent, topComponent(cs.freshComponentName())))
  )(ps);
  must(eof)(ps);
  return buildComponent(asRight(checkComponent(r.value)));
}
function buildMethod(mnode: MethodAST, vmap: Map<string, number>): cs.Method {
  return new cs.Method(
    vmap.size,
    mnode.ins.map((v) => (vmap.get(v.value.parameterName.value): any)),
    mnode.outs.map((v) => (vmap.get(v.value.parameterName.value): any)),
    mnode.promiseMask,
    mnode.code.value,
    mnode.name == undefined ? "" : mnode.name.value
  );
}

function buildAndAddConstraint(
  owner: cs.Component,
  cnode: ConstraintAST
): cs.Constraint {
  let variableNames = cnode.variableNames;
  if (variableNames == null)
    throw Error(
      "Constraint node must be type checked before it is used for building a constraint"
    );

  let name2index: Map<string, number> = new Map();
  let index = 0;

  for (let v of variableNames) name2index.set(v, index++);
  let ms: Array<cs.Method> = cnode.methods.map((m) =>
    buildMethod(m, name2index)
  );
  let vrefs: Array<cs.VariableReference<any>> = Array.from(
    name2index.keys(),
    (name) => (owner.getVariableReference(name): any)
  );
  let cspec = new cs.ConstraintSpec(ms);
  let name = cnode.name != null ? cnode.name.value : null;
  return owner.emplaceConstraint(name, cspec, vrefs, false);
}

function buildComponent(
  compNode: ComponentAST,
  owner?: ?cs.Component = null
): cs.Component {
  let comp = new cs.Component(compNode.name.value);
  if (owner != null) owner.addComponent(comp);
  for (let vdecl of compNode.variableDeclarations.values()) {
    if (vdecl.isReference) comp.emplaceVariableReference(vdecl.name);
    else {
      let init =
        vdecl.initializer != null ? vdecl.initializer.value : undefined;
      comp.emplaceVariable(vdecl.name, init);
    }
  }
  compNode.constraints.forEach((c) => buildAndAddConstraint(comp, c));
  compNode.components.forEach((c) => buildComponent(c, comp));
  return comp;
}
