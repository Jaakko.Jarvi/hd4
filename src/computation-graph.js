// [[file:../org/computation-graph.org::*Tangle][Tangle:2]]
// @flow

import * as util from "../src/utilities";
import * as hd from "../src/constraint-system";
import type {
  AdjacencyIteration,
  BidirectionalAdjacencyIteration,
} from "../src/graph-concepts.js";
import { hdconsole } from "../src/hdconsole";

class Deferred<T> {
  resolve: (T) => void;
  reject: (any) => void;

  _promise: Promise<T>;

  constructor() {
    this._promise = new Promise((r, j) => {
      this.resolve = (v) => {
        r(v);
      };
      this.reject = (e) => {
        j(e);
      };
    });
  }

  get promise(): Promise<T> {
    return this._promise;
  }
}
class TrackingPromise<T> {
  _promise: Promise<T>;
  _fulfillTracker: () => any;

  constructor(p: Promise<T>, tracker: () => any) {
    this._promise = p;
    this._fulfillTracker = tracker;
  }

  then(onFulfilled: ?Function, onRejected?: ?Function): Promise<T> {
    if (onFulfilled != null) this._fulfillTracker();
    return this._promise.then(onFulfilled, onRejected);
  }

  catch(onRejected: Function): Promise<T> {
    return this._promise.catch(onRejected);
  }
  finally(onFinally: Function): Promise<T> {
    return this._promise.finally(onFinally);
  }
}
const abandoned = {};

export class VariableActivation {
  static abandoned: {};
  _deferred: Deferred<any>;
  _pending: boolean;
  _variable: hd.Variable<any>;

  _outMethods: Set<MethodActivation>;
  _inMethod: ?MethodActivation;

  constructor(v: hd.Variable<any>) {
    this._pending = false;
    this._variable = v;
    this._deferred = new Deferred();
    this._outMethods = new Set();
    this._inMethod = null;
    util.mkRefCounted(this, (t) => {
      t.reject(VariableActivation.abandoned);
      t._inMethod = null;
      return;
    });
  }

  get name(): string {
    return this._variable.name;
  }
  get inMethod(): ?MethodActivation {
    return this._inMethod;
  }
  get outMethods(): Iterable<MethodActivation> {
    return this._outMethods;
  }
  outMethodsSize(): number {
    return this._outMethods.size;
  }
  addOutMethod(m: MethodActivation): void {
    this._outMethods.add(m);
  }
  setInMethod(m: MethodActivation): void {
    this._inMethod = m;
  }
  resolve(v: any): void {
    this._pending = false;
    return this._deferred.resolve(v);
  }
  reject(e: any): void {
    this._pending = false;
    return this._deferred.reject(e);
  }
  get promise(): Promise<any> {
    return this._deferred.promise;
  }
}
VariableActivation.abandoned = {};
export class MethodActivation {
  _f: Function;
  _mask: Array<hd.MaskType>;

  _ins: Array<VariableActivation>;
  _outs: Array<VariableActivation>;

  constructor(f: Function, promiseMask: Array<hd.MaskType>) {
    this._f = f;
    this._mask = promiseMask;
  }

  get ins(): Iterable<VariableActivation> {
    return this._ins;
  }
  get outs(): Iterable<VariableActivation> {
    return this._outs;
  }
  insSize(): number {
    return this._ins.length;
  }
  outsSize(): number {
    return this._outs.length;
  }

  async execute(
    ins: Array<VariableActivation>,
    outs: Array<VariableActivation>
  ): Promise<any> {
    this._ins = ins;
    this._outs = outs;
    outs.forEach((o) => o.setInMethod(this));

    // retain all inputs
    ins.forEach((i) => util.retain(i));

    // once _all_ output variable activations have been settled
    // (either by this method or by becoming abandoned), this method
    // activation has no use anymore; in particular it does not need
    // its input activations anymore, and it will release them.  We
    // use Promise.all to detect when all outputs have been settled
    // or rejected, but since Promise.all rejects immediately if any
    // of the promises it waits rejects, we do not wait for the output
    // promises directly, but rather for another set of promises
    // (oproms) that wrap the output promises and resolve regardless
    // whether the corresponding outputs reject or resolve.
    let oproms = outs.map((o) =>
      o.promise.then(
        (v) => null,
        (e) => null
      )
    );
    Promise.all(oproms).then(() => {
      ins.forEach((i) => {
        util.release(i);
      });
    });
    // FIXME: this can be replaced with Promise.allSettled(), once
    // it becomes supported

    let nonpromiseIns = ins.filter(
      (e, i) => (this._mask[i] & hd.maskPromise) === 0
    );
    // these are regular in-parameters that are awaited before passing into method's f

    let modifiedIns = ins.filter(
      (e, i) => (this._mask[i] & hd.maskUpdate) !== 0
    );
    // these are the inputs that are also outputs, and where there is aliasing between
    // the input and the output. Methods that perform this kind of modification can only
    // be executed when the inputs have reference count 1 (then all the other methods that
    // use those inputs have already finished (there can be at most one method that performs
    // such a modifying write)

    let mCount = modifiedIns.length;
    if (mCount > 0) {
      let def = new Deferred();
      let arr = [];
      for (let i = 0; i < modifiedIns.length; ++i) {
        arr.push(modifiedIns[i].name);
        util.setUniqueHandler(modifiedIns[i], (v) => {
          if (--mCount === 0) def.resolve(null);
        });
      }
      await def.promise;
      // now all possibly modifying variables have only one reference
    }

    let inValues: Array<any>;
    try {
      // await for all non-promise inputs
      inValues = await Promise.all(nonpromiseIns.map((v) => v.promise));
    } catch (e) {
      // if any non-promise input rejects, the wrapped f cannot be called, so
      // all outputs can be rejected
      outs.forEach((o) => o.reject("a method's input was rejected"));
      return;
    }

    // construct the wrapped f's input argument from promise inputs
    // and the resolved values of non-promise inputs
    let inArgs = [],
      vind: number = 0;

    for (let i = 0; i < ins.length; ++i) {
      if ((this._mask[i] & hd.maskPromise) !== 0) {
        inArgs[i] = new TrackingPromise(ins[i].promise, () =>
          ins[i].addOutMethod(this)
        );
      } else {
        ins[i].addOutMethod(this);
        inArgs[i] = inValues[vind++];
      }
    }
    let r;
    try {
      r = await this._f(...inArgs); // in case of one return, result can be
      // a promise, we await for it here to
      // avoid unhandled rejection. (This also
      // allows a method that returns a
      // promise that resolves to an array)
    } catch (e) {
      // this error is from f itself; therefore we reject all outputs
      // with the reason e
      outs.forEach((o) => o.reject(e));
      return;
    }

    // if method has more than one output, f should return an array
    if (outs.length > 1) {
      console.assert(
        Array.isArray(r) && outs.length == r.length,
        `Method result shoud be an array of ${outs.length} elements`
      );
      for (let i = 0; i < r.length; ++i) outs[i].resolve(r[i]);
      // r[i] can be a promise or a value
    } else {
      // only one output; result of f is not expected to be wrapped in a singleton array
      outs[0].resolve(r);
    }
  }
}
type eg_vertex_descriptor = VariableActivation | MethodActivation;

class EvaluationGraph
  implements BidirectionalAdjacencyIteration<eg_vertex_descriptor>
{
  adjacentOutVertices(v: eg_vertex_descriptor): Iterable<eg_vertex_descriptor> {
    if (v instanceof VariableActivation) return v.outMethods;
    else return ((v: any): MethodActivation).outs;
  }

  outDegree(v: eg_vertex_descriptor): number {
    if (v instanceof VariableActivation) return v.outMethodsSize();
    else return ((v: any): MethodActivation).outsSize();
  }

  adjacentInVertices(v: eg_vertex_descriptor): Iterable<eg_vertex_descriptor> {
    if (v instanceof VariableActivation)
      return v.inMethod != null ? [v.inMethod] : [];
    else return ((v: any): MethodActivation).ins;
  }

  inDegree(v: eg_vertex_descriptor): number {
    if (v instanceof VariableActivation) return v.inMethod != null ? 1 : 0;
    else return ((v: any): MethodActivation).insSize();
  }
}
// Tangle:2 ends here
