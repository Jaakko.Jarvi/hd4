// @flow
import type {
  Graph,
  VertexListIteration,
  EdgeListIteration,
  AdjacencyIteration,
  ModifiableGraph,
  AssignableLabels,
} from "../src/graph-concepts.js";

import { iteratorToIterable } from "../src/utilities.js";
import assert from "assert";
export type EdgeDescriptorT<V> = [V, V];

export type vertex_descriptor = number;
export type edge_descriptor = EdgeDescriptorT<vertex_descriptor>;
type Connections<V> = {
  outs: Set<V>,
  ins: Set<V>,
};
class AdjacencyList<VertexDescriptor, VertexLabel, EdgeLabel>
  implements
    VertexListIteration<VertexDescriptor>,
    EdgeListIteration<EdgeDescriptorT<VertexDescriptor>>,
    AdjacencyIteration<VertexDescriptor>,
    AssignableLabels<
      VertexDescriptor,
      EdgeDescriptorT<VertexDescriptor>,
      VertexLabel,
      EdgeLabel
    >,
    ModifiableGraph<
      VertexDescriptor,
      EdgeDescriptorT<VertexDescriptor>,
      VertexLabel,
      EdgeLabel
    >
{
  _vertices: Map<VertexDescriptor, Connections<VertexDescriptor>>;
  _vlabels: Map<VertexDescriptor, VertexLabel>;
  _elabels: Map<VertexDescriptor, Map<VertexDescriptor, EdgeLabel>>;

  constructor() {
    this._vertices = new Map();
    this._vlabels = new Map();
    this._elabels = new Map();
  }
  _vertex(v: VertexDescriptor): Connections<VertexDescriptor> {
    let s = this._vertices.get(v);
    if (s === undefined) throw "no vertex " + String(v);
    else return s;
  }
  source(e: EdgeDescriptorT<VertexDescriptor>): VertexDescriptor {
    return e[0];
  }

  target(e: EdgeDescriptorT<VertexDescriptor>): VertexDescriptor {
    return e[1];
  }

  edge(
    source: VertexDescriptor,
    target: VertexDescriptor
  ): ?EdgeDescriptorT<VertexDescriptor> {
    if (this._vertex(source).outs.has(target)) return [source, target];
    else return undefined;
  }
  hasEdge(source: VertexDescriptor, target: VertexDescriptor): boolean {
    return this._vertex(source).outs.has(target);
  }

  hasVertex(v: VertexDescriptor): boolean {
    return this._vertices.has(v);
  }

  vertexLabel(v: VertexDescriptor): ?VertexLabel {
    return this._vlabels.get(v);
  }

  edgeLabel(e: EdgeDescriptorT<VertexDescriptor>): ?EdgeLabel {
    let t = this._elabels.get(e[0]);
    if (t) return t.get(e[1]);
    else return undefined;
  }
  vertices(): Iterable<VertexDescriptor> {
    return iteratorToIterable(this._vertices.keys());
  }
  edges(): Iterable<EdgeDescriptorT<VertexDescriptor>> {
    let self = this;
    return (function* () {
      for (let s of self.vertices()) {
        for (let t of self.adjacentOutVertices(s)) yield [s, t];
      }
    })();
  }
  adjacentOutVertices(v: VertexDescriptor): Iterable<VertexDescriptor> {
    let s = this._vertices.get(v);
    if (s !== undefined) return s.outs;
    else throw `vertex ${String(v)} does not exist`;
  }
  adjacentInVertices(v: VertexDescriptor): Iterable<VertexDescriptor> {
    let s = this._vertices.get(v);
    if (s !== undefined) return s.ins;
    else throw `vertex ${String(v)} does not exist`;
  }

  outDegree(v: VertexDescriptor): number {
    return this._vertex(v).outs.size;
  }
  inDegree(v: VertexDescriptor): number {
    return this._vertex(v).ins.size;
  }

  // GrowableGraph methods
  addEdge(
    src: VertexDescriptor,
    target: VertexDescriptor,
    l: ?EdgeLabel
  ): EdgeDescriptorT<VertexDescriptor> {
    this._vertex(src).outs.add(target);
    this._vertex(target).ins.add(src);
    if (l != null) this._setEdgeLabel(src, target, l);
    return [src, target];
  }
  addVertex(l: ?VertexLabel): VertexDescriptor {
    throw "addVertex not supported by AdjacencyList (use addVertexDescriptor)";
  }

  addVertexDescriptor(v: VertexDescriptor, l: ?VertexLabel): VertexDescriptor {
    if (!this._vertices.has(v))
      this._vertices.set(v, { outs: new Set(), ins: new Set() });
    if (l != null) this._vlabels.set(v, l);
    return v;
  }

  removeEdgeBetween(s: VertexDescriptor, t: VertexDescriptor): boolean {
    let v1 = this._vertices.get(s);
    if (v1 === undefined) throw `vertex ${String(s)} does not exist`;
    let v2 = this._vertices.get(t);
    if (v2 === undefined) throw `vertex ${String(t)} does not exist`;

    if (!v1.outs.delete(t)) return false;

    // remove incoming too
    v2.ins.delete(s); // we assume that the incoming info exists

    // remove labels
    let el = this._elabels.get(s);
    if (el != null) el.delete(s);
    return true;
  }
  removeEdge(e: EdgeDescriptorT<VertexDescriptor>): boolean {
    return this.removeEdgeBetween(this.source(e), this.target(e));
  }
  removeVertex(v: VertexDescriptor): boolean {
    if (!this._vertices.has(v)) return false;
    for (let s of this.adjacentOutVertices(v)) {
      this.removeEdgeBetween(v, s);
    }
    for (let s of this.adjacentInVertices(v)) {
      this.removeEdgeBetween(s, v);
    }
    // if s == v (selfloop), then selfloop is already deleted by the first loop

    // FIXME: could save a bit by only removing s from the .outs and .ins
    // of the adjacent in and out vertices, the .outs and .ins
    // of v disappear when v deleted
    this._elabels.delete(v);
    this._vlabels.delete(v);
    return this._vertices.delete(v); // v guaranteed to exist, always returns true here
  }
  setVertexLabel(v: VertexDescriptor, l: VertexLabel): void {
    this._vlabels.set(v, l);
  }
  setEdgeLabel(e: EdgeDescriptorT<VertexDescriptor>, l: EdgeLabel): void {
    return this._setEdgeLabel(this.source(e), this.target(e), l);
  }
  _setEdgeLabel(s: VertexDescriptor, t: VertexDescriptor, l: EdgeLabel): void {
    let el = this._elabels.get(s);
    if (el === undefined) {
      // edge label map not yet created
      el = new Map();
      this._elabels.set(s, el);
    }
    el.set(t, l);
  }
}
class AdjacencyListOpaqueDescriptors<
  VertexLabel,
  EdgeLabel
> extends AdjacencyList<vertex_descriptor, VertexLabel, EdgeLabel> {
  _fresh: vertex_descriptor;

  constructor() {
    super();
    this._fresh = 0;
  }

  addVertex(l: ?VertexLabel): vertex_descriptor {
    let v = this._fresh++;
    this._vertices.set(v, { outs: new Set(), ins: new Set() });
    if (l != null) this._vlabels.set(v, l);
    return v;
  }

  addVertexDescriptor(
    v: vertex_descriptor,
    l: ?VertexLabel
  ): vertex_descriptor {
    throw "addVertexDescriptor not supported by AdjacencyListOpaqueDescriptors (use addVertex)";
  }
}

export { AdjacencyList, AdjacencyListOpaqueDescriptors };
