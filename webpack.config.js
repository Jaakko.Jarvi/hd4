const path = require('path');

module.exports = {
  entry: './lib/hotdrink.js',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'dist'),    
    filename: 'hotdrink.js',
    library: {
      name: 'hd',
      type: 'umd',
      umdNamedDefine: true,
    }
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: { 
          presets: [] 
        }
      }
    ]
  }
};
