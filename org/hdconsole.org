#+TITLE: A simple wrapper over Console

#+INCLUDE: setup.org

* Tangle                    

The code in this document is tangled to ~hdconsole.js~, whose imports
are shown below.

#+BEGIN_SRC js :noweb strip-export :tangle "../src/hdconsole.js" :comments no
  // @flow

  import * as util from "./utilities"

  <<code>>
#+END_SRC

* HdConsole

The ~HdConsole~ class replicates basic functionality of ~Console~, and
adds methods to turn logging on and off.

#+BEGIN_SRC js :noweb-ref code
  export class HdConsole {  
    _name: string;
    log: Function;
    warn: Function;
    error: Function;
    assert: Function;

    constructor(name: string) { this._name = name; this.off(); }

    off() { 
      this.log = (...args) => {};
      this.warn = (...args) => {};
      this.error = (...args) => {};
      this.assert = (...args) => {};
    }
    errorsOnly() { 
      this.off(); 
      this.error = console.error;
      this.assert = console.assert;
    }
    on() { 
      this.log = console.log;
      this.warn = console.warn;
      this.error = console.error;
      this.assert = console.assert;
    }
  }

  export var hdconsole: HdConsole = new HdConsole("HotDrink");
#+END_SRC

* HotDrink exceptions

The ~HdError~ is HotDrink's custom error type.

#+BEGIN_SRC js :noweb-ref code
  export class HdError extends Error {
    constructor(msg: string, ...params: Array<any>) {
      super(msg, ...params);

      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, HdError);
      }

      this.name = 'HdError';
    }
  }
#+END_SRC


* File local variables                                     :ARCHIVE:noexport:
  
# Local Variables:
# mode: org
# org-html-postamble: nil
# org-babel-use-quick-and-dirty-noweb-expansion: t
# End:
