// [[file:../org/observable.org::*Tangle][Tangle:1]]
// @flow

// [[[[file:~/Git/hd4/org/observable.org::*Tangle][Tangle]]][]]
export interface Observer<T> {
  +start?: (subscription: Subscription) => void;
  +next?: (value: T) => void;
  +error?: (errorValue: any) => void;
  +complete?: () => void;
}
// ends here
// [[[[file:~/Git/hd4/org/observable.org::*Tangle][Tangle]]][]]
export interface Subscription {
  unsubscribe(): void;
  +closed: boolean;
  // get closed(): boolean; (flowtype does not support getters in interfaces)
}
// ends here
// [[[[file:~/Git/hd4/org/observable.org::*Tangle][Tangle]]][]]
export type SubscriberFunction<T> = (
  SubscriptionObserver<T>
) => ((void) => void) | Subscription;

// create an observer from separate functions
export function mkObserver<T>(
  obs: Observer<T> | ((T) => void),
  err?: (any) => void,
  complete?: () => void
): Observer<T> {
  if (typeof obs === "function") {
    let observer = {};
    observer.next = obs;
    if (err != null) observer.error = err;
    if (complete != null) observer.complete = complete;
    return observer;
  } else {
    return obs;
  }
}

export class Observable<T> {
  _subscriber: Function;

  constructor(subscriber: Function) {
    this._subscriber = subscriber;
  }

  subscribe(
    obs: Observer<T> | ((T) => void),
    err?: (any) => void,
    complete?: () => void
  ): Subscription {
    let observer = mkObserver(obs, err, complete);
    let subscription = new HdSubscription(observer);

    try {
      if (observer.start != null) observer.start(subscription);
    } catch (e) {
      // FIXME: HostReportError(e)
    }
    if (subscription.closed) return subscription;

    let subscriptionObserver = new SubscriptionObserver(subscription);

    try {
      let cleanup = this._subscriber(subscriptionObserver);
      if (typeof cleanup === "object") cleanup = () => cleanup.unsubscribe();
      // must be a Subscription object
      subscription._cleanup = cleanup;
    } catch (e) {
      subscriptionObserver.error(e);
    }
    if (subscription.closed) subscription.cleanup();
    return subscription;
  }

  // FIXME Implement later
  // [Symbol.observable]() : Observable;

  static of(...items: Array<T>): Observable<T> {
    return new Observable((observer) => {
      for (let item of items) {
        observer.next(item);
        if (observer.closed) return;
      }
      observer.complete();
    });
  }
  // FIXME Implement later
  // static from(obs: Observable<T> | Iterable<T>) : Observable<T> {
  //
  // }

  filter(p: (T) => boolean): Observable<T> {
    return new Observable((observer) => {
      let o = mkObserver(observer);
      if (o.next == null) o = { next: (e) => {} };
      let s = this.subscribe((e) => {
        if (p(e) == true) (o: any).next(e);
      });
      return s;
    });
  }
  map<U>(f: (T) => U): Observable<U> {
    return new Observable((observer) => {
      let o = mkObserver(observer);
      if (o.next == null) o = { next: (e) => {} };
      let s = this.subscribe((e) => {
        let o = mkObserver(observer);
        (o: any).next(f(e));
      });
      return s;
    });
  }
}

export class HdSubscription<T> implements Subscription {
  _observer: ?Observer<T>;
  _cleanup: any;
  constructor(observer: Observer<T>) {
    this._observer = observer;
    this._cleanup = undefined;
  }
  unsubscribe(): void {
    this._observer = undefined;
    this.cleanup();
  }
  get closed(): boolean {
    return this._observer === undefined;
  }
  cleanup(): void {
    const cUp = this._cleanup;
    if (cUp === undefined) return;
    this._cleanup = undefined;
    try {
      cUp();
    } catch (e) {
      /* FIXME: HostReportErrors(e) */
    }
  }
}

export class SubscriptionObserver<T> {
  _subscription: HdSubscription<T>;
  constructor(s: HdSubscription<T>) {
    this._subscription = s;
  }
  get closed(): boolean {
    return this._subscription.closed;
  }

  next(value: T): void {
    let s = (this._subscription: any); // appeasing flowtype
    if (s.closed) return;
    if (s._observer.next != null) {
      try {
        s._observer.next(value);
      } catch (e) {
        /* FIXME: HostReportErrors(e) */
      }
    }
  }
  error(errorValue: any): void {
    let s = (this._subscription: any); // appeasing flowtype
    if (s.closed) return; // FIXME, why not test this.closed ?
    if (s._observer.error != null) {
      try {
        s._observer.error(errorValue);
      } catch (e) {
        /* FIXME: HostReportErrors(e) */
      }
    }
    this._subscription.cleanup();
  }
  complete(): void {
    let s = (this._subscription: any); // appeasing flowtype
    if (s.closed) return;
    if (s._observer.complete != null) {
      try {
        s._observer.complete();
      } catch (e) {
        /* FIXME: HostReportErrors(e) */
      }
    }
    this._subscription.cleanup();
  }
}
// ends here
// [[[[file:~/Git/hd4/org/observable.org::*Tangle][Tangle]]][]]
export class Subject<T> extends Observable<T> {
  _subjectState: {
    observers: Set<SubscriptionObserver<T>>,
    done: boolean,
  };

  constructor(f?: Function = (o) => {}) {
    let state = {
      observers: new Set(),
      done: false,
    };
    super((o) => {
      if (!state.done) state.observers.add(o);
      f(o);
      return () => state.observers.delete(o); // this is a no-op if done==true
    });
    this._subjectState = state;
  }

  sendNext(v: T): void {
    if (this._subjectState.done) return;
    for (const obs of this._subjectState.observers) obs.next(v);
  }
  sendError(e: any): void {
    if (this._subjectState.done) return;
    this._subjectState.done = true;
    for (const obs of this._subjectState.observers) obs.error(e);
  }
  sendComplete(): void {
    if (this._subjectState.done) return;
    this._subjectState.done = true;
    for (const obs of this._subjectState.observers) obs.complete();
  }

  get nObservers(): number {
    return this._subjectState.observers.size;
  }
}
// ends here
// [[[[file:~/Git/hd4/org/observable.org::*Tangle][Tangle]]][]]
export class ReplaySubject<T> extends Subject<T> {
  _replaySubjectState: {
    past: Array<T>,
    pastLimit: number,
    isCompleted: boolean,
    isError: boolean,
    error?: any,
  };

  constructor(limit?: number = 1) {
    let state = {
      past: [],
      pastLimit: limit,
      isCompleted: false,
      isError: false,
    };
    super((o) => {
      for (let v of state.past) o.next(v);
      if (state.isError) o.error(state.error);
      else if (state.isCompleted) o.complete();
    });
    this._replaySubjectState = state;
  }
  sendNext(v: T): void {
    this._replaySubjectState.past.push(v);
    if (
      this._replaySubjectState.past.length > this._replaySubjectState.pastLimit
    ) {
      this._replaySubjectState.past.shift();
    }
    super.sendNext(v);
  }
  sendError(e: T): void {
    this._replaySubjectState.isError = true;
    this._replaySubjectState.error = e;
    super.sendError(e);
  }
  sendComplete(): void {
    this._replaySubjectState.isCompleted = true;
    super.sendComplete();
  }
}

// ends here
// [[[[file:~/Git/hd4/org/observable.org::*Tangle][Tangle]]][]]
export class BehaviorSubject<T> extends ReplaySubject<T> {
  constructor(v: T) {
    super(1);
    this._replaySubjectState.past.push(v);
  }
}
// ends here
// Tangle:1 ends here
