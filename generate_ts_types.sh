#!/bin/zsh

out_file=./dist/hotdrink.d.ts
folder=./types/ts

files=(
    ./graph-concepts.d.ts
    ./utilities.d.ts
    ./adjacency-list-graph.d.ts
    ./computation-graph.d.ts
    ./constraint-builder.d.ts
    ./constraint-system/variable.d.ts
    ./constraint-system/constraint.d.ts
    ./constraint-system/method.d.ts
    ./constraint-system/constraint-system.d.ts
    ./constraint-system/component.d.ts
    ./constraint-system/simple-planner.d.ts
    ./constraint-system/constraint-spec.d.ts
    ./constraint-system/variable-reference.d.ts
    ./constraint-system/constraint-system-util.d.ts
    ./constraint-system/solution-graph.d.ts
    ./graph-algorithms.d.ts
    ./hdconsole.d.ts
    ./hotdrink.d.ts
    ./lexing-tools.d.ts
    ./observable.d.ts
    ./parsing.d.ts
    ./peek-iterator.d.ts
    )

# shellcheck disable=SC2207
expected_files=($(cd $folder && find . -name "*.d.ts"))
expected_size=${#expected_files[@]}
actual_size=${#files[@]}

# Simplistic test to make sure we are in sync (does not work on file rename)
test "$expected_size" -ne "$actual_size" \
    && echo -e "ERROR bad size! Found $expected_size files on disk, but $actual_size files are defined\n\nDefined:\n${files[*]}\n\nOn disk:\n${expected_files[*]}" && exit 1

rm -f $out_file

for file in "${files[@]}"; do
    {
      echo -e "\n\n//////////////////////////////////////////////////////////////\n//"
      echo -e "// File $file\n//"
      echo -e "//////////////////////////////////////////////////////////////\n\n"
      cat $folder/"$file"
    } >> $out_file
done;

# Remove all import and export statements, they are not needed in the bundled definition
sed '/^ *import.*/d' $out_file | \
  sed '/^ *export *[{*].*from.*$/d' > $out_file.tmp && \
  mv $out_file.tmp  $out_file

