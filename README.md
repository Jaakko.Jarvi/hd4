# HotDrink

## Install HotDrink in an NPM project

```sh
npm install --save hotdrink
```

or with yarn:

```sh
yarn add hotdrink
```

## Import as module

Hotdrink is exported as an [UMD (Universal Module Definition)](https://github.com/umdjs/umd).

You can import HotDrink using ECMAScript's `import`:

```typescript
// main.js
import { ConstraintSystem, component } from "hotdrink";

let system = new ConstraintSystem();

let comp = component`
    var a = 2, b;
    
    constraint {
      (a -> b) => a*2;
    }
  `
system.addComponent(comp)

// Print the current value of b (should be undefined)
// and a new value whenever the variable changes
comp.vs.b.value.subscribeValue(v => console.log('Value of b: ' + v));

system.update(); // This should print 'Value of b: 4'
```

Running the file outputs the following.

```sh
$ node main.js
Value of b: undefined
Value of b: 4
```

Note that node uses CommonJS-modules by default (uses `module.exports` and `require` for exporting and importing), so you'll have to add `"type": "module"` in your `package.json` (works for node >= v15.0.0) if you're not using a bundler such as WebPack.

## Use in website with WebPack

An easy way to develop a website with HotDrink is to use the WebPack Module Bundler.
WebPack bundles all imported modules into one JavaScript-file and injects it in your HTML file.

1. Initialise an NPM/yarn-project (`npm init -y`) in an empty folder
2. Install `webpack` and `webpack-cli` as developer dependencies (`npm install --save-dev webpack webpack-cli`).
3. Run `npx webpack-cli init` to use the default WebPack-template. Append `--force` at the end to skip all the questions asked by the command-line tool (ES6 or TypeScript, CSS or SCSS, etc.).
4. Install `hotdrink` as a dependency (`npm install --save hotdrink`) and import it as a module in your JavaScript-files.

To start a development server, run `npx webpack serve`.

## Documentation

The org-files serves as documentation for this project.

Run `yarn && make docs` to generate standalone HTML files from the org-files.

## Local Development

Prerequisites: Yarn and Emacs with `htmlize` installed (Emacs is only needed to process the org-files).

To install the `htmlize` package: Start Emacs (preferably not in the terminal), press `Alt/Option + x`, type `list-packages` and press Enter, find and click `htmlize`, and click the install button.

The pipeline for the source files is as follows:

```
org/*.org ---> src/*.js
org/*.org ---> test/*.test.js
           ^tangled

org/.js   ---> src/*.js
           ^copied

src/*.js  ---> lib/*.js
           ^stripped from flowtype annotations

lib/*.js  ---> dist/hotdrink.js
           ^bundled with webpack
```

To setup the repository on your computer, run

```sh
$ yarn && make all
```

You can also run `make help` for more options.

You can add the local library to a local NPM project by running either of these commands in your project:

```sh
$ npm install --save /path/to/this/folder # Using NPM
# or
$ yarn add /path/to/this/folder # Using Yarn
```

You can also bundle the library to a single, web-ready, JavaScript file with `make bundle-dev` (included in `make all`). The generated file can be copied (or symlinked) into your web-project folder and linked directly to in your HTML-file.

```html
<!-- index.html -->
<!-- ... -->

<script src="hotdrink/hotdrink.js"></script>
<script src="myScript.js"></script>

<!-- ... -->
```

This exposes a global variable `hd` that contains all exposed classes and functions from HotDrink.

```javascript
// myScript.js

function bind(inputElement, variable) {
  inputElement.addEventListener('change', event => {
    variable.set(parseInt(event.target.value));
  });
  variable.subscribeValue(v => {
    inputElement.value = v;
  })
}

// Notice that HotDrink is implicitly imported
const system = hd.defaultConstraintSystem;
const comp = hd.component`
    component comp {
        var c = 1, f;
        constraint {
            (f -> c) => (f - 31) / 1.8;
            (c -> f) => (c * 1.8) + 31;
        }
    }
`;
system.addComponent(comp);

bind(document.getElementById("cel"), comp.vs.c.value);
bind(document.getElementById("fahr"), comp.vs["f"].value);
system.update();
```
