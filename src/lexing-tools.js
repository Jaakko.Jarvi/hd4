// [[file:../org/lexing-tools.org::*Tangle][Tangle:1]]
// @flow

// [[[[file:~/Git/hd4/org/lexing-tools.org::*Tangle][Tangle]]][]]
export class Position {
  _pos: number; // 0-indexed position on the entire input string
  _row: number; // 1-indexed
  _col: number; // 0-indexed
  _source: ?string; // name of a source (file) the position refers to

  constructor(pos: number, row: number, col: number, source?: string) {
    this._pos = pos;
    this._row = row;
    this._col = col;
    if (source !== undefined) this._source = source;
  }

  get pos(): number {
    return this._pos;
  }
  get row(): number {
    return this._row;
  }
  get col(): number {
    return this._col;
  }
  get source(): string {
    return this._source != null ? this._source : "-";
  }
  toString(): string {
    return this.source + ":" + this.row + ":" + this.col;
  }
  advance(str: string): void {
    for (let i = 0; i < str.length; ++i) {
      if (str[i] === "\n") {
        this._row++;
        this._col = 0;
      } else {
        this._col++;
      }
      this._pos++;
    }
  }
  clone(): Position {
    let p = new Position(this.pos, this.row, this.col);
    if (this.hasOwnProperty("_source")) p._source = this._source;
    // optional parameters do not accept null,
    // therefore we set _source after construction
    // We only set it if the property exists in the source;
    //   otherwise we might get _source to exist but be undefined in target,
    //   and then source and target would not compare (deep)equal
    return p;
  }
}
// ends here
// [[[[file:~/Git/hd4/org/lexing-tools.org::*Tangle][Tangle]]][]]
export function isWhitespace(c: string): boolean {
  switch (c) {
    case " ":
    case "\t":
    case "\r":
    case "\n":
      return true;
    default:
      return false;
  }
}

export function isDigit(c: string): boolean {
  return c >= "0" && c <= "9";
}

export function isAlpha(c: string): boolean {
  return (c >= "a" && c <= "z") || (c >= "A" && c <= "Z");
}

export function isAlphaPlus(c: string): boolean {
  return isAlpha(c) || c === "_" || c === "$";
}
export function isAlphaNum(c: string): boolean {
  return isAlphaPlus(c) || isDigit(c);
}
// ends here
// Tangle:1 ends here
