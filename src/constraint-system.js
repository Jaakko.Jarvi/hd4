// @flow

import type { AdjacencyIteration } from "./graph-concepts";
import { reverseTopoSortFrom } from "./graph-algorithms";
import { VariableActivation, MethodActivation } from "./computation-graph";
import { hdconsole } from "./hdconsole";
import * as Obs from "./observable";
import * as util from "./utilities";

export type MaskType = 0 | 1 | 2 | 3;
export const maskNone = 0;
export const maskPromise = 1;
export const maskUpdate = 2;
export const maskPromiseUpdate = maskPromise | maskUpdate;

export class Method {
  _name: string;

  _ins: Set<number>;
  _outs: Set<number>;
  _nonOuts: Set<number>;

  code: Function;

  _promiseMask: Array<MaskType>;

  constructor(
    nvars: number,
    ins: Iterable<number>,
    outs: Iterable<number>,
    promiseMask: Iterable<MaskType>,
    code: Function,
    name?: string
  ) {
    this._ins = new Set(ins);
    this._outs = new Set(outs);

    // compute _nonOuts
    let indices: Array<?number> = Array.from(Array(nvars).keys()); // [0, 1, ..., nvars-1]
    this._outs.forEach((i) => (indices[i] = null)); // mark with null the indices that are outputs
    this._nonOuts = new Set();
    for (let e of indices) {
      if (e != null) this._nonOuts.add(e);
    }
    this._promiseMask = Array.from(promiseMask);
    hdconsole.assert(
      this._ins.size == this._promiseMask.length,
      "Number of input arguments and length of promise mask must be the same"
    );

    this.code = code;
    if (name != null) this._name = name;
  }
  get name(): string {
    return this._name == null ? "" : this._name;
  }
  get nvars(): number {
    return this._outs.size + this._nonOuts.size;
  }

  validIndex(i: number): boolean {
    return i >= 0 && i < this.nvars;
  }
  ins(): Iterable<number> {
    return this._ins;
  }
  outs(): Iterable<number> {
    return this._outs;
  }
  nonOuts(): Iterable<number> {
    return this._nonOuts;
  }
  vIns<T>(vs: Array<T>): Iterable<T> {
    return util.map((i) => vs[i], this._ins);
  }
  vOuts<T>(vs: Array<T>): Iterable<T> {
    return util.map((i) => vs[i], this._outs);
  }
  vNonOuts<T>(vs: Array<T>): Iterable<T> {
    return util.map((i) => vs[i], this._nonOuts);
  }
  nIns(): number {
    return this._ins.size;
  }
  nOuts(): number {
    return this._outs.size;
  }
  nNonOuts(): number {
    return this._nonOuts.size;
  }
  isIn(i: number): boolean {
    return this._ins.has(i);
  }
  isOut(i: number): boolean {
    return this._outs.has(i);
  }
  isNonOut(i: number): boolean {
    return this._nonOuts.has(i);
  }
  prettyPrint(vrefs: Array<VariableReference<any>>): string {
    let s = this.name + "(";
    for (const i of this._nonOuts) s += vrefs[i].name + " ";
    s += "->";
    for (const i of this._outs) s += " " + vrefs[i].name;
    s += ");";
    return s;
  }
}
export class ConstraintSpec {
  _methods: Set<Method>;
  _v2ins: Array<Array<Method>>;
  _nvars: number;

  constructor(methods: Iterable<Method>) {
    this._methods = new Set(methods);
    hdconsole.assert(
      this._methods.size > 0,
      "Constraint specification must have at least one method."
    );

    const nvars = (util.first(this._methods): any).nvars;
    this._nvars = nvars;

    this._v2ins = [];

    for (let i = 0; i < nvars; ++i) this._v2ins.push([]);

    for (let m of this._methods) {
      hdconsole.assert(
        m.nvars === nvars,
        `All methods in constraint specification must have the same number of variables`
      );
      for (let i of m.outs()) {
        this._v2ins[i].push(m);
      }
    }
  }
  get nvars(): number {
    return this._nvars;
  }

  ins(vIndex: number): Iterable<Method> {
    return this._v2ins[vIndex];
  }
  insSize(vIndex: number): number {
    return this._v2ins[vIndex].length;
  }

  methods(): Iterable<Method> {
    return this._methods;
  }
  hasMethod(m: Method): boolean {
    return this._methods.has(m);
  }
  prettyPrint(prefix: string, vrefs: Array<VariableReference<any>>): string {
    let s = "";
    for (let m of this._methods) {
      s += prefix + m.prettyPrint(vrefs) + "\n";
    }
    return s;
  }
}
export class VariableReference<T> extends util.ObservableReference<
  Variable<T>
> {
  _owner: Component;

  constructor(owner: Component) {
    super();
    this._owner = owner;
  }

  get system(): ?ConstraintSystem {
    return this._owner.system;
  }

  get name(): string {
    return this._owner.variableReferenceName(this);
  }

  isOwningReference(): boolean {
    return this.value == null ? false : this.value._owner === this;
  }
  prettyPrint(): string {
    let s = this.name;
    if (this.value != null) s += ":" + this.value._index;
    return s;
  }
}
export function mkVariable<T>(owner: Component, v: ?T): VariableReference<T> {
  let r = new VariableReference(owner);
  r.value = new Variable(r, v);
  return r;
}
let freshIndex = (() => {
  let ind = 0;
  return () => ++ind;
})();

export class Variable<T> {
  _index: number; // for debugging purposes
  _owner: VariableReference<T>;

  _activation: VariableActivation;

  _subject: Obs.Subject<Event<T>>; // holds observers

  _value: ?T;
  _error: any;
  _inError: boolean;
  _pending: boolean;

  get value(): ?T {
    return this._inError ? undefined : this._value;
  }
  get error(): any {
    return this._inError ? this._error : undefined;
  }
  get stale(): boolean {
    return !this._pending && this._inError;
  }
  get pending(): boolean {
    return this._pending;
  }

  constructor(owner: VariableReference<T>, v?: ?T = undefined) {
    this._index = freshIndex();
    this._owner = owner;

    this._value = undefined;
    this._error = undefined;
    this._inError = false;
    this._pending = true;

    this._subject = new Obs.Subject((o) => {
      let evt: Event<T> = {
        value: this._value,
        pending: this._pending,
        inError: this._inError,
      };
      if (this._inError) evt.error = this._error;
      o.next(evt);
    });

    this.pushNewActivation();
    if (v !== undefined) this._activation.resolve(v);
  }

  subscribe(o: any): Obs.Subscription {
    return this._subject.subscribe(o);
  }
  subscribeValue(o: any): Obs.Subscription {
    return this._subject
      .filter((e) => e.hasOwnProperty("value"))
      .map((e) => e.value)
      .subscribe(o);
  }
  subscribePending(o: any): Obs.Subscription {
    return this._subject
      .filter((e) => e.hasOwnProperty("pending"))
      .map((e) => e.pending)
      .subscribe(o);
  }
  subscribeError(o: any): Obs.Subscription {
    return this._subject
      .filter((e) => e.hasOwnProperty("inError") || e.hasOwnProperty("error"))
      .map((e) => {
        let r = {};
        if (e.hasOwnProperty("error")) r.error = e.error;
        if (e.hasOwnProperty("inError")) r.inError = e.inError;
        return r;
      })
      .subscribe(o);
  }

  pushNewActivation(): VariableActivation {
    let r = new VariableActivation(this);
    this._activation = r;

    if (this._pending === false) {
      this._pending = true;
      this._subject.sendNext({ pending: true });
    }

    r.promise.then(
      (val) => {
        if (r === this._activation) {
          // is the activation still the newest
          this._value = val;
          let evt: Event<T> = { value: val };
          if (this._inError) {
            this._inError = false;
            evt.inError = false;
          }
          this._pending = false;
          evt.pending = false;
          this._subject.sendNext(evt);
        }
      },
      (e) => {
        if (r === this._activation) {
          this._error = e;
          let evt: Event<T> = { error: e };
          if (!this._inError) {
            this._inError = true;
            evt.inError = true;
          }
          this._pending = false;
          evt.pending = false;
          this._subject.sendNext(evt);
        }
      }
    );
    return r;
  }
  get currentPromise(): Promise<T> {
    return this._activation.promise;
  }
  get owner(): VariableReference<T> {
    return this._owner;
  }
  get system(): ?ConstraintSystem {
    return this._owner.system;
  }
  get name(): string {
    if (this._owner == null) return "<unnamed>";
    else return this._owner.name;
  }
  set(v: T): Variable<T> {
    this.assign(v);
    let s = this.system;
    if (s != null) s.updateDirty();
    return this;
  }

  fail(e: any): Variable<T> {
    this._assign().reject(e);
    let s = this.system;
    if (s != null) s.updateDirty();
    return this;
  }

  assign(v: T): void {
    this._assign().resolve(v);
  }

  _assign(): VariableActivation {
    let prevActivation = this._activation;
    this.pushNewActivation();
    let s = this.system;
    if (s != null) {
      s.promoteVariable(this);
      let sc = s.getSourceConstraint(this);
      if (sc != null) s.setDirty(sc);
      s.setDirty(s.getStay(this));
    }
    util.release(prevActivation);
    return this._activation;
  }
  touch(): Variable<T> {
    let s = this.system;
    if (s != null) s.promoteVariable(this);
    return this;
  }
  _touchPlanSolve(): VariableActivation {
    this.touch();
    let s = this.system;
    let prevActivation = this._activation;
    this.pushNewActivation();

    if (s == null) {
      util.release(prevActivation);
      return this._activation;
    }

    let pushedActivation = this._activation;

    let sc = s.getSourceConstraint(this);
    s.setDirty(sc != null ? sc : s.getStay(this));
    s.updateDirty();

    // solving pushes new activations to variables, also possibly to this._activation.
    // This happens if 'this' cannot stay in the current plan.

    util.release(prevActivation);

    // return the activation that should be resolved or rejected
    return pushedActivation;
  }
}
type Event<T> = {
  value?: ?T,
  error?: any,
  pending?: boolean,
  inError?: boolean,
};
export class Component {
  _owner: ?Component;
  _system: ?ConstraintSystem;
  _name: string;

  _vars: Set<Variable<any>>;
  _varRefs: Map<string, VariableReference<any>>;
  _varRefNames: Map<VariableReference<any>, string>;

  _constraints: Map<string, Constraint>;
  _constraintNames: Map<Constraint, string>;
  _nextFreshCIndex: number; // used for generating fresh constraint names

  _components: Map<string, Component>;

  constructor(name: string) {
    this._owner = null;
    this._system = null;
    this._name = name;
    this._vars = new Set();
    this._varRefs = new Map();
    this._varRefNames = new Map();
    this._constraints = new Map();
    this._constraintNames = new Map();
    this._nextFreshCIndex = 0;
    this._components = new Map();
  }
  clone(name?: string = freshComponentName()): Component {
    let that = new Component(name);
    let o2n = new Map(); // old2new

    // clone variable references
    for (let [n, v] of this._varRefs) {
      if (v.isOwningReference()) {
        o2n.set(v, that.emplaceVariable(n));
      } else o2n.set(v, that.emplaceVariableReference(n));
    }

    // clone constraints
    for (let [n, c] of this._constraints) {
      let nrefs: any = c._varRefs.map((vr) => o2n.get(vr));
      if (!isStay(c)) that.emplaceConstraint(n, c._cspec, nrefs, c._optional);
    }

    // clone nested components
    for (let [n, cmp] of this._components) that.addComponent(cmp.clone(n));
    return that;
  }
  get system(): ?ConstraintSystem {
    return this._system;
  }
  get name(): string {
    return this._name;
  }
  connectSystem(system: ConstraintSystem): boolean {
    if (this._owner != null) {
      hdconsole.error(`Tried to connect the nested component ${this._name}`);
      return false;
    }
    if (this._system === system) {
      hdconsole.warn(`Same system connected twice to component ${this._name}`);
      return false;
    }
    if (this._system != null) {
      hdconsole.error(
        `Trying to connect an already connected component ${this._name} to a another system`
      );
      return false;
    }
    this._connectSystem(system);
    return true;
  }

  _connectSystem(system: ConstraintSystem): void {
    this._system = system; // must set system already here
    // so that connect notifications
    // compare systems correctly FIXME: think abou this
    for (let c of this._components.values()) c._connectSystem(system);
    for (let c of this._constraints.values())
      if (c._danglingCount === 0) system.addConstraint(c);
  }

  disconnectSystem(): boolean {
    if (this._system == null) {
      hdconsole.error(
        `Tried to disconnect a component ${this._name} that is already disconnected`
      );
      return false;
    }
    if (this._owner != null) {
      hdconsole.error(
        `Tried to disconnect a subcomponent ${this._name} when owner is still connected`
      );
      return false;
    }
    this._disconnectSystem();
    return true;
  }

  _disconnectSystem(): void {
    for (let c of this._components.values()) c._disconnectSystem();
    for (let c of this._constraints.values())
      (this._system: any).removeConstraint(c);
    // cast OK since we know system is connected
    this._system = null;
  }
  addComponent(c: Component) {
    if (c._owner != null) {
      hdconsole.error(
        `Tried to add component ${c._name} that already has an owner to component ${this._name}`
      );
      return;
    }
    if (c._system != null) {
      hdconsole.error(
        `Tried to add to component ${this._name} the component ${c._name} that is connected to a system`
      );
      return;
    }
    if (this._components.has(c._name)) {
      hdconsole.error(
        `Tried to add the component whose name ${c._name} already exists`
      );
      return;
    }
    c._owner = this;
    this._components.set(c._name, c);
    if (this._system != null) c.connectSystem(this._system);
  }
  emplaceVariable<T>(n: string, v: ?T): VariableReference<any> {
    hdconsole.assert(
      !this._varRefs.has(n),
      `Trying to add variable ${n} twice to component ${this._name}`
    );
    let vr = mkVariable(this, v);
    this._varRefs.set(n, vr);
    this._varRefNames.set(vr, n);
    this._vars.add((vr.value: any)); // cast ok, vr is an owning reference
    let name = "__stay__" + n;
    let c = new Constraint(this, stayConstraintSpec, [vr], true);
    this._constraints.set(name, c);
    this._constraintNames.set(c, name);
    return vr;
  }

  emplaceVariableReference(n: string): VariableReference<any> {
    hdconsole.assert(
      !this._varRefs.has(n),
      `Trying to add variable reference ${n} twice to component ${this._name}`
    );
    let vr = new VariableReference(this);
    this._varRefs.set(n, vr);
    this._varRefNames.set(vr, n);
    return vr;
  }
  emplaceConstraint(
    n: ?string,
    cspec: ConstraintSpec,
    vrefs: Iterable<VariableReference<any>>,
    optional: boolean = false
  ): Constraint {
    let c = new Constraint(this, cspec, vrefs, optional);
    if (n == null) n = this._nextFreshConstraintName();
    if (this._constraints.has(n))
      throw `Constraint ${n} already exists in component ${this._name}`;
    this._constraints.set(n, c);
    this._constraintNames.set(c, n);
    return c;
  }

  _nextFreshConstraintName(): string {
    return "c#" + this._nextFreshCIndex++;
  }
  variableReferenceName(r: VariableReference<any>): string {
    let s = this._varRefNames.get(r);
    return s == null ? "<unnamed>" : s;
  }
  constraintName(c: Constraint): ?string {
    return this._constraintNames.get(c);
  }

  getVariableReference(n: string): ?VariableReference<any> {
    let r = this._varRefs.get(n);
    if (r != null) return r;
    if (this._owner == null) return undefined;
    return this._owner.getVariableReference(n);
  }
  get vs(): any {
    return new Proxy(this, {
      get: function (target, property): any {
        return target._varRefs.get(property);
      },
      set: function (target, property, value, receiver): boolean {
        let r = target._varRefs.get(property);
        if (r == null) {
          hdconsole.warn(`Linking an unknown variable ${property}`);
          return false; // this causes a TypeError in strict mode
        } else {
          r.link(value);
          return true;
        }
      },
    });
  }
  get cs(): any {
    return new Proxy(this, {
      get: function (target, property) {
        return target._constraints.get(property);
      },
    });
  }
  get components(): any {
    return new Proxy(this, {
      get: function (target, property) {
        return target._components.get(property);
      },
    });
  }
}
function freshNameGenerator(prefix: string): () => string {
  let n = 0;
  return () => prefix + n++;
}
const freshComponentName: () => string = freshNameGenerator("comp#");
export { freshComponentName };
export function isOwnerOf(c1: Component, c2: Component): boolean {
  while (c1 !== c2) {
    if (c2._owner == null) return false;
    else c2 = c2._owner;
  }
  return true;
}
// FIXME: these are hacks because of trouble with instanceof and babel
function isConstraint(c: Constraint | Variable<any>): boolean {
  return c.hasOwnProperty("_cspec");
}
function isVariable(v: Variable<any> | Constraint): boolean {
  return v.hasOwnProperty("_activation");
}
function isMethod(v: Variable<any> | Method): boolean {
  return v.hasOwnProperty("_promiseMask");
}
export class Constraint {
  _owner: Component;

  _cspec: ConstraintSpec;

  _vars: Array<Variable<any>>;
  _used: Array<boolean>;

  _varRefs: Array<VariableReference<any>>;
  _danglingCount: number;

  _indices: Map<Variable<any>, number>;

  _optional: boolean;

  _selectedMethod: ?Method;

  _viableMethods: Set<Method>; // used in pruning
  _outCounts: Array<number>; // used in pruning
  _forcedVariables: Set<number>; // used in pruning

  prettyPrint(): string {
    let s = "constraint " + this.name + " {\n";
    s += "  ";
    for (let vr of this._varRefs) s += vr.prettyPrint() + "; ";
    s += "\n";
    s += this._cspec.prettyPrint("  ", this._varRefs);
    s += "}\n";
    return s;
  }

  substitute(vrs: Iterable<VariableReference<any>>): Constraint {
    return new Constraint(this._owner, this._cspec, vrs, this._optional);
  }
  constructor(
    owner: Component,
    cspec: ConstraintSpec,
    vrefs: Iterable<VariableReference<any>>,
    optional?: boolean = false
  ) {
    this._owner = owner;
    this._cspec = cspec;
    this._varRefs = [];
    this._vars = Array(cspec.nvars).fill(null);

    this._used = Array(cspec.nvars).fill(true); // FIXME: this is probably not used anymore

    this._danglingCount = cspec.nvars;

    this._optional = optional;
    // _vars is only used when all refs are resolved

    this._indices = new Map();

    let i = 0;
    for (let r of vrefs) {
      hdconsole.assert(
        isOwnerOf(r._owner, this._owner),
        `Constructing constraint ${this.name} with a foreign variable reference`
      );
      this._varRefs[i] = r;
      {
        let index = i; // for correct scoping
        r.subscribe((v) => {
          if (v !== null) {
            // connect
            if (this._vars[index] !== null)
              console.log("NOTNULL", this._vars[index]);
            // FIXME: is this necessary still?

            this._vars[index] = v;
            this._indices.set(v, index);
            if (--this._danglingCount == 0) {
              let s = this.system;
              if (s != null) s.addConstraint(this);
            }
          } else {
            // disconnect
            if (this._vars[index] === null) return; // already disconnected
            if (this._danglingCount++ == 0) {
              let s = this.system;
              if (s != null) s.removeConstraint(this);
            }
            this._indices.delete(this._vars[index]);
            this._vars[index] = (null: any);
          }
        });
      }
      ++i;
    }

    this._initPruningData();
    this.clearSelectedMethod();
  }
  equals(c: Constraint): boolean {
    return this === c;
  }
  get name(): string {
    return this._owner == null
      ? "<unnamed>"
      : ((this._owner.constraintName(this): any): string);
  }
  get owner(): Component {
    return this._owner;
  }
  get nvars(): number {
    return this._cspec.nvars;
  }
  get system(): ?ConstraintSystem {
    return this._owner.system;
  }
  i2v(i: number): Variable<any> {
    return this._vars[i];
  }
  v2i(v: Variable<any>): number {
    hdconsole.assert(
      this._indices.has(v),
      "unknown variable in Constraint.v2i: " + v.name
    );
    return ((this._indices.get(v): any): number);
  }
  variables(): Iterable<Variable<any>> {
    return this._vars;
  }

  outs(m: Method): Iterable<Variable<any>> {
    return m.vOuts(this._vars);
  }
  ins(m: Method): Iterable<Variable<any>> {
    return m.vIns(this._vars);
  }
  nonOuts(m: Method): Iterable<Variable<any>> {
    return m.vNonOuts(this._vars);
  }

  methods(): Iterable<Method> {
    return this._cspec.methods();
  }
  hasMethod(m: Method): boolean {
    return this._cspec.hasMethod(m);
  }

  viableMethods(): Iterable<Method> {
    return this._viableMethods;
  }
  get selectedMethod(): ?Method {
    return this._selectedMethod;
  }
  set selectedMethod(m: Method): void {
    hdconsole.assert(
      this._cspec.hasMethod(m),
      "selecting a method not in the constraint"
    );
    this._selectedMethod = m;
    for (let i of m.ins()) this._used[i] = true;
  }
  clearSelectedMethod(): void {
    this._selectedMethod = null;
  }
  isEnforced(): boolean {
    return this._selectedMethod != null;
  }
  isOutputVariable(v: Variable<any>): boolean {
    if (this.selectedMethod == null) return false;
    else return this.selectedMethod.isOut((this.v2i(v): any));
  }
  isInputVariable(v: Variable<any>): boolean {
    if (this.selectedMethod == null) return false;
    else return this.selectedMethod.isIn((this.v2i(v): any));
  }
  isNonOutputVariable(v: Variable<any>): boolean {
    if (this.selectedMethod == null) return false;
    else return this.selectedMethod.isNonOut((this.v2i(v): any));
  }
  downstreamVariables(): Iterable<Variable<any>> {
    if (this.selectedMethod == null) return util.mkEmptyIterable();
    else return this.selectedMethod.vOuts(this._vars);
  }
  downstreamAndUndetermined(): Iterable<Variable<any>> {
    if (this.selectedMethod == null) return this._vars;
    else return this.selectedMethod.vOuts(this._vars);
  }
  upstreamVariables(): Iterable<Variable<any>> {
    if (this.selectedMethod == null) return util.mkEmptyIterable();
    else return this.selectedMethod.vNonOuts(this._vars);
  }
  upstreamAndUndetermined(): Iterable<Variable<any>> {
    if (this.selectedMethod == null) return this._vars;
    else return this.selectedMethod.vNonOuts(this._vars);
  }
  _initPruningData(): void {
    this._viableMethods = new Set();
    this._forcedVariables = new Set();
    this._outCounts = Array(this.nvars).fill(0);
    for (let m of this.methods()) {
      this._viableMethods.add(m);
      for (let i of m.outs()) this._outCounts[i] += 1;
    }
    const vmcount = this._viableMethods.size;
    for (let i = 0; i < this.nvars; ++i) {
      if (this._outCounts[i] === vmcount) this._forcedVariables.add(i);
    }
  }
  _makeMethodViable(m: Method): Iterable<number> {
    hdconsole.assert(
      !this._viableMethods.has(m),
      "making a method viable twice"
    );
    hdconsole.assert(this.hasMethod(m), "unknown method");

    let vmcountOld = this._viableMethods.size;
    this._viableMethods.add(m);

    let newlyUnforced = [];
    for (let i of m.outs()) this._outCounts[i] += 1;
    for (let i of m.nonOuts()) {
      if (this._outCounts[i] === vmcountOld) {
        this._forcedVariables.delete(i);
        newlyUnforced.push(i);
      }
    }
    return newlyUnforced;
  }
  // returns the indices of the newly forced variables
  _makeMethodNonviable(m: Method): Iterable<number> {
    hdconsole.assert(
      this._viableMethods.has(m),
      "making a method nonviable twice"
    );
    hdconsole.assert(
      this._viableMethods.size >= 2,
      "trying to make the last viable method nonviable"
    );

    this._viableMethods.delete(m);
    let vmcount = this._viableMethods.size;
    let newlyForced = [];
    for (let i of m.outs()) this._outCounts[i] -= 1;
    for (let i of m.nonOuts()) {
      if (this._outCounts[i] === vmcount) {
        this._forcedVariables.add(i);
        newlyForced.push(i);
      }
    }

    // FIXME: remove at some point
    for (let i = 0; i < this._outCounts.length; ++i)
      hdconsole.assert(this._outCounts[i] >= 0);

    return newlyForced;
  }
  _makeMethodsNonviableByVariable(v: Variable<any>): Iterable<Variable<any>> {
    let vi = this.v2i(v);
    if (this._forcedVariables.has(vi)) return util.mkEmptyIterable();
    let newlyForced = new Set();
    for (let m of util.filter((m) => m.isOut(vi), this._viableMethods)) {
      for (let vv of util.map(
        (v) => this.i2v(v),
        this._makeMethodNonviable(m)
      )) {
        newlyForced.add(vv);
      }
    }
    return newlyForced;
  }
}
export class ConstraintSystem {
  _musts: Set<Constraint>;
  _optionals: util.PriorityList<Constraint>;

  _dirty: Set<Constraint>;

  // FIXME: move to a better place
  allConstraints(): Iterable<Constraint> {
    return util.join([this._musts, this._optionals.entries()]);
  }

  addComponent(c: Component): boolean {
    return c.connectSystem(this);
  }
  removeComponent(c: Component): boolean {
    if (c.system != this) {
      console.warn(
        `Tried to remove component ${c._name} from a constraint system in which it does not exist.`
      );
      return false;
    }
    return c.disconnectSystem();
  }

  prettyPrint(): string {
    let s = "-- musts --\n";
    for (let m of this._musts) s += m.prettyPrint();
    s += "-- optionals --\n";
    for (let m of this._optionals.entries()) s += m.prettyPrint();
    return s;
  }

  _v2cs: util.OneToManyMap<Variable<any>, Constraint>;

  // _cg: ConstraintGraph;
  _usg: UpstreamSolutionGraph;
  _dsg: DownstreamSolutionGraph;

  constructor() {
    this._optionals = new util.PriorityList();
    this._musts = new Set();
    this._dirty = new Set();

    this._v2cs = new util.OneToManyMap();

    this._usg = new UpstreamSolutionGraph(this);
    this._dsg = new DownstreamSolutionGraph(this);
  }

  update(): void {
    this.plan(this.allConstraints()); // FIXME: a little wasteful since plan iterates
    // all vars of all constraints, and same vars appear many time. OK for now
    this.solveFromConstraints(this.allConstraints());
  }
  variables(): Iterable<Variable<any>> {
    return this._v2cs.keys();
  }
  constraints(v: Variable<any>): Iterable<Constraint> {
    return this._v2cs.values(v);
  }
  *allConstraints(): Iterable<Constraint> {
    for (let c of this._musts) yield c;
    for (let c of this._optionals.entries()) yield c;
  }
  isMust(c: Constraint): boolean {
    return this._musts.has(c);
  }
  isOptional(c: Constraint): boolean {
    return !this._musts.has(c);
  }
  addConstraint(c: Constraint) {
    for (let v of c.variables()) this._v2cs.add(v, c);
    if (c._optional) this._optionals.pushBack(c);
    else this._musts.add(c);
    this._dirty.add(c);
  }

  removeConstraint(c: Constraint) {
    for (let v of c.variables()) {
      this._v2cs.remove(v, c);
      if (this._v2cs.count(v) == 0) this._v2cs.removeKey(v);
    }
    if (c._optional) this._optionals.remove(c);
    else this._musts.delete(c);
    this._dirty.delete(c);
  }

  setDirty(c: Constraint): void {
    this._dirty.add(c);
  }
  gatherUpstreamConstraints(
    vs: Iterable<sg_vertex_descriptor>
  ): Iterable<Constraint> {
    let r: Iterable<sg_vertex_descriptor> = util.filter(
      (v) => isConstraint(v),
      reverseTopoSortFrom(this._usg, vs)
    );
    return (r: any); // cast is to shut up flowtype
  }
  gatherDownstreamConstraints(
    vs: Iterable<sg_vertex_descriptor>
  ): Iterable<Constraint> {
    let r: Iterable<sg_vertex_descriptor> = util.filter(
      (v) => isConstraint(v),
      reverseTopoSortFrom(this._dsg, vs)
    );
    return (r: any);
  }
  gatherDownstreamVariables(
    vs: Iterable<sg_vertex_descriptor>
  ): Iterable<Variable<any>> {
    let r: Iterable<sg_vertex_descriptor> = util.filter(
      (v) => isVariable(v),
      reverseTopoSortFrom(this._dsg, vs)
    );
    return (r: any);
  }
  promoteVariable(v: Variable<any>): void {
    this.promoteConstraint(this.getStay(v));
  }
  promoteConstraint(c: Constraint) {
    this._optionals.promote(c);
  }
  demoteConstraint(c: Constraint) {
    this._optionals.demote(c);
  }
  strength(c: Constraint): number {
    if (this.isMust(c)) return this._optionals.highestPriority + 1;
    else return this._optionals.priority(c);
  }
  getStay(v: Variable<any>): Constraint {
    for (let c of this._v2cs.values(v)) {
      if (c.name == "__stay__" + v.name) return ((c: any): Constraint);
    }
    hdconsole.assert(false, "Variable's stay constraint does not exists");
    return ((null: any): Constraint);
  }
  planDirty(): Iterable<Constraint> {
    return this.plan(this._dirty);
  }
  updateDirty(): void {
    this.planDirty();
    this.executeDirty();
    this._dirty.clear();
  }
  plan(toEnforce: Iterable<Constraint>): Iterable<Constraint> {
    let vs = util.join(util.map((c) => c.variables(), toEnforce));
    // all variables from all constraints in the enforce queues

    let constraintsThatNeedNewPlan = this.gatherUpstreamConstraints(vs);

    // gather all constraints upstream from these variables.
    // This includes all constraints reachable through any method
    // of an unenforced constraint. Quickplan does not do this since
    // it has a separate reenforcing phase, but we put all constraints
    // that may need to change into one bunch.
    // The point is that unenforced constraints may become enforceable,
    // and then they may force resolving of their upstream constraints.

    let enforceable = new Set();
    // this is the set of constraints that is determined to be enforceable
    let forcedSet = new Set();
    // this is the set of variables that are forced with the enforceable
    // methods

    let opts = new util.BinaryHeap(
      (c1, c2) => this.strength(c1) > this.strength(c2)
    );

    // initialize simple planner with all must constraints (should be enforceable)
    let sp = new SimplePlanner();
    util.forEach((c) => {
      c.clearSelectedMethod();
      c._initPruningData(); // FIXME: maybe just make methods viable again
      if (this.isMust(c)) {
        enforceable.add(c);
        sp.addConstraint(c);
      } else {
        opts.push(c);
      }
    }, constraintsThatNeedNewPlan); //

    hdconsole.assert(sp.plan(), "no plan for must constraints");
    // the conflicting constraints are in sp.constraints()

    while (!opts.empty()) {
      let opt = opts.pop();

      switch (sp.extendPlanIfPossible(opt, forcedSet)) {
        case false:
          break;
        case true:
          enforceable.add(opt);
          this._prune(opt, enforceable, forcedSet);
          break;
        case null:
          sp.clear();
          for (let c of enforceable) {
            //            c.clearSelectedMethod();
            sp.addConstraint(c);
          }
          sp.addConstraint(opt);
          if (sp.plan()) {
            enforceable.add(opt);
            this._prune(opt, enforceable, forcedSet);
          } else {
            sp.removeConstraint(opt);
            opt.clearSelectedMethod();
            sp.undoChangesAfterFailedPlan();
          }

          break;
      }
    }

    return enforceable;
  }
  // prune starting from constraint c
  _prune(
    c: Constraint,
    constraintsOfInterest: Set<Constraint>,
    forcedSet: Set<Variable<any>>
  ) {
    let inspected: Set<Variable<any>> = new Set();
    // initialize worklist with the variables that c forces
    let worklist: Array<Variable<any>> = Array.from(c._forcedVariables, (i) =>
      c.i2v(i)
    );
    while (worklist.length > 0) {
      let v = worklist.pop();
      inspected.add(v);
      if (forcedSet.has(v)) continue;
      forcedSet.add(v);

      // inspect all constraints connected to v
      for (let cn of this.constraints(v)) {
        if (!cn.isEnforced()) continue;
        if (!constraintsOfInterest.has(cn)) continue;
        for (let vv of cn._makeMethodsNonviableByVariable(v)) {
          // vv iterates over the set of newly forced variables
          if (!inspected.has(vv)) {
            worklist.push(vv);
          }
        }
      }
    }
  }
  solveFromConstraints(cs: Iterable<Constraint>): void {
    this.solve(util.join(util.map((c) => c.downstreamVariables(), cs)));
  }
  solve(vs: Iterable<Variable<any>>): void {
    let cs: Array<Constraint> = [];
    for (let v of vs) {
      let c = this.getSourceConstraint(v);
      if (c != null) cs.push(c);
    }
    // Find the constraints that write to variables in vs.
    // Usually these are stay constraints, but could be other constraints if the
    // variable cannot stay in the chosen plan
    // NOTE: every variable must have a stay or other constraint that writes to it. Otherwise
    // constraints downstream from it are not found.
    let dsc: Array<Constraint> = Array.from(
      this.gatherDownstreamConstraints(cs)
    ).reverse();
    for (let c of dsc) this.scheduleConstraint(c);
  }

  executeDirty(): void {
    let dsc: Array<Constraint> = Array.from(
      this.gatherDownstreamConstraints(this._dirty)
    ).reverse();
    for (let c of dsc) this.scheduleConstraint(c);
  }

  getSourceConstraint(v: Variable<any>): ?Constraint {
    for (let c of this._v2cs.values((v: any))) {
      if (c.isOutputVariable(v)) return c;
    }
    return null;
  }
  scheduleConstraint(c: Constraint) {
    if (!c.isEnforced()) return; // if there is no selected method, there  is nothing to schedule

    let m = ((c.selectedMethod: any): Method);
    let inDeferreds: Array<VariableActivation> = Array.from(
      c.ins(m),
      (v) => v._activation
    );

    // push new activations to all outputs, but remember the previous onces
    let prevActs: Array<VariableActivation> = [];
    util.forEach((v) => {
      prevActs.push(v._activation);
      v.pushNewActivation();
    }, c.outs(m));
    let outDeferreds: Array<VariableActivation> = Array.from(
      c.outs(m),
      (v) => v._activation
    );

    new MethodActivation(m.code, m._promiseMask).execute(
      inDeferreds,
      outDeferreds
    );

    // only release the old activations of outputs here; after a new method activation has been executed
    util.forEach((v) => util.release(v), prevActs);
  }

  // touch order is fixed, last parameter gets highest
  scheduleCommand(
    ins: Iterable<Variable<any>>,
    outs: Iterable<Variable<any>>,
    f: Function
  ) {
    let inDeferreds: Array<VariableActivation> = Array.from(
      ins,
      (v) => v._activation
    );
    let outArray: Array<Variable<any>> = Array.from(outs);

    // push new activations to all outputs, but remember the previous onces
    let prevActs: Array<VariableActivation> = [];
    util.forEach((v) => {
      prevActs.push(v._activation);
      v.pushNewActivation();
    }, outArray);
    let outDeferreds: Array<VariableActivation> = Array.from(
      outArray,
      (v) => v._activation
    );
    new MethodActivation(f, Array(inDeferreds.length).fill(false)).execute(
      inDeferreds,
      outDeferreds
    );
    // only release the old activations of outputs here; after a new method activation has been executed
    util.forEach((v) => util.release(v), prevActs);

    outArray.forEach((v) => this.promoteVariable(v));
    let enforceable = this.plan(util.map((v) => this.getStay(v), outArray));

    this.solve(outArray);
  }
}
type sg_vertex_descriptor = Variable<any> | Constraint;
type sg_edge_descriptor = [sg_vertex_descriptor, sg_vertex_descriptor];
export class DownstreamSolutionGraph
  implements AdjacencyIteration<sg_vertex_descriptor>
{
  _system: ConstraintSystem;
  constructor(system: ConstraintSystem) {
    this._system = system;
  }

  adjacentOutVertices(v: sg_vertex_descriptor): Iterable<sg_vertex_descriptor> {
    if (isConstraint(v)) {
      return ((v: any): Constraint).downstreamAndUndetermined();
    } else
      return util.filter(
        (c) =>
          c.isNonOutputVariable(((v: any): Variable<any>)) || !c.isEnforced(),
        this._system._v2cs.values(((v: any): Variable<any>))
      );
  }
  outDegree(v: sg_vertex_descriptor): number {
    if (isConstraint(v)) {
      return (v: any).selectedMethod != null
        ? (v: any).selectedMethod.nOuts()
        : 0;
    } else {
      return util.count(this.adjacentOutVertices((v: any))); // not constant time
    }
  }
}
export class UpstreamSolutionGraph
  implements AdjacencyIteration<sg_vertex_descriptor>
{
  _system: ConstraintSystem;
  constructor(system: ConstraintSystem) {
    this._system = system;
  }

  adjacentOutVertices(v: sg_vertex_descriptor): Iterable<sg_vertex_descriptor> {
    if (isConstraint(v)) {
      return (v: any).upstreamAndUndetermined();
    } else
      return util.filter(
        (c) => c.isOutputVariable(((v: any): Variable<any>)) || !c.isEnforced(),
        this._system._v2cs.values((v: any))
      );
  }
  outDegree(v: sg_vertex_descriptor): number {
    if (isConstraint(v)) {
      return (v: any).selectedMethod != null
        ? (v: any).selectedMethod.nNonOuts()
        : 0;
    } else {
      return util.count(this.adjacentOutVertices((v: any))); // not constant time
    }
  }
}
export class SimplePlanner {
  _v2cs: util.OneToManyMap<Variable<any>, Constraint>;
  _freeVars: Set<Variable<any>>;

  _forcedByPlan: Map<Variable<any>, Method>;

  _changeList: Array<[Constraint, Method]>;
  // record if selected method changes, so we can undo
  // to a previous solution

  constructor() {
    this._v2cs = new util.OneToManyMap();
    this._freeVars = new Set();

    this._forcedByPlan = new Map();

    this._changeList = [];
  }

  addConstraint(c: Constraint): void {
    for (let v of c.variables()) {
      this._v2cs.add(v, c);
      switch (this._v2cs.count(v)) {
        case 1:
          this._freeVars.add(v);
          break;
        case 2:
          this._freeVars.delete(v);
          break;
      }
    }
  }
  removeConstraint(c: Constraint): void {
    for (let v of c.variables()) {
      this._v2cs.remove(v, c);
      switch (this._v2cs.count(v)) {
        case 0:
          this._freeVars.delete(v);
          this._v2cs.removeKey(v);
          break;
        case 1:
          this._freeVars.add(v);
          break;
      }
    }
  }
  clear() {
    this._v2cs.clear();
    this._freeVars.clear();
    //    this._forcedByPlan.clear();
  }
  plan(): boolean {
    let foundFree = true;
    while (foundFree) {
      foundFree = false;
      for (let [c, m] of this.freeMethods()) {
        foundFree = true;
        let oldm = c.selectedMethod;
        if (oldm !== m && oldm != null) {
          this._changeList.push([c, oldm]);
          for (let v of c.outs(oldm)) this._forcedByPlan.delete(v);
          // clear old methods forced by data
        }
        c.selectedMethod = m;
        for (let v of c.outs(m)) this._forcedByPlan.set(v, m);
        this.removeConstraint(c);
      }
    }
    if (this._v2cs.countKeys() === 0) {
      this._changeList = []; // no undo info after successful plan
      return true;
    } else {
      return false;
    }
  }

  undoChangesAfterFailedPlan(): void {
    for (let [c, m] of this._changeList) {
      let oldm = c.selectedMethod;
      if (oldm != null) {
        for (let v of c.outs(oldm)) this._forcedByPlan.delete(v);
      }
      for (let v of c.outs(m)) this._forcedByPlan.set(v, m);
      c.selectedMethod = m;
    }
  }

  extendPlanIfPossible(c: Constraint, forcedSet: Set<Variable<any>>): ?boolean {
    // FIXME: special case for stays for speed
    if (
      util.every(
        (m) => util.some((v) => forcedSet.has(v), c.outs(m)),
        c._viableMethods
      )
    )
      return false;
    // if every method has some output to a forced variable,
    // the constraint is unenforceable

    if (util.every((v) => !this._forcedByPlan.has(v), c.variables())) {
      let m = util.first(c._viableMethods);
      // just pick the first (likely only method) for the plan
      hdconsole.assert(m != null, "constraint has no viable methods");
      if (m == null) return false;
      // FIXME: think if asserting is the best way: could just return false?
      c.selectedMethod = m;
      for (let v of c.outs(m)) {
        this._forcedByPlan.set(v, m);
      }

      // Methods that have an output to a forcedByPlan variable
      // cannot be added to the plan. But methods that have
      // nonOutputs that are forced, could lead to a cycle.
      // So we ban all constraints that have have variables
      // forced by the current plan.
      // This could be detected, but instead we just say
      // that it is unusure if plan is possible or not.
      // The usual case
      // of calling this function is with stay constraints,
      // and then cycles are not an issue.

      return true;
    }

    return null; // don't know if c can be enforced, must do planning
  }
  *freeMethods(): Iterable<[Constraint, Method]> {
    let seen: Set<Constraint> = new Set(); // check constraint only once
    for (let v of this._freeVars) {
      for (let c of this._v2cs.values(v)) {
        // FIXME: this loop should only happen once, because v is free.
        // right?
        if (!seen.has(c)) {
          seen.add(c);
          for (let m of c.viableMethods()) {
            if (util.every((v) => this._freeVars.has(v), c.outs(m))) {
              yield [c, m];
              break; // only generate the first method per constraint
            }
          }
        }
      }
    }
  }

  firstFreeMethod(): ?[Constraint, Method] {
    return util.first(this.freeMethods());
  }
  constraints(): Iterable<Constraint> {
    let cs: Set<Constraint> = new Set();
    for (let v of this._v2cs.keys()) {
      for (let c of this._v2cs.values(v)) cs.add(c);
    }
    return cs;
  }
}
const stayConstraintSpec = new ConstraintSpec([
  new Method(1, [0], [0], [maskNone], (a) => a, "stay"),
]);

export function enforceStayMethod(stay: Constraint) {
  stay.selectedMethod = (util.first(stay._cspec._methods): any);
}

export function isStay(c: Constraint): boolean {
  return c._cspec === stayConstraintSpec;
}
