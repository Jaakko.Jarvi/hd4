// @flow

// export type {
//   Graph,
//   VertexLabels,
//   AssignableVertexLabels,
//   EdgeLabels,
//   AssignableEdgeLabels,
//   Labels,
//   AssignableLabels,
//   VertexListIteration,
//   EdgeListIteration,
//   AdjacencyIteration,
//   BidirectionalAdjacencyIteration,
//   UndirectedAdjacencyIteration,
//   GrowableGraph,
//   ShrinkableGraph,
//   ModifiableGraph
// }
export interface Graph<VertexDescriptor, EdgeDescriptor> {
  source(e: EdgeDescriptor): VertexDescriptor;
  target(e: EdgeDescriptor): VertexDescriptor;
  edge(src: VertexDescriptor, target: VertexDescriptor): ?EdgeDescriptor;

  hasEdge(src: VertexDescriptor, target: VertexDescriptor): boolean;
  hasVertex(v: VertexDescriptor): boolean;
}
export interface VertexLabels<VertexDescriptor, VertexLabel> {
  vertexLabel(v: VertexDescriptor): ?VertexLabel;
}
export interface AssignableVertexLabels<VertexDescriptor, VertexLabel>
  extends VertexLabels<VertexDescriptor, VertexLabel> {
  setVertexLabel(v: VertexDescriptor, l: VertexLabel): void;
}

export interface EdgeLabels<EdgeDescriptor, EdgeLabel> {
  edgeLabel(e: EdgeDescriptor): ?EdgeLabel;
}
export interface AssignableEdgeLabels<EdgeDescriptor, EdgeLabel>
  extends EdgeLabels<EdgeDescriptor, EdgeLabel> {
  setEdgeLabel(e: EdgeDescriptor, l: EdgeLabel): void;
}

export interface Labels<
  VertexDescriptor,
  EdgeDescriptor,
  VertexLabel,
  EdgeLabel
> extends VertexLabels<VertexDescriptor, VertexLabel>,
    EdgeLabels<EdgeDescriptor, EdgeLabel> {}
export interface AssignableLabels<
  VertexDescriptor,
  EdgeDescriptor,
  VertexLabel,
  EdgeLabel
> extends AssignableVertexLabels<VertexDescriptor, VertexLabel>,
    AssignableEdgeLabels<EdgeDescriptor, EdgeLabel> {}
export interface VertexListIteration<VertexDescriptor> {
  vertices(): Iterable<VertexDescriptor>;
}
export interface EdgeListIteration<EdgeDescriptor> {
  edges(): Iterable<EdgeDescriptor>;
}
export interface AdjacencyIteration<VertexDescriptor> {
  adjacentOutVertices(v: VertexDescriptor): Iterable<VertexDescriptor>;
  outDegree(v: VertexDescriptor): number;
}
export interface BidirectionalAdjacencyIteration<VertexDescriptor>
  extends AdjacencyIteration<VertexDescriptor> {
  adjacentInVertices(v: VertexDescriptor): Iterable<VertexDescriptor>;
  inDegree(v: VertexDescriptor): number;
}
export interface UndirectedAdjacencyIteration<VertexDescriptor> {
  adjacentVertices(v: VertexDescriptor): Iterable<VertexDescriptor>;
  degree(v: VertexDescriptor): number;
}
export interface GrowableGraph<
  VertexDescriptor,
  EdgeDescriptor,
  VertexLabel,
  EdgeLabel
> extends Graph<VertexDescriptor, EdgeDescriptor> {
  addVertex(l: ?VertexLabel): VertexDescriptor;
  addVertexDescriptor(vd: VertexDescriptor, l: ?VertexLabel): VertexDescriptor;

  addEdge(
    s: VertexDescriptor,
    t: VertexDescriptor,
    l: ?EdgeLabel
  ): EdgeDescriptor;
}
export interface ShrinkableGraph<VertexDescriptor, EdgeDescriptor>
  extends Graph<VertexDescriptor, EdgeDescriptor> {
  removeVertex(v: VertexDescriptor): boolean;
  removeEdge(e: EdgeDescriptor): boolean;
  removeEdgeBetween(s: VertexDescriptor, t: VertexDescriptor): boolean;
}
export interface ModifiableGraph<
  VertexDescriptor,
  EdgeDescriptor,
  VertexLabel,
  EdgeLabel
> extends GrowableGraph<
      VertexDescriptor,
      EdgeDescriptor,
      VertexLabel,
      EdgeLabel
    >,
    ShrinkableGraph<VertexDescriptor, EdgeDescriptor> {}
