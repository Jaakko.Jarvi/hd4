// Define a constraint system (variables and constraints) as a component
let whap = hd.component`
  component whap {
    var A=100, w, h, p;
    constraint pwh {
      m1(w, h -> p) => 2*(w+h);
      m2(p, w -> h) => p/2 - w;
      m3(p, h -> w) => p/2 - h;
    }
    constraint Awh {
      n1(w, h -> A) => w*h;
      n2(A -> w, h) => [Math.sqrt(A), Math.sqrt(A)];
    }
  }
  `

// Define the property model and add its components
var cs = new hd.ConstraintSystem();
cs.addComponent(whap);
cs.update();

  function textBinder(box, v) {
    v.value.subscribe(val => { if ('value' in val) box.value = val.value; });
    box.addEventListener('input', () => { v.value.set(box.value); });
  };

  window.addEventListener('load', function() {
      textBinder(document.getElementById("A"), whap.vs.A);
      textBinder(document.getElementById("w"), whap.vs.w);
      textBinder(document.getElementById("h"), whap.vs.h);
      textBinder(document.getElementById("p"), whap.vs.p);
  });
