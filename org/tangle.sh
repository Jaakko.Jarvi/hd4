#!/usr/bin/env zsh
# -*- mode: shell-script -*-
#
# Tangle .org files with org-mode
# Example usage:
#    tangle file.org

# Set this to the location of your emacs executable
EMACSCMD="emacs"

# wrap each argument in the code required to call tangle on it
DIR=`pwd`
FILES=""
for i in $@; do
    echo "File: $i"
    $EMACSCMD -nw --batch --eval "
(progn
  (require 'org-install)
  (setq org-babel-use-quick-and-dirty-noweb-expansion t)
  (find-file (expand-file-name \"$i\" \"$DIR\"))  
  (org-babel-tangle)
  (kill-buffer))" 2>&1 | grep "Tangled"
done
