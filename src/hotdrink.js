// [[file:../org/hotdrink.org::*Tangle][Tangle:1]]
// @flow

export * from "./constraint-system.js";

import { ConstraintSystem, Method } from "./constraint-system.js";

export { component, hdl } from "./constraint-builder.js";

export const defaultConstraintSystem: ConstraintSystem = new ConstraintSystem();

// [[[[file:~/Git/hd4/org/hotdrink.org::*Tangle][Tangle]]][visualization]]
let objectId: (Object) => number = (() => {
  let m = new WeakMap();
  let counter = 0;
  let wm = (o: Object) => {
    let id = m.get(o);
    if (id !== undefined) return id;
    else {
      let curr = counter++;
      m.set(o, curr);
      return curr;
    }
  };
  return wm;
})();

export function constraintSystemInVisNodesAndEdges(
  cs: ConstraintSystem
): [any, any] {
  let nodes = [];
  let edges = [];

  for (let v of cs.variables()) {
    nodes.push({ id: objectId(v), label: v.name, shape: "box" });
  }
  for (let c of cs.allConstraints()) {
    let mcounter = 0;
    if (!c.isEnforced()) continue;

    let m = ((c.selectedMethod: any): Method);
    let id = `m#${objectId(c)}-${mcounter}`;
    console.log("ID: ", id, "C.M", c.name + "." + m.name);
    let color = "lime";
    nodes.push({
      id: id,
      label: c.name + "." + m.name,
      cid: "C#" + objectId(c),
      color: color,
    });
    for (let v of c.nonOuts(m))
      edges.push({ from: objectId(v), to: id, arrows: "to", dashes: true });
    for (let v of c.outs(m))
      edges.push({ from: id, to: objectId(v), arrows: "to" });
    // FIXME: ins should be the complement of outs
    ++mcounter;
  }
  return [nodes, edges];
}

export function constraintSystemInVisAllNodesAndEdges(
  cs: ConstraintSystem
): [any, any] {
  let nodes = [];
  let edges = [];

  for (let v of cs.variables()) {
    nodes.push({ id: objectId(v), label: v.name, shape: "box" });
  }
  for (let c of cs.allConstraints()) {
    let mcounter = 0;

    //if (!c.isEnforced()) continue;

    for (let m of c.methods()) {
      let id = `m#${objectId(c)}-${mcounter}`;
      console.log("ID: ", id, "C.M", c.name + "." + m.name);
      let color = "lime";
      nodes.push({
        id: id,
        label: c.name + "." + m.name,
        cid: "C#" + objectId(c),
        color: color,
      });
      for (let v of c.nonOuts(m)) {
        edges.push({ from: objectId(v), to: id, arrows: "to", dashes: true });
        console.log("INS: ", v.name);
      }
      for (let v of c.outs(m)) {
        edges.push({
          from: id,
          to: objectId(v),
          arrows: "to" /*, label: "FROM_C_"+c.name+"_M_"+m.name+"+TO_"+v.name*/,
        });
        console.log("OUTS: ", v.name);
      }
      // FIXME: ins should be the complement of outs
      ++mcounter;
    }
  }
  return [nodes, edges];
}
// visualization ends here
// Tangle:1 ends here
