#+TITLE: HotDrink's Observable Implementation

#+INCLUDE: setup.org

A minimalistic observable imlementation; follows the standard proposal https://tc39.es/proposal-observable/, but some 
features are missing. In addition, some runtime type checks are not done where we can rely on
flowtype's static guarantees.

* Tangle                    

The code in this document is tangled to ~observable.js~ and ~observable.test.js~.

#+BEGIN_SRC js :noweb strip-export :tangle "../src/observable.js" :comments noweb :exports none
  // @flow
  
  <<code>>
#+END_SRC

#+BEGIN_SRC js :noweb yes :tangle "../test/observable.test.js" :exports none
  // @flow
  import * as hd from '../src/observable.js'

  <<test>>
#+END_SRC

* Observer/Observable

** Observer interface

#+BEGIN_SRC js :noweb-ref code
    export interface Observer<T> {
      +start?: (subscription: Subscription) => void;
      +next?: (value: T) => void;
      +error?: (errorValue: any) => void;
      +complete?: () => void;
    }
#+END_SRC

An observer can define any subset of the interface's functions, which
are callbacks to be called on an observed event. Though an observer
that defines none seems pointless, we nevertheless allow such
observers too.  The observer interface could be parametrized by the
error type as well, but here a design choice, somewhat arbitrarily,
was made to receive error values as ~any~ instead.

** Subscription

#+BEGIN_SRC js :noweb-ref code
  export interface Subscription {
    unsubscribe(): void;
    +closed: boolean;
    // get closed(): boolean; (flowtype does not support getters in interfaces)
  }
#+END_SRC

#+BEGIN_SRC js :noweb-ref code
  export type SubscriberFunction<T> = (SubscriptionObserver<T>) => ((void => void) | Subscription);

  // create an observer from separate functions
  export function mkObserver<T>(obs: Observer<T> | (T) => void, 
                                err?: ((any) => void),
                                complete?: (() => void)): Observer<T> {
    if (typeof obs === 'function') {
        let observer = {};
        observer.next = obs;
        if (err != null) observer.error = err;
        if (complete != null) observer.complete = complete;
        return observer
      } else {
        return obs;
      }
  }

  export class Observable<T> {
    _subscriber: Function;

    constructor(subscriber: Function) {
      this._subscriber = subscriber;
    }

    subscribe(obs: Observer<T> | (T) => void, 
              err?: ((any) => void),
              complete?: (() => void)): Subscription {

      let observer = mkObserver(obs, err, complete);
      let subscription = new HdSubscription(observer);

      try {
        if (observer.start != null) observer.start(subscription);
      } catch (e) {
        // FIXME: HostReportError(e)
      }
      if (subscription.closed) return subscription;

      let subscriptionObserver = new SubscriptionObserver(subscription);

      try {
        let cleanup = this._subscriber(subscriptionObserver);
        if (typeof cleanup === 'object') cleanup = () => cleanup.unsubscribe(); 
          // must be a Subscription object
        subscription._cleanup = cleanup;
      } catch(e) {
        subscriptionObserver.error(e);
      }
      if (subscription.closed) subscription.cleanup();
      return subscription;
    }

      // FIXME Implement later
      // [Symbol.observable]() : Observable;

      static of(...items: Array<T>): Observable<T> {
        return new Observable(observer => {
          for (let item of items) {
            observer.next(item);
            if (observer.closed) return;
          }
          observer.complete();
        });
      }
      // FIXME Implement later
      // static from(obs: Observable<T> | Iterable<T>) : Observable<T> {
      //   
      // }   

      filter(p: (T) => boolean): Observable<T> {
        return new Observable(observer => {
          let o = mkObserver(observer);
          if (o.next == null) o = { next: (e) => {} };
          let s = this.subscribe(e => { if (p(e) == true) (o: any).next(e); });
          return s;
        });
      }
      map<U>(f: (T) => U): Observable<U> {
        return new Observable(observer => {
          let o = mkObserver(observer);
          if (o.next == null) o = { next: (e) => {} };
          let s = this.subscribe(e => { 
            let o = mkObserver(observer);
            (o: any).next(f(e)); });
          return s;
        });
      }
  }


  export class HdSubscription<T> implements Subscription {
    _observer: ?Observer<T>;
    _cleanup: any;
    constructor(observer: Observer<T>) {
      this._observer = observer;
      this._cleanup = undefined;
    }
    unsubscribe(): void { 
      this._observer = undefined;
      this.cleanup();
    }
    get closed(): boolean {
      return this._observer === undefined;
    }
    cleanup(): void {
      const cUp = this._cleanup;
      if (cUp === undefined) return;
      this._cleanup = undefined;
      try { cUp(); } catch(e) { /* FIXME: HostReportErrors(e) */ } 
    }
  }

  export class SubscriptionObserver<T> {
    _subscription: HdSubscription<T>;
    constructor(s: HdSubscription<T>) { this._subscription = s; }
    get closed(): boolean { return this._subscription.closed; }

    next(value: T): void {
      let s = (this._subscription: any); // appeasing flowtype
      if (s.closed) return;
      if (s._observer.next != null) { 
        try { s._observer.next(value); }
        catch (e) { /* FIXME: HostReportErrors(e) */ }
      }
    }
    error(errorValue: any): void {
      let s = (this._subscription: any); // appeasing flowtype
      if (s.closed) return; // FIXME, why not test this.closed ?
      if (s._observer.error != null) { 
        try { s._observer.error(errorValue); }
        catch (e) { /* FIXME: HostReportErrors(e) */ }
      }        
      this._subscription.cleanup();
    }
    complete(): void {
      let s = (this._subscription: any); // appeasing flowtype
      if (s.closed) return;
      if (s._observer.complete != null) {
        try { s._observer.complete(); }
        catch (e) { /* FIXME: HostReportErrors(e) */ }
      }
      this._subscription.cleanup();
    };    
  }
#+END_SRC

** Subjects

Subjects are observables that maintain a list of observers.  Given a
subject ~s~, ~s.sendNext(v)~, ~s.sendError(e)~, and ~s.sendComplete()~
call, respectively, the ~next~, ~error~, and ~complete~ functions for
all current observables.  ~Subject~ class implements this basic
behavior of subjects. The constructor takes an optional callback
function that is called at subscription on the observer that is
being subscribed, after the observer has been added.  This can be
used, e.g., to send an event at subscribing.

The ~nObservers~ property returns the number of currently subscribed
observers.

#+BEGIN_SRC js :noweb-ref code
  export class Subject<T> extends Observable<T> {
    _subjectState: {
      observers: Set<SubscriptionObserver<T>>,
      done: boolean
    };

    constructor(f?: Function = (o) => {}) {
      let state = {
        observers: new Set(),
        done: false
      }
      super(o => {
        if (!state.done) state.observers.add(o);
        f(o);
        return () => state.observers.delete(o); // this is a no-op if done==true
      });
      this._subjectState = state;
    };

    sendNext(v: T): void {
      if (this._subjectState.done) return;
      for (const obs of this._subjectState.observers) obs.next(v);
    }
    sendError(e: any): void {
      if (this._subjectState.done) return;
      this._subjectState.done = true;
      for (const obs of this._subjectState.observers) obs.error(e);      
    }
    sendComplete(): void {
      if (this._subjectState.done) return;
      this._subjectState.done = true;
      for (const obs of this._subjectState.observers) obs.complete();      
    }

    get nObservers(): number { return this._subjectState.observers.size; }
  }
#+END_SRC

#+BEGIN_SRC js :noweb-ref test :exports none
  test("Subject tests", () => {
    let k = new hd.Subject();
    expect(k.nObservers).toBe(0);
    let s = "";
    k.subscribe(v => s += "A" + v);    
    expect(k.nObservers).toBe(1);

    k.sendNext("1");
    expect(s).toEqual("A1");  
    
    k.subscribe(v => s += "B" + v);
    expect(k.nObservers).toBe(2);

    k.sendNext("2")
    expect(s).toEqual("A1A2B2");  

    k.sendComplete();
    expect(k.nObservers).toBe(0); // complete unsubscribes all observers

    k.sendNext("3");
    k.sendError(0);
    expect(s).toEqual("A1A2B2");  

    let q = new hd.Subject(o => o.next("I"));
    s = "";
    let u1 = q.subscribe(v => s += v);
    expect(s).toEqual("I");
    q.sendNext("A");
    expect(s).toEqual("IA");
    let u2 = q.subscribe(v => s += v);
    expect(s).toEqual("IAI");
    q.sendNext("B");
    expect(s).toEqual("IAIBB");

    expect(q.nObservers).toBe(2);
    u1.unsubscribe();
    expect(q.nObservers).toBe(1);
    u1.unsubscribe();
    expect(q.nObservers).toBe(1);
    u2.unsubscribe();
    expect(q.nObservers).toBe(0);

    q = new hd.Subject(o => {});
    q.subscribe(v => {});
    expect(q.nObservers).toBe(1);
    q.sendComplete(); // complete (and error) unsubscribes all observers
    expect(q.nObservers).toBe(0);

    // after subject has been closed, subscribing does not 
    // add observers.
    // The function passed to the constructor
    // is, however, called. (E.g. ReplaySubject relies on this)

    let us = q.subscribe(v => {});
    expect(q.nObservers).toBe(0);

    q.sendComplete(); // no-op, already closed
    expect(q.nObservers).toBe(0);

    us.unsubscribe(); // it is ok to call unsubscribe, even though 
    // subject closed during subscribing; it will not do anything
    expect(q.nObservers).toBe(0);

  });
#+END_SRC

The ~ReplaySubject~ class rembers (upto) a given number of prior
elements, and replays them at subscription.

#+BEGIN_SRC js :noweb-ref code
  export class ReplaySubject<T> extends Subject<T> {
    _replaySubjectState: {
      past: Array<T>,    
      pastLimit: number,
      isCompleted: boolean,
      isError: boolean,
      error?: any
    }
    
    constructor(limit?: number = 1) {
      let state = {
        past : [],
        pastLimit : limit,
        isCompleted : false,
        isError : false
      }
      super(o => {
        for (let v of state.past) o.next(v);
        if (state.isError) o.error(state.error);
        else if (state.isCompleted) o.complete();
      });
      this._replaySubjectState = state;
    };
    sendNext(v: T): void {
      this._replaySubjectState.past.push(v);
      if (this._replaySubjectState.past.length > this._replaySubjectState.pastLimit) { 
        this._replaySubjectState.past.shift();
      }
      super.sendNext(v);
    };
    sendError(e: T): void {
      this._replaySubjectState.isError = true;
      this._replaySubjectState.error = e;
      super.sendError(e);
    };
    sendComplete(): void {
      this._replaySubjectState.isCompleted = true;
      super.sendComplete();
    }
  }
  
#+END_SRC

#+BEGIN_SRC js :noweb-ref test :exports none
  test("ReplaySubject tests", () => {
    let k = new hd.ReplaySubject(2);
    let s = "";
    k.subscribe(v => s += "A" + v);
    k.sendNext("1");
    expect(s).toEqual("A1");    
    s = "";
    k.subscribe(v => s += "B" + v);
    expect(s).toEqual("B1");    
    s = "";
    k.sendNext("2");
    expect(s).toEqual("A2B2");    
    s = "";
    k.sendNext("3");
    expect(s).toEqual("A3B3");    
    s = "";
    k.subscribe(v => s += "C" + v);
    expect(s).toEqual("C2C3");    


    k = new hd.ReplaySubject(2);
    k.sendNext("1");
    k.sendError("e");
    s = ""
    k.subscribe({ next: v => { s += ":V" + v; }, error: e => { s += ":E" + e; } });
    expect(s).toEqual(":V1:Ee");
    k.sendNext("A");
    expect(s).toEqual(":V1:Ee");
  });
#+END_SRC

A /behavior subject/ is a /replay subject/ that replays
one prior element, and if no element has been produced yet, 
it ``replays'' a default value given at construction.

#+BEGIN_SRC js :noweb-ref code
  export class BehaviorSubject<T> extends ReplaySubject<T> {
    constructor(v: T) {
      super(1);
      this._replaySubjectState.past.push(v);
    };
  }
#+END_SRC

** BehaviorSubject test                                            :testing:

#+BEGIN_SRC js :noweb-ref test
  test("BehaviorSubject test", () => {
    let k = new hd.BehaviorSubject(1);
    let r: number;
    let s: number;
    let o1 = k.subscribe(v => r = v);
    expect(r).toBe(1);
    k.sendNext(2);
    let o2 = k.subscribe(v => s = v);
    expect(r).toBe(2);
    expect(s).toBe(2);
    k.sendNext(3);
    expect(r).toBe(3);
    expect(s).toBe(3);
    o1.unsubscribe();
    k.sendNext(4);
    expect(r).toBe(3);
    expect(s).toBe(4);
    k.sendError(5);
    expect(r).toBe(3);
    expect(s).toBe(4);
  });
#+END_SRC


* Tests                                                            :testing:

#+BEGIN_SRC js :noweb-ref test
  function deferred(): any { 
    let r; 
    let p = new Promise((resolve, reject) => r = resolve);
    return [r, p];
  };

  test("Simple Observable test", async () => {
    expect.assertions(1);
    var tobs = new hd.Observable ((o) => {
      setTimeout(() => o.next(1), 10);
      setTimeout(() => o.next(2), 10);
      setTimeout(() => o.next(3), 10);
      setTimeout(() => o.complete(), 100);
    });
    let sum = 0;
    let [r, p] = deferred(); 
    tobs.subscribe({ next: s => { sum += s },
                     complete: () => r(sum) });                        
    expect(await p).toBe(6);
  });

  test("Erroring Observable test", async () => {
    expect.assertions(1);
    var tobs = new hd.Observable ((o) => {    
      setTimeout(() => o.next(1), 10);
      setTimeout(() => o.next(2), 20);
      setTimeout(() => o.error("bad"), 30);
      setTimeout(() => o.next(3), 40);
      setTimeout(() => o.complete(), 100);
    });
    let sum = 0;
    let msg;
    let [r, p] = deferred(); 
    tobs.subscribe({ next: s => { sum += s },
                     error: e => { msg = e },
                     complete: () => r([sum, msg]) });                        
    expect(await p).toEqual([6, "bad"]);
  });

  test("Start and cleanup Observable test", async () => {
    expect.assertions(6);
    let status = "";
    var tobs = new hd.Observable ((o) => {    
      
      setTimeout(() => o.next(1), 10);      
      setTimeout(() => o.next(2), 10);
      setTimeout(() => o.complete(), 100);

      return () => { status = "closing remarks"; }
    });
    let sum = { v: 0 };
    let msg;
    let [r, p] = deferred(); 
    let su = tobs.subscribe({ val: sum,
                              start: subs => (subs: any)._observer.val.v += 100, 
                              next: s => { sum.v += s },
                              complete: () => r(sum.v) });                        
    expect(status).toEqual("");
    expect(await p).toEqual(103);

    // should have been completed and cleaned up now
    expect(status).toEqual("closing remarks");
    su.unsubscribe();

    status = "---";
    var tobs2 = new hd.Observable ((o) => {    
      
      setTimeout(() => o.next(1), 10);      
      setTimeout(() => o.next(2), 10);

      return () => { status = "tobs2 cleaned"; }
    });

    [r, p] = deferred(); 
    let o = { val: "-",
              start: subs => { status = "started"; } };
    expect(status).toEqual("---");              
    su = tobs2.subscribe(o);
    expect(status).toEqual("started");
    su.unsubscribe();
    expect(status).toEqual("tobs2 cleaned");
  });

  test("Unsubscribe from active Observable test", async () => {
    expect.assertions(1);
    var tobs = new hd.Observable ((o) => {
      setTimeout(() => o.next(1), 10);
      setTimeout(() => o.next(2), 20);
      setTimeout(() => o.next(3), 40);
      setTimeout(() => o.complete(), 100);
    });
    let sum = 0;
    let [r, p] = deferred(); 
    let us = tobs.subscribe({ next: s => { sum += s; }});
    setTimeout(() => { us.unsubscribe(); }, 30);
    setTimeout(() => { r(sum); }, 110);

    expect(await p).toBe(3);
  });

  test("Hot Observable test", async () => {
    expect.assertions(1);
    let counter = 0;
    let observers = new Set();
    function f() { 
      for (let o of observers) o.next(counter); 
      ++counter;
      if (counter < 100) setTimeout(f, 100);
    }
    f();
    var tobs = new hd.Observable ((o) => {
      observers.add(o);
      return () => { observers.delete(o); }
    });
    let sum = 0;
    let [r, p] = deferred(); 
    let us;
    setTimeout(() => {
      us = tobs.subscribe({ next: s => { sum += s; }});
      setTimeout(() => { us.unsubscribe(); }, 300);
    }, 150); // subscribe at 150, go on until 450, should get 2, 3, 4
    setTimeout(() => { r(sum); }, 600);
    expect(await p).toBe(2+3+4);
  });

  test("Observable of test", async () => {
    expect.assertions(1);
    let oble = hd.Observable.of(1, 2, 3);
    let sum = 0;
    let s1 = oble.subscribe(v => { sum += v; });
    let s2 = oble.subscribe(v => { sum += v; });
    let [r, p] = deferred();
    setTimeout(() => { r(sum); }, 100);    
    return p.then(data => expect(data).toBe(12));
  });
#+END_SRC


