// @flow
import type {
  VertexListIteration,
  AdjacencyIteration,
  BidirectionalAdjacencyIteration,
  ShrinkableGraph,
} from "./graph-concepts";

// export type { DfsAdjacencyVisitor };

export { reverseTopoSort, reverseTopoSortFrom, dfsVisit };

import { iterableToIterator } from "./utilities";

export interface DfsAdjacencyVisitor<VertexDescriptor> {
  discoverVertex(v: VertexDescriptor): void;
  finishVertex(v: VertexDescriptor): void;
}
function dfsVisit<VertexDescriptor>(
  g: AdjacencyIteration<VertexDescriptor>,
  vs: Iterable<VertexDescriptor>,
  visitor: DfsAdjacencyVisitor<VertexDescriptor>,
  visited?: Set<VertexDescriptor>
): void {
  if (visited == null) visited = new Set();

  let stack: Array<[VertexDescriptor, Iterator<VertexDescriptor>]> = [];
  let it = iterableToIterator(vs);

  while (true) {
    let n = it.next();
    if (n.done) {
      if (stack.length === 0) break; // returns
      let v;
      [v, it] = stack.pop();
      visitor.finishVertex(v);
      continue;
    }
    let v = n.value;
    if (visited.has(v)) {
      continue;
    }
    visitor.discoverVertex(v);
    visited.add(v);
    stack.push([v, it]);
    it = iterableToIterator(g.adjacentOutVertices(v));
  }
}
function reverseTopoSort<VertexDescriptor>(
  g: AdjacencyIteration<VertexDescriptor> &
    VertexListIteration<VertexDescriptor>
): Iterable<VertexDescriptor> {
  return reverseTopoSortFrom(g, g.vertices());
}
class TopoVisitor<VertexDescriptor> {
  result: Array<VertexDescriptor>;
  constructor() {
    this.result = [];
  }

  discoverVertex(v: VertexDescriptor): void {}
  finishVertex(v: VertexDescriptor): void {
    this.result.push(v);
  }
}

function reverseTopoSortFrom<VertexDescriptor>(
  g: AdjacencyIteration<VertexDescriptor>,
  vs: Iterable<VertexDescriptor>
): Iterable<VertexDescriptor> {
  let vis = new TopoVisitor();
  dfsVisit(g, vs, vis);
  return vis.result;
}
