let iscl = new hd.ConstraintSystem();

function checkboxBinder(box, v) {
  v.subscribe({ next: val => {
    if (val.hasOwnProperty('value')) {
      box.checked = val.value;
      visualize(iscl);
    }
  } });
  box.addEventListener('change', () => {
    if (box.checked) {
      eqc.connectSystem(iscl);
      iscl.plan([eqc.cs.Eq]);
      iscl.solveFromConstraints([eqc.cs.Eq]);
    } else {
      eqc.disconnectSystem(iscl);
    }
    v.value.set(box.checked);
  });
}

function textBinder(box, v) {
  v.value.subscribe({ next: val => {
    if (val.hasOwnProperty('value')) {
      box.value = val.value;
      visualize(iscl);
    }
  } });
  box.addEventListener('input', () => { v.value.set(box.value); });
};

let cmp;
let thing;
let eqc;

window.onload = () => {

  let cmp = hd.component`
   var  wi = 600, wr = 100, wa, 
        hi = 400, hr = 100, ha, ratio;`;

  let relc = hd.component`
   var &i, &r, &a;

   constraint C {
     m1(i, r -> a) => i * r;
     m2(i, a -> r) => i / a;
   }`;

  eqc = hd.component`
      var  &a, &b;
      constraint Eq {
        m1(a -> b) => a;
        m2(b -> a) => b;
      }`;
  eqc.vs.a = cmp.vs.wr;
  eqc.vs.b = cmp.vs.hr;
  
  let wcmp = relc.clone("w");
  let hcmp = relc.clone("h");

  cmp.addComponent(wcmp);
  cmp.addComponent(hcmp);
  wcmp.vs.i = cmp.vs.wi;
  wcmp.vs.r = cmp.vs.wr;
  wcmp.vs.a = cmp.vs.wa;
  hcmp.vs.i = cmp.vs.hi;
  hcmp.vs.r = cmp.vs.hr;
  hcmp.vs.a = cmp.vs.ha;
  
  cmp.connectSystem(iscl);
        
  iscl.update();
  
  textBinder(document.getElementById("wi"), cmp.vs.wi);
  textBinder(document.getElementById("wa"), cmp.vs.wa);
  textBinder(document.getElementById("wr"), cmp.vs.wr);

  textBinder(document.getElementById("hi"), cmp.vs.hi);
  textBinder(document.getElementById("ha"), cmp.vs.ha);
  textBinder(document.getElementById("hr"), cmp.vs.hr);

  checkboxBinder(document.getElementById("ratio"), cmp.vs.ratio);

  initvis(iscl);
  thing = function() {
    let k = 0;
    iscl.scheduleCommand([cmp.vs.wr.value], [cmp.vs.wr.value], v => [v * 2]);
  }
}

var data;

function initvis(cs) {
  let [nodes, edges] = hd.constraintSystemInVisNodesAndEdges(cs);

  // create a network
  var container = document.getElementById('mynetwork');
  data = {
    nodes: new vis.DataSet(nodes),
    edges: new vis.DataSet(edges)
  };
  var options = {
  };
  
  var network = new vis.Network(container, data, options);
}

function visualize(cs) {
  let [nnodes, nedges] = hd.constraintSystemInVisNodesAndEdges(cs);
  var ndata = {
    nodes: new vis.DataSet(nnodes),
    edges: new vis.DataSet(nedges)
  };
  data.nodes.forEach(n => {
    if (ndata.nodes.get(n.id) == null || true) {
      data.nodes.remove(n.id);
    }
      
  });
  ndata.nodes.forEach(n => {
    if (data.nodes.get(n.id) == null) {
      data.nodes.add(n);
    }
  });
  data.edges.forEach(n => {
    if (ndata.edges.get(n.id) == null || true) {
      data.edges.remove(n.id);
    }
  });
  ndata.edges.forEach(n => {
    if (data.edges.get(n.id) == null) {
      data.edges.add(n);
    }      
  });
}


