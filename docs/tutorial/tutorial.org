#+TITLE: Introduction to HotDrink
#+LANGUAGE:  en
#+OPTIONS: H:3 author:nil creator:nil toc:nil
#+HTML_LINK_HOME: http://hotdrink.github.io/hotdrink/
#+HTML_LINK_UP: file:index.html
#+HTML_HEAD_EXTRA: <script type="text/javascript" src="../../dist/hotdrink.js"></script>
#+HTML_HEAD_EXTRA: <script type="text/javascript" src="collapse.js"></script>
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="style.css"/>
#+INCLUDE: setup.org
#+MACRO: scraplink [[$1][~<<$1>>~]]

#+NAME: expand-noweb
#+BEGIN_SRC elisp :exports none
  (save-excursion
    (org-babel-goto-named-src-block name)
    (org-babel-expand-noweb-references))
#+END_SRC

HotDrink is a JavaScript library for implementing GUIs declaratively.
With HotDrink, the programmer specifies relationships (as sets of
functions) between pieces of data in the GUI, instead of event
handlers that control what should happen and when. HotDrink decides
when to enforce which relationships, and using which functions.

This document gives an overview of HotDrink and how one programs GUIs
with it.

* Basic concepts

** Running example
   :PROPERTIES:
   :CUSTOM_ID: sec:running-example
   :END:

#+HTML: <div class="whap-gui">
#+INCLUDE: "tangle/intro.html" export html
#+HTML: </div>
#+HTML: <script type="text/javascript">
#+INCLUDE: "tangle/intro.js" export html
#+HTML: </script>

In the adjacent simple GUI, four textboxes hold values of the /area/,
/width/, /height/, and /perimeter/ of a rectangle. The GUI makes sure
that the values are consistent, i.e., drawn from the same
rectangle. If the user modifies any of the values, the GUI responds by
modifying others to re-establish consistency. 

** Architecture of a HotDrink GUI 

GUIs written with HotDrink can be viewed as instances the
/Model-View-ViewModel/ (a.k.a. MVVM) design pattern.  This pattern
divides the application as follows:

- The /View/ is responsible for presenting data to the user and
  capturing events from user interaction. The four textboxes comprise the
  view in the example GUI.

- The /ViewModel/ manages all state presented in the view.  In HotDrink
  applications, the view-model is a /dataflow constraint system/.

- The /bindings/ connect elements in the view with variables in the
  view-model: they (1) detect user events in the view and send
  requests to change the view-model's variables, and (2) deliver new
  variable values from the view-model to the view.

- The /Model/ is responsible for the ``business-logic'' and is usually
  not aware of the user interface. The model is outside the scope of
  HotDrink. 

** Constraint systems

At the heart of a ``HotDrink-powered'' GUI is a /multi-way dataflow
constraint system/.  A constraint system consists of /variables/ and
/constraints/, where each constraint represents a relation over some
of the variables. A /constraint solver/ can compute a valuation for
the variables, such that all constraints are satisfied.

For example, given three variables, $A$, $w$, and $h$ (for area,
width, and height of a rectangle), a constraint could represent the
relation $A = w h$.  Given one more variable $p$ (for perimeter),
another constraint could represent the relation $p = 2(w + h)$. This
constraint system of four variables and two constraints, let's name it
as the /whap/ system, is the view-model of the example GUI.  Figure
[[fig:whap-relations]]  shows a graph representation of the /whap/ system.
This bipartite graph's two vertex types are variables and constraints.

#+begin_src dot :file whap-relations.svg :cmd neato :cmdline -Kdot -Tsvg 
  graph whap {
    rankdir=LR; ranksep=1;
    node [shape=box,fixedsize=true,width=0.5]; A; w; h; p;
    node [shape=pentagon,fixedsize=true,width=1,height=1,label=""]; 
    Awh [label="A=wh"]; whp [label="p=2(w+h)"];

   { rank=same; A }
   { rank=same; Awh }
   { rank=same; w; h }
   { rank=same; whp  }
   { rank=same; p }

    A -- Awh;
    Awh -- {w h};

    w -- whp;
    h -- whp;
    whp -- p;

    overlap=false
    fontsize=12;
  }
#+end_src

#+CAPTION: A multi-way dataflow constraint system of four variables ($A$, $w$, $h$, and $p$) and two constraints.
#+LABEL: fig:whap-relations
#+RESULTS:
[[file:whap-relations.svg]]

A constraint represents a relation of its variables. In a multi-way
dataflow constraint the relation is implemented as a set of functions.
These functions are called /methods/ (short for /constraint
satisfaction methods/). Each method uses one set of the constraint's
variables as inputs and computes new values for another, possibly
overlapping, set of variables.  The expectation is that after
executing any of the methods, the variables satisfy the constraint's
relation. We say that a method /enforces/ a constraint.

It is up to the programmer to decide how to break a constraint's
relation into methods---the decomposition can be arbitrary, except
that the output variables of one method cannot be a subset of those of
another. In modeling the /whap/ system, we choose to represent
the first constraint with three methods
  - $m_1: p \leftarrow 2 (w+h)$, 
  - $m_2: h \leftarrow p/2 - w$, and
  - $m_3: w \leftarrow p/2 - h$; 
and the second constraint with two methods 
  - $n_1: A \leftarrow wh$ and 
  - $n_2: \langle w, h \rangle \leftarrow \langle\sqrt A, \sqrt A \rangle$.

This view of constraints as a set of methods also gives rise to a
bipartite graph---now it is variables and methods that are the two
vertex types, and edges indicate the input and output variables of
methods. Figure [[fig:whap-graph]] shows the graph of the /whap/ system.

#+begin_src dot :file area-w-h-perimeter.svg :cmd neato :cmdline -Kdot -Tsvg 
  digraph whap {
    rankdir=LR; ranksep=1;
    node [shape=box]; A; w; h; p;
    node [shape=circle,fixedsize=true,width=0.3,label=""]; 
    A2wh [label="n1"]; wh2A [label="n2"]; ph2w [label="m3"]; pw2h [label="m2"]; wh2p [label="m1"];

   { rank=same; A }
   { rank=same; A2wh; wh2A }
   { rank=same; w; h }
   { rank=same; ph2w; pw2h }
   { rank=same; wh2p }
   { rank=same; p }

    A -> A2wh [style=dotted];
    A2wh -> {w h};

    w -> wh2A [style=dotted]; 
    h -> wh2A [style=dotted]; 
    wh2A -> A;

    w -> wh2p [style=dotted];
    h -> wh2p [style=dotted];
    wh2p -> p;

    p -> pw2h [style=dotted, constraint=false];
    w -> pw2h [style=dotted];
    pw2h -> h [constraint=false];

    p -> ph2w [style=dotted, constraint=false];
    h -> ph2w [style=dotted];
    ph2w -> w [constraint=false];

    overlap=false
    fontsize=12;
  }
#+end_src

#+CAPTION: A multi-way dataflow constraint system on variables $A$, $w$, $h$, and $p$. For better readability, methods' inputs are marked with dashed and outputs with solid edges.
#+LABEL: fig:whap-graph
#+RESULTS:
[[file:area-w-h-perimeter.svg]]

*** Solving a multi-way dataflow constraint system

Solving a multi-way dataflow constraint system means executing one
method from each constraint, so that all constraints become enforced.
Determining which methods to execute and in which order is called
/planning/. 

Planning consists of finding a subgraph of the method-variable graph
that (1) contains exactly one method vertex from each constraint, (2)
is acyclic, and (3) has no variable vertex with more than one
in-edge. A /plan/ is a topological ordering of the method vertices of
this subgraph. 

When methods are executed in the order specified by the plan,
constraints that have already been enforced will not be invalidated by
methods executed later.

Planning determines a /dataflow/.  There may be many dataflows that
could enforce a system's constraints. Table [[fig:whap-dataflows]] shows
all four possible dataflows of the /whap/ system. HotDrink's planner
algorithm selects a dataflow that is (intended to be) the least
surprising to the user. Slightly simplifying, the planner favors
dataflows that preserve the values of variables that the user has
changed most recently.
# TODO: add ref to where details are

#+NAME: awh-flow-1
#+begin_src dot :file area-w-h-perimeter-flow-1.svg :cmd neato :cmdline -Kdot -Tsvg :results none
  digraph whap {
    size="3,2";
    rankdir=LR; ranksep=1;
    node [shape=box]; A; w; h; p;
    node [shape=circle,fixedsize=true,width=0.2,label=""]; 
    A2wh; wh2A [color=lightgray]; ph2w [color=lightgray]; pw2h [color=lightgray]; wh2p;

   { rank=same; A }
   { rank=same; A2wh; wh2A }
   { rank=same; w; h }
   { rank=same; ph2w; pw2h }
   { rank=same; wh2p }
   { rank=same; p }

    A -> A2wh [style=solid];
    A2wh -> {w h};

    w -> wh2A [style=solid, color=lightgrey]; 
    h -> wh2A [style=solid, color=lightgrey]; 
    wh2A -> A [color=lightgrey];

    w -> wh2p;
    h -> wh2p;
    wh2p -> p;

    edge [color=lightgrey];
    p -> pw2h [style=solid,constraint=false];
    w -> pw2h [style=solid];
    pw2h -> h [constraint=false];

    p -> ph2w [style=solid,constraint=false];
    h -> ph2w [style=solid];
    ph2w -> w [constraint=false];

    overlap=false
    fontsize=12;
  }
#+end_src

#+NAME: awh-flow-2
#+begin_src dot :file area-w-h-perimeter-flow-2.svg :cmd neato :cmdline -Kdot -Tsvg :results none
  digraph whap {
    size="3,2";
    rankdir=LR; ranksep=1;
    node [shape=box]; A; w; h; p;
    node [shape=circle,fixedsize=true,width=0.2,label=""]; 
    A2wh [color=lightgray]; wh2A; ph2w [color=lightgray]; pw2h; wh2p [color=lightgray];

   { rank=same; A }
   { rank=same; A2wh; wh2A }
   { rank=same; w; h }
   { rank=same; ph2w; pw2h }
   { rank=same; wh2p }
   { rank=same; p }

    A -> A2wh [color=lightgrey];
    A2wh -> {w h} [color=lightgrey];

    w -> wh2A; 
    h -> wh2A; 
    wh2A -> A;

    w -> wh2p [color=lightgrey];
    h -> wh2p [color=lightgrey];
    wh2p -> p [color=lightgrey];

    p -> pw2h [constraint=false];
    w -> pw2h;
    pw2h -> h [constraint=false];

    edge [color=lightgrey];
    p -> ph2w [constraint=false];
    h -> ph2w;
    ph2w -> w [constraint=false];

    overlap=false
    fontsize=12;
  }
#+end_src

#+NAME: awh-flow-3
#+begin_src dot :file area-w-h-perimeter-flow-3.svg :cmd neato :cmdline -Kdot -Tsvg :results none
  digraph whap {
    size="3,2";
    rankdir=LR; ranksep=1;
    node [shape=box]; A; w; h; p;
    node [shape=circle,fixedsize=true,width=0.2,label=""]; 
    A2wh [color=lightgray]; wh2A; ph2w [color=lightgray]; pw2h [color=lightgray]; wh2p;

   { rank=same; A }
   { rank=same; A2wh; wh2A }
   { rank=same; w; h }
   { rank=same; ph2w; pw2h }
   { rank=same; wh2p }
   { rank=same; p }

    A -> A2wh [color=lightgrey];
    A2wh -> {w h} [color=lightgrey];

    w -> wh2A; 
    h -> wh2A; 
    wh2A -> A;

    w -> wh2p;
    h -> wh2p;
    wh2p -> p;

    edge [color=lightgrey];
    p -> pw2h [constraint=false];
    w -> pw2h;
    pw2h -> h [constraint=false];

    p -> ph2w [constraint=false];
    h -> ph2w;
    ph2w -> w [constraint=false];

    overlap=false
    fontsize=12;
  }
#+end_src

#+NAME: awh-flow-4
#+begin_src dot :file area-w-h-perimeter-flow-4.svg :cmd neato :cmdline -Kdot -Tsvg :results none
  digraph whap {
    size="3,2";
    rankdir=LR; ranksep=1;
    node [shape=box]; A; w; h; p;
    node [shape=circle,fixedsize=true,width=0.2,label=""]; 
    A2wh [color=lightgray]; wh2A; ph2w; pw2h [color=lightgray]; wh2p [color=lightgray];

   { rank=same; A }
   { rank=same; A2wh; wh2A }
   { rank=same; w; h }
   { rank=same; ph2w; pw2h }
   { rank=same; wh2p }
   { rank=same; p }

    A -> A2wh [color=lightgrey];
    A2wh -> {w h} [color=lightgrey];

    w -> wh2A; 
    h -> wh2A; 
    wh2A -> A;

    edge [color=lightgrey];
    w -> wh2p;
    h -> wh2p;
    wh2p -> p;

    p -> pw2h [constraint=false];
    w -> pw2h;
    pw2h -> h [constraint=false];

    edge [color=black];
    p -> ph2w [constraint=false];
    h -> ph2w;
    ph2w -> w [constraint=false];

    overlap=false
    fontsize=12;
  }
#+end_src

# Results collected here manually to be able to place images inline

#+CAPTION: The dataflows of the /whap/ constraint system.
#+LABEL: fig:whap-dataflows
#+ATTR_HTML: :rules none :frame void :style margin-left:auto; margin-right:auto;
| [[file:area-w-h-perimeter-flow-1.svg]] | [[file:area-w-h-perimeter-flow-2.svg]] |
| [[file:area-w-h-perimeter-flow-3.svg]] | [[file:area-w-h-perimeter-flow-4.svg]] |

*** Reacting to user events

In a HotDrink-powered user interface, events from the view to modify
GUI state are translated to requests to change a value of a constraint
system's variable. Changing a variable's value invalidates all the
constraints that the variable belongs to. HotDrink responds by solving
the constraint system: it determines which constrains may have to be
re-enforced, finds a plan, and executes the plan's methods. If a
variable gets a new value during solving, HotDrink notifies views
that are observing the variable, so that they can be updated.

# ** Commands

# /Commands/ are procedures that observe and modify
# the variable values in HotDrink's constraint system. After
# executing each command, HotDrink solves the constraint system. Both
# commands and methods can be asynchronous. HotDrink coordinates their
# execution so that consistency is preserved.

* Implementing the running example

This section describes the implementation of the /whap/ GUI above.  We
recognize that the GUI is in many respects unsatisfactory: fields
allow negative lengths, non-numeric input is not filtered out, etc. We
accept these shortcomings for now to keep the code simple and the
focus on the features of HotDrink.

** Importing HotDrink 

To use HotDrink on a page, import the [[file:hotdrink.min.js][=hotdrink.min.js=]] script (that
comes with this tutorial).  Concretely, add the following to the
~<head>~ section of your HTML, and modify the path in the ~src~
attribute as needed.

#+NAME: import-hotdrink 
#+BEGIN_SRC emacs-lisp :var hdpath="hotdrink.min.js" :exports none
(format "<script type=\"text/javascript\" src=\"%s\"></script>" hdpath)
#+END_SRC

#+BEGIN_SRC html :noweb yes
<<import-hotdrink()>>
#+END_SRC

To avoid conflicts with other libraries, HotDrink exports only one
global symbol, ~hd~. 

# All of its interactions with the DOM are via
# cooperative API functions, such as [[https://developer.mozilla.org/en-US/docs/Web/API/EventTarget.addEventListener][~addEventListener~]].

** Specifying the constraint system

The /whap/ constraint system is specified as follows.  

#+CAPTION: whap-component
#+NAME: whap-component
#+BEGIN_SRC js
let whap = hd.component`
  component whap {
    var A=100, w, h, p;
    constraint pwh {
      m1(w, h -> p) => 2*(w+h);
      m2(p, w -> h) => p/2 - w;
      m3(p, h -> w) => p/2 - h;
    }
    constraint Awh {
      n1(w, h -> A) => w*h;
      n2(A -> w, h) => [Math.sqrt(A), Math.sqrt(A)];
    }
  }
  `
#+END_SRC

The ~hd.component~ template literal defines a domain-specific
language for specifying a /component/ of a constraint system. A
component is HotDrink's abstraction for grouping variables and
constraints, so that they can be added and removed from a constraint
system as one unit.

The ~var~ statement declares variables; initializers are optional.
Here we initialize the variable ~A~. Since ~A~ is declared first, it
gets the highest /priority/, which guides the planner to select a
dataflow where ~A~ determines all the other variables.  The other
variables will thus be assigned values when the system is solved for
the first time.

Each ~constraint~ declaration specifies a group of methods.
Constraints and methods may be given names. 
Constraint names must be unique within a component and method names unique
within a constraint. 

The signature of a method specifies its input and output structure.
For example, ~m1(w, h -> p)~ specifies that the method ~m1~ has two
inputs, ~w~ and ~h~, and one output, ~p~. The body of the method, any
JavaScript expression or block statement that can serve as a function
body, follows after ~=>~.  The input variables (here ~w~ and ~h~) are
in scope in the method body. The body can also be a /placeholder/ that
denotes a JavaScript function with appropriate input and output
arities. For example, the ~m1~ method could alternatively be written
as:

#+BEGIN_SRC js
  m1(w, h -> p) => ${(w, h) => 2*(w+h)};
#+END_SRC

Here the expression inside ~${ ... }~ is an ordinary JavaScript arrow
function.

Methods that have more than one output variable return an array, one
element for each output. For example, the method ~n2~, whose signature
is ~n2(A -> w, h)~, returns the values to be assigned to ~w~ and ~h~
in a two-element array.

** The View 

HotDrink does not dictate how the View of an application 
is implemented; we use HTML here.

#+CAPTION: whap-view
#+NAME: whap-view
#+BEGIN_SRC html -n -r :exports code :tangle tangle/intro.html :padline no :noweb strip-export
  <<whap-css>>
  <fieldset id="whap-gui">
    <label for="A">A</label><input id="A" type="text"></input>

    <span class="vertical-middle">
      <label for="w">w</label><input id="w" type="text"/></br>
      <label for="h">h</label><input id="h" type="text"/>
    </span>

    <label for="p">p</label><input id="p" type="text"/>
  </fieldset>
#+END_SRC

To line the widgets nicely, we add some styling:

#+CAPTION: whap-css
#+NAME: whap-css
#+BEGIN_SRC html -n -r :exports code 
  <style type="text/css">
    fieldset#whap-gui { background-color: white }
    fieldset#whap-gui input[type="text"] { width: 8ex; margin-right: 2ex }
    fieldset#whap-gui label { display:inline-block; width: 2ex; } 
    .vertical-middle { display:inline-block; vertical-align:middle; } 
  </style>
#+END_SRC

** ViewModel

The view-model is a ~ConstraintSystem~ object. 
The code below constructs a new constraint system ~cs~ and
adds to it the constraints of the ~whap~ component 
(that was defined in the {{{scraplink(whap-component)}}} block).

#+NAME: construct-property-model
#+BEGIN_SRC js :noweb yes
var cs = new hd.ConstraintSystem();
cs.addComponent(whap);
cs.update();
#+END_SRC

The constraint system ~cs~ now manages the view-model's state.  The
call to ~update()~ triggers the solver, which enforces the constraints
$A = w h$ and $p = w + h$ using the plan ~n2, m1~, in which ~A~
determines all other variables.

# FIXME: refer to where priorities are explained

** Accessing variables

Access to a variable is through the component that contains it. For
example, the ~w~ variable is accessed as ~whap.variables.w~.  There is
no API for accessing variables via a constraint system.

** Variable state

A variable's state is a list of /variable activations/, which are
wrappers over JavaScript /promises/. We call the list of activations as the
variable's history.  To give a variable a new value means adding an
activation to the history, and waiting for its promise to be settled.

The most recent activation whose promise has been settled determines the
variable's value. If that promise was resolved, the variable's value
is the /resolved value/. If that promise was rejected, the variable's 
error value is the /rejection reason/.

A variable is /pending/ if the newest activation's promise is pending.

** Variables are observables

A variable is an /observable/.  /Observers/ can /subscribe/ to receive
events whenever a variable's state changes.  Assuming ~T~ is the type
of the values a variable holds, the event type is ~{ value?: T~,
~error?: any~, ~pending?: boolean }~; at least one of the properties
is defined.

The details of event types and when events are sent are as follows. 

  |------------------------------------+----------------------------------------------------------------|
  | Event type                         | When event is sent                                             |
  |------------------------------------+----------------------------------------------------------------|
  | ~{ value: T }~                     | Variable gets a new value.                                     |
  | ~{ value: T, pending: boolean }~   | Variable gets a new value and pending state changes.           |
  | ~{ error: any }~                   | Variable gets a new error value.                               |
  | ~{ error: any, pending: boolean }~ | Variable gets a new error value and the pending state changes. |
  | ~{ pending: true }~                | Variable becomes pending.                                      |
  |------------------------------------+----------------------------------------------------------------|

In addition to these events that are sent to all observers, when an
observer subscribes to a variable it is sent an event of either the
type ~{ value: T, pending: boolean }~ or ~{ error: any, pending:
boolean }~.

Events only contain those properties that have changed.  The only
exceptions are if a variable is resolved to the same value or rejected
with the same error value it already has.

*** Subscribing to variables

The ~subscribe~ method registers an observer to variable's state changes.
Below we subscribe an observer that displays the current value of the variable ~p~ in this box 
call_expand-noweb(name="p-display-1")[:wrap html]{{{results(@@html:<span id="p-span" style="border: 1px solid green;">?</span> @@)}}} (modify the values in the [[id:sec:running-example][example GUI]] to see the value change).
The box is a simple ~span~ element:

#+NAME: p-display-1 
#+BEGIN_SRC html :exports code
  <span id="p-span" style="border: 1px solid green;">?</span> 
#+END_SRC

whose contents the observer changes:

#+NAME: p-subscribe
#+BEGIN_SRC js :exports code
  let pBox = document.getElementById("p-span");
  let pSubscription = whap.vs.p.subscribe(e => { if ('value' in e) pBox.textContent = e.value; });
#+END_SRC

#+RESULTS: p-subscribe

#+NAME: p-subscribe-script
#+BEGIN_SRC html :exports none
  <script>
    <<p-subscribe>>
  </script>
#+END_SRC

#+CALL: expand-noweb(name="p-subscribe-script") :exports results raw :wrap export html

#+RESULTS:
#+BEGIN_export html
<script>
  let pBox = document.getElementById("p-span");
  let pSubscription = whap.vs.p.subscribe(e => { if ('value' in e) pBox.textContent = e.value; });
</script>
#+END_export

The return value of subscribing is a /subscription/ object.  It has an
~unsubscribe~ method for canceling the subscription.
Click @@html:<button
onclick="pSubscription.unsubscribe()">Unsubscribe</button>@@ to run the
unsubscribing code below.

#+NAME: p-unsubscribe
#+BEGIN_SRC js :exports code
  pSubscription.unsubscribe();
#+END_SRC

** Updating the value of a variable

As mentioned above, a variable maintains a history of activations,
essentially /promise/ objects. To give a new value to a variable means
adding a new promise to the history. Promises are added either by the constraint system
or by client code. Clients add promises with the ~set~ method. 
For example, the function below adds a new promise to ~w~ variable's history, and resolves it to
the value ~9~. @@html:<button
onclick="set_w()">Run the code</button>@@

#+NAME: w-set
#+BEGIN_SRC js :exports code
  function set_w() { whap.variables.w.set(9); }
#+END_SRC

#+NAME: w-set-script
#+BEGIN_SRC html :exports none
  <script>
    <<w-set>>
  </script>
#+END_SRC

#+CALL: expand-noweb(name="w-set-script") :exports results raw :wrap export html

#+RESULTS:
#+BEGIN_export html
<script>
  function set_w() { whap.variables.w.set(9); }
</script>
#+END_export

Calling ~set~ solves the constraint system and thus values of other variables may change too.
If they do, change events are sent to their observers (after ~set~ returns).

# The most recent promise can be
# obtained with the ~currentPromise~ getter:

# #+name: try-current-promise :exports code
# #+BEGIN_SRC js 
#   whap.variables.w.currentPromise.then(
#     v => window.alert(`w's current value is ${v}`)
#   );
# #+END_SRC


# #+NAME: try1
# #+BEGIN_SRC js :exports none
#   <script>
#     function addObserverHandler() {
#       <<try-current-promise>>
#     }
#   </script>
#   <button onclick="addObserverHandler()">Try</button>
# #+END_SRC

# #+CALL: expand-noweb(name="try1") :exports results raw :wrap export html

# #+RESULTS:
# #+BEGIN_export html
# <script>
#   function addObserverHandler() {
#     whap.variables.w.currentPromise.then(
#       v => window.alert(`w's current value is ${v}`)
#     );
#   }
# </script>
# <button onclick="addObserverHandler()">Try</button>
# #+END_export

** Bindings --- Connecting the View and the ViewModel

Knowing how to observe variables and how to assign to them, we can create /bindings/
between the view and the view-model. HotDrink will eventually offer a library of
useful binder functions---here we show the mechanism in its basic
form.

We first define the function ~textBinder~ that takes two parameters, a
DOM element (a textbox) ~box~ and a variable ~v~ in a constraint system,
and binds them so that a change in either will be reflected in the
other. The function thus creates a /two-way/ binding. 

#+NAME: simple-two-way-textbox-binder
#+BEGIN_SRC js
  function textBinder(box, v) {
    v.value.subscribe(val => { if ('value' in val) box.value = val.value; });
    box.addEventListener('input', () => { v.value.set(parseInt(box.value)); });
  };
#+END_SRC

The observer of ~v~ modifies the DOM object ~box~ whenever ~v.value~
changes.  The event handler for the ~input~ events of ~box~ sets ~v~
whenever ~box~ is edited.

Calling ~textBinder~ with all four textbox-variable pairs creates the
desired bindings in the example GUI.

#+CAPTION: create-four-binders
#+BEGIN_SRC js :noweb-ref create-four-binders
  textBinder(document.getElementById("A"), whap.vs.A);
  textBinder(document.getElementById("w"), whap.vs.w);
  textBinder(document.getElementById("h"), whap.vs.h);
  textBinder(document.getElementById("p"), whap.vs.p);
#+END_SRC

To point out an obvious issue, the calls to ~textBinder~
should only be issued after the DOM is available.

#+NAME: on-load
#+BEGIN_SRC js -n -r :exports code :padline no :noweb no-export
  window.addEventListener('load', function() {
    <<create-four-binders>>
  });
#+END_SRC

#+RESULTS: on-load

This completes the GUI implementation.

#+BEGIN_SRC html :exports none :padline no :noweb yes :tangle whap.html
  <html>
    <head>
      <<import-hotdrink("../../scripts/hotdrink.js")>>
      <script>
        <<whap-component>>
        <<construct-property-model>>
        <<simple-two-way-textbox-binder>>
        <<on-load>>
      </script>
    </head>
    <body>
      <<whap-view>>
    </body>
  </html>
#+END_SRC

#+BEGIN_SRC js -n -r :exports none :padline no :noweb yes :tangle tangle/intro.js
// Define a constraint system (variables and constraints) as a component
<<whap-component>>

// Define the property model and add its components
<<construct-property-model>>

<<simple-two-way-textbox-binder>>

<<on-load>>
#+END_SRC

** COMMENT Commands

Property model's variables can be written to with ~set~, and they can
be read from with observers or from the variable's promise. 
They are both special cases of /commands/.  A command can both read a property model's
variables and write to them, in one atomic operation. (Thus ~set~ is a
command that does no reading, and calls to observers are commands that
do no writing).

An example illustrates. We define a command named ~double~ that scales
the rectangle by a factor of two, and add it to the ~pm~ property
model. It reads both ~w~ and ~h~, computes new values for them, to 
be assigned back to ~w~ and ~h~ again.

This example also illustrates how one component refers to
variables in other components. For this we need to define a
/reference/ member in the component, bind that to another component,
and then refer to variables through the reference using dot notation.

FIXME: component builder seems to not create the command if it uses 
references (r.w, etc.)
QUESTION: does it matter if we first connect r to whap, then add to pm, or vice versa

#+NAME: command
#+BEGIN_SRC js
  let com = hd.pcnew hd.ComponentBuilder()
      .reference('r')
      .command('double', '!r.w, !r.h -> r.w, r.h', (w, h) => [2*w, 2*h])
      .component();

  com.r = whap; // connect the reference to whap
  pm.addComponent(com);

  document.getElementById("double").addEventListener("click", () => com.double.activate());
#+END_SRC



#  <button id="double">Double</button>



# In many ways, a command is similar to an event handler.  However, whereas an
# event handler represents something that happened in the View, a command
# represents something that happened in the View-Model.  For example, a
# key-press event is generated when the user presses a key.  That event is
# translated by a binding into a variable-set command.

# Most commands in this example are, in fact, variable-set commands issued by
# bindings.  These commands are created automatically; no extra work is
# required.  If you want to create your own custom commands, however, you may do
# so using the component builder.


# This command can be bound to events, like button presses.  Commands can also
# be used to modify variables of the property model, or event modify the
# property model itself, adding or removing variables, constraints, etc.

#  LocalWords:  HotDrink javascript src js stylesheet css href INFOJS sdepth
#  LocalWords:  le HotDrink's multi ldquo rdquo MVVM ViewModel html JQuery hd
#  LocalWords:  Dojo MooTools DOM API addEventListener padline noweb th td
#  LocalWords:  checkin oneDayMs setHours ComponentBuilder propmod num getTime
#  LocalWords:  ModelBuilder PropertyModel addComponent removeComponent
#  LocalWords:  performDeclaredBindings toc scraplink GUIs dataflow


# Changes to GUI state are managed through /commands/ written by the programmer.
# HotDrink ensures that (1) commands are initiated only when all relationships
# defined by the programmer have been enforced, and (2) commands will not
# interfere with one another, at least with respect to the GUI state.  This
# means that your program will behave correctly even if a new command is issued
# before the previous command has finished.









** COMMENT Primary HotDrink objects

Important classes of HotDrink include:

1. A ~ConstraintSystem~ object represents the view-model.  It manages
   a hierarchical dataflow constraint system. In the simplest case, an
   application has a single constraint system that maintains all data
   relevant for the view.

2. A ~Variable<T>~ object represents a variable of type ~T~.

3. A ~VariableReference<T>~ object represents a reference to a
   variable of type ~T~.

4. A ~Constraint~ 
2. A ~Component~ object is a collection of elements of a constraint system:
   variables, constraints, and (nested) components.  Components are
   a grouping mechanism that allow several elements to be
   added to or removed from a constraint system in one operation.

3. A ~Variable<T>~ object represents a variable. 

4. A ~VariableReference<T>~ object ...

3. A ~ComponentBuilder~ object with its member functions is a language
   for constructing components.  By chaining method calls of a
   ~ComponentBuilder~ object the programmer defines a component's
   elements.  The constraint system specification in the
   {{{scraplink(whap-component)}}} source block is written using the
   component builder's language.

* File local variables                                     :noexport:
  
# Local Variables:
# mode: org
# org-html-postamble: nil
# org-babel-use-quick-and-dirty-noweb-expansion: t
# org-confirm-babel-evaluate: nil
# End:
#  LocalWords:  declaratively whap gui fn leftarrow wh sqrt png cmd
#  LocalWords:  neato cmdline Kdot Tpng rankdir ranksep fixedsize ph
#  LocalWords:  pw fontsize cb acyclic dataflows awh lightgray ATTR
#  LocalWords:  lightgrey hotdrink emacs hdpath HotDrinked fieldset
#  LocalWords:  br textboxes getStagedPromise observables onNext
#  LocalWords:  onError onCompleted textBinder textbox addObserver
