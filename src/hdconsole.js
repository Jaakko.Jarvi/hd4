// @flow

import * as util from "./utilities";

export class HdConsole {
  _name: string;
  log: Function;
  warn: Function;
  error: Function;
  assert: Function;

  constructor(name: string) {
    this._name = name;
    this.off();
  }

  off() {
    this.log = (...args) => {};
    this.warn = (...args) => {};
    this.error = (...args) => {};
    this.assert = (...args) => {};
  }
  errorsOnly() {
    this.off();
    this.error = console.error;
    this.assert = console.assert;
  }
  on() {
    this.log = console.log;
    this.warn = console.warn;
    this.error = console.error;
    this.assert = console.assert;
  }
}

export var hdconsole: HdConsole = new HdConsole("HotDrink");
export class HdError extends Error {
  constructor(msg: string, ...params: Array<any>) {
    super(msg, ...params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, HdError);
    }

    this.name = "HdError";
  }
}
