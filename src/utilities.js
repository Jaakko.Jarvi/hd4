// [[file:../org/utilities.org::*Tangle][Tangle:1]]
// @flow

import { hdconsole } from "./hdconsole.js";
import * as Obs from "./observable.js";

// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function equals<T>(a: T, b: T): boolean {
  a = (a: any);
  b = (b: any);

  if (a === b) return true;

  if (Array.isArray(a) && Array.isArray(b)) {
    if (a.length !== b.length) return false;
    for (let i = 0; i < a.length; ++i) if (!equals(a[i], b[i])) return false;
    return true;
  }

  if (a instanceof Set && b instanceof Set) return orderedSetEquals(a, b);
  // Equality of keys is by ===, not by equals

  if (a instanceof Map && b instanceof Map) return orderedMapEquals(a, b);

  if (typeof a === "object" && a !== null) {
    // for any other kind of object (even function object)
    // find .equals method if one exists

    if (typeof a.equals === "function") return a.equals(b);
    else if (b != null) {
      // otherwise recurse through all members
      let ak: any = Object.keys(a),
        bk: any = Object.keys(b);
      if (ak.length !== bk.length) return false;

      for (let m of ak) if (!equals(a[m], b[m])) return false;

      return true;
    }
  }
  if (typeof a === "function" && typeof a.equals === "function")
    return a.equals(b);
  // by default functions are the same only if === says so; but if a function object has
  // the equals member, then that member determines equality

  return false;
}

// two ordered Sets are equal if they have the same keys in the same order
export function orderedSetEquals<Key, Value, M: Set<Key> | Map<Key, Value>>(
  s1: M,
  s2: M
): boolean {
  if (s1.size !== s2.size) return false;
  return bisame(s1, s2);
}

// two ordered Maps are equal if they have the same keys, in the same
// order, and for each key, values are equal
export function orderedMapEquals<Key, Value, M: Map<Key, Value>>(
  s1: M,
  s2: M
): boolean {
  if (s1.size !== s2.size) return false;
  return every2(
    ([key1, value1], [key2, value2]) => key1 === key2 && equals(value1, value2),
    s1,
    s2
  );
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
// two Sets are `set equal' if they have the same keys
export function setEquals<Key, Value, M: Set<Key> | Map<Key, Value>>(
  s1: M,
  s2: M
): boolean {
  if (s1.size !== s2.size) return false;
  for (let k of s1.keys()) if (!s2.has(k)) return false;
  return true;
}

// two Maps are `set equal' if they have the same keys and for each key, values are equal
export function mapEquals<Key, Value, M: Map<Key, Value>>(
  s1: M,
  s2: M
): boolean {
  if (s1.size !== s2.size) return false;
  for (let [k, v]: [Key, Value] of s1) {
    if (!equals(s2.get(k), v)) return false;
  }
  return true;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function copy<T>(a: T): T {
  return (_copy(a): T);
}
function _copy(a: any): any {
  if (Array.isArray(a)) return a.map((v) => copy(v));
  if (a instanceof Set) return new Set(a);
  if (a instanceof Map) return new Map(map(([k, v]) => [k, copy(v)], a));
  if (typeof a === "object" && a !== null) {
    if (typeof a.copy === "function") return a.copy();
    else {
      let b = Object.create(Object.getPrototypeOf(a));
      for (let m of Object.keys(a)) b[m] = copy(a[m]);
      return b;
    }
  }
  if (typeof a === "function" && typeof a.copy === "function") return a.copy();

  // here should be a value, or something that is shared by reference
  return a;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function subsetOf<Key, Value, M: Set<Key> | Map<Key, Value>>(
  sub: M,
  sup: M
): boolean {
  for (let k of sub.keys()) if (!sup.has(k)) return false;
  return true;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function modifyMapValue<Key, Value>(
  m: Map<Key, Value>,
  k: Key,
  fn: (?Value) => Value
): Value {
  let newv = fn(m.get(k));
  m.set(k, newv);
  return newv;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function setUnion<T>(s1: Iterable<T>, s2: Iterable<T>): Set<T> {
  let s = new Set();
  setUnionTo(s, s1);
  setUnionTo(s, s2);
  return s;
}

export function setUnionTo<T>(s1: Set<T>, s2: Iterable<T>): void {
  for (let v of s2) s1.add(v);
}

export function setUnionToWithDiff<T>(s1: Set<T>, s2: Iterable<T>): Set<T> {
  let diff = new Set();
  for (let v of s2)
    if (!s1.has(v)) {
      s1.add(v);
      diff.add(v);
    }
  return diff;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function setDifference<T>(s1: Set<T>, s2: Set<T>): Set<T> {
  let s = new Set();
  for (let v of s1) if (!s2.has(v)) s.add(v);
  return s;
}
export function setDifferenceTo<T>(s1: Set<T>, s2: Iterable<T>): void {
  for (let v of s2) s1.delete(v);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function* mkEmptyIterable<T>(): Iterable<T> {}
export function* mkSingletonIterable<T>(v: T): Iterable<T> {
  yield v;
}
export function mkEmptyIterator<T>(): Iterator<T> {
  // flowtype wants iterators to have @@iterator, which is an iterable's method,
  // and does not need to be in iterator. To appease flowtype,
  // we define a dummy in flowtype-comments
  return {
    /*:: @@iterator(): Iterator<T> { return ({}: any); }, */
    // $FlowFixMe: computed property

    next: () => ({ done: true, value: undefined }),
  };
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function iteratorToIterable<T, U, V>(
  it: $Iterator<T, U, V>
): $Iterable<T, U, V> {
  let v = {};
  v[Symbol.iterator] = function () {
    return it;
  };
  return (v: any);
  //    return ({ [Symbol.iterator]: () => it }: any);
}
export function iterableToIterator<T, U, V>(
  it: $Iterable<T, U, V>
): $Iterator<T, U, V> {
  return (it: any)[Symbol.iterator]();
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function map<T, U>(fn: (T) => U, iterable: Iterable<T>): Iterable<U> {
  return ({
    [Symbol.iterator]: () => {
      const iterator = (iterable: any)[Symbol.iterator]();
      let mapIterator = {};
      mapIterator.next = () => {
        const { done, value } = iterator.next();
        return { done, value: done ? undefined : fn(value) };
      };
      if (typeof iterator.return === "function") {
        mapIterator.return = (v) => {
          return iterator.return(v);
        };
      }
      if (typeof iterator.throw === "function") {
        mapIterator.throw = (e) => {
          return iterator.throw(e);
        };
        // assuming that if .throw throws, iterator relases resources, sets itself closed etc.
      }
      return mapIterator;
    },
  }: any);
}

export function filter<T>(
  fn: (T) => boolean,
  iterable: Iterable<T>
): Iterable<T> {
  return ({
    [Symbol.iterator]: () => {
      const iterator = (iterable: any)[Symbol.iterator]();
      let filterIterator = {};
      filterIterator.next = () => {
        let done = false;
        let value: any = undefined;
        do {
          ({ done, value } = iterator.next());
        } while (!done && !fn(value));
        return { done, value };
      };
      if (typeof iterator.return === "function") {
        filterIterator.return = (v) => {
          return iterator.return(v);
        };
      }
      if (typeof iterator.throw === "function") {
        filterIterator.throw = (e) => {
          return iterator.throw(e);
        };
        // assuming that if .throw throws, iterator relases resources, sets itself closed etc.
      }
      return filterIterator;
    },
  }: any);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function until<T>(fn: Function, iterable: Iterable<T>): Iterable<T> {
  return ({
    [Symbol.iterator]: () => {
      const iterator = (iterable: any)[Symbol.iterator]();
      let untilIterator = {};
      let step = () => iterator.next();
      untilIterator.done = false;
      untilIterator.next = () => {
        if (untilIterator.done) return { done: true };
        let { done, value } = step();
        step = () => iterator.next();
        if (done) {
          // normal closing of iterator: assuming it releases resources
          untilIterator.done = true;
          return { done: true };
        }
        done = fn(value);
        if (done) {
          if (typeof iterator.return === "function") iterator.return(); // call return to release resources
          return { done: true };
        } else {
          return { done: false, value: value };
        }
      };
      if (typeof iterator.return === "function") {
        untilIterator.return = (v) => {
          return iterator.return(v);
        };
      }
      if (typeof iterator.throw === "function") {
        untilIterator.throw = (e) => {
          step = () => iterator.throw(e);
          return untilIterator.next();
          // we do not catch exceptions and call iterator.return; assuming that
          // if iterator.throw throws, iterator releases resources
        };
      }
      return untilIterator;
    },
  }: any);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function every<T>(fn: (T) => boolean, iterable: Iterable<T>): boolean {
  for (let i of iterable) if (!fn(i)) return false;
  return true;
}

export function some<T>(fn: (T) => boolean, iterable: Iterable<T>): boolean {
  for (let i of iterable) if (fn(i)) return true;
  return false;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function forEach<T, U>(fn: (T) => U, iterable: Iterable<T>): (T) => U {
  for (let i of iterable) fn(i);
  return fn;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function adjacentFind<T, U, V>(
  cmp: (T, T) => boolean,
  iterable: $Iterable<T, U, V>
): ?[T, T] {
  let iter = iterableToIterator(iterable);
  let r = iter.next();
  if (r.done) return null;
  let prevVal: T = r.value;
  while (true) {
    let curr = iter.next();
    if (curr.done) return null;
    if (cmp(prevVal, curr.value)) {
      if (typeof (iter: any).return === "function") (iter: any).return(); // release resources
      // FIXME: how to convince flowtype that return may exist?
      return [prevVal, curr.value];
    } else {
      prevVal = curr.value;
    }
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function isSortedBy<T>(
  cmp: (T, T) => boolean,
  iterable: Iterable<T>
): boolean {
  return adjacentFind((a, b) => !cmp(a, b), iterable) === null;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function count<T>(iterable: Iterable<T>): number {
  let i = 0;
  for (let _ of iterable) ++i;
  return i;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function first<T>(iterable: Iterable<T>): ?T {
  const iterator = (iterable: any)[Symbol.iterator]();
  let r = iterator.next();
  if (r.done) return undefined;
  else {
    if (typeof iterator.return === "function") iterator.return();
    return r.value;
  }
}

export function firstKeepOpen<T>(iterable: Iterable<T>): ?T {
  let r = (iterable: any)[Symbol.iterator]().next();
  if (r.done) return undefined;
  else return r.value;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function rest<T>(iterable: Iterable<T>): Iterable<T> {
  let iterator = iterableToIterator(iterable);
  iterator.next();
  return iteratorToIterable(iterator);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function foldl<T, U>(fn: (T, U) => U, u: U, iterable: Iterable<T>): U {
  let acc = u;
  for (let t of iterable) acc = fn(t, acc);
  return acc;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function join<T>(it1: Iterable<Iterable<T>>): Iterable<T> {
  return ({
    [Symbol.iterator]: () => {
      let outerIt = iterableToIterator(it1);
      let innerIt;
      let innerDone = true;
      let allDone = false;

      // FIXME can one call next repeatedly after past the end?
      return {
        next: () => {
          if (allDone) return { done: true };
          //            let a: { value: Iterable<T>, done: false } | { done: true };
          //            let b: { value: T, done: false } | { done: true };
          let a: IteratorResult<Iterable<T>, void>;
          let b: IteratorResult<T, void>;

          while (true) {
            if (innerDone == true) {
              a = outerIt.next();
              if (a.done) return { done: true };
              innerIt = iterableToIterator(a.value);
            }
            b = innerIt.next();
            if (b.done) {
              innerDone = true;
              continue;
            }
            innerDone = false;
            return { done: false, value: b.value };
          }
        },
        return: (v) => {
          if (allDone) return { done: true, value: v };
          allDone = true;
          if (!innerDone && typeof (innerIt: any).return === "function") {
            // FIXME: how to express optional iterator properties in flowtype?
            (innerIt: any).return(v); // close the currently open inner iterator
          }
          return { done: true, value: v };
        },
        throw: (e) => {
          if (allDone) throw e;
          // FIXME: think how to deal with throw
        },
      };
    },
  }: any);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function zipWith<T, U, A>(
  fn: (T, U) => A,
  it1: Iterable<T>,
  it2: Iterable<U>
): Iterable<A> {
  let done = false;
  return ({
    [Symbol.iterator]: () => {
      const i1 = (it1: any)[Symbol.iterator](),
        i2 = (it2: any)[Symbol.iterator]();

      return {
        next: () => {
          if (done) return { done: true };
          const a = i1.next(),
            b = i2.next();
          done = a.done || b.done;
          if (!a.done && typeof i1.return === "function") i1.return();
          if (!b.done && typeof i2.return === "function") i2.return();
          if (done) return { done: true };
          else {
            return { done: false, value: fn(a.value, b.value) };
          }
        },
        return: (v) => {
          if (typeof i1.return === "function") i1.return();
          if (typeof i2.return === "function") i2.return();
          done = true;
          return { done: true, value: v };
        },
      };
    },
  }: any);
}

export function zip<T, U>(
  it1: Iterable<T>,
  it2: Iterable<U>
): Iterable<[T, U]> {
  return zipWith((a, b) => [a, b], it1, it2);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function bisimilar<T>(it1: Iterable<T>, it2: Iterable<T>): boolean {
  return every2((a, b) => equals(a, b), it1, it2);
}
export function bisame<T>(it1: Iterable<T>, it2: Iterable<T>): boolean {
  return every2((a, b) => a === b, it1, it2);
}
export function every2<T, U>(
  fn: (T, U) => boolean,
  it1: Iterable<T>,
  it2: Iterable<U>
): boolean {
  const i1 = (it1: any)[Symbol.iterator](),
    i2 = (it2: any)[Symbol.iterator]();
  while (true) {
    const a = i1.next(),
      b = i2.next();
    if (!a.done && !b.done) {
      if (!fn(a.value, b.value)) return false;
      else continue;
    }

    if (a.done && b.done) return true; // in this case i1 and i2 should have released resources
    if (!a.done) {
      if (typeof i1.return === "function") i1.return();
      return false;
    }
    if (!b.done) {
      if (typeof i2.return === "function") i2.return();
      return false;
    }
  }
  return true; // dead code; added to appease flowtype
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function sameElements<T>(a: Iterable<T>, b: Iterable<T>): boolean {
  let m = new Map();
  for (let k of a) modifyMapValue(m, k, (c) => (c == null ? 1 : c + 1));
  for (let k of b) {
    let c = m.get(k);
    if (c === 1) m.delete(k);
    else if (c === undefined) return false; // this closes b
    else m.set(k, c - 1);
  }
  return m.size === 0;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class OneToManyMap<Key, Value> {
  _m: Map<Key, Set<Value>>;

  constructor() {
    this._m = new Map();
  }

  hasKey(k: Key): boolean {
    return this._m.has(k);
  }

  keys(): Iterator<Key> {
    return this._m.keys();
  }

  values(k: Key): Iterable<Value> {
    let s = this._m.get(k);
    if (s !== undefined) return s.values();
    else return mkEmptyIterable();
  }

  countKeys(): number {
    return this._m.size;
  }

  count(k: Key): number {
    let s = this._m.get(k);
    return s === undefined ? 0 : s.size;
  }
  addKey(k: Key): OneToManyMap<Key, Value> {
    if (this._m.get(k) === undefined) this._m.set(k, new Set());
    return this;
  }
  add(k: Key, v: Value): OneToManyMap<Key, Value> {
    let s = this._m.get(k);
    if (s === undefined) {
      s = new Set();
      this._m.set(k, s);
    }
    s.add(v);
    return this;
  }
  addMany(k: Key, ite: Iterable<Value>): void {
    let s = this._m.get(k);
    if (s === undefined) {
      s = new Set();
      this._m.set(k, s);
    }
    // even with empty iterator, key is always added
    for (let v: Value of ite) s.add(v);
  }
  remove(k: Key, v: Value): boolean {
    let s = this._m.get(k);
    if (s === undefined) return false;
    else return s.delete(v);
  }
  removeKey(k: Key): void {
    this._m.delete(k);
  }
  clear(): void {
    this._m.clear();
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
let refCountMap: WeakMap<interface {}, number> = new WeakMap();
let disposerMap: WeakMap<interface {}, {}> = new WeakMap();
let uniqueHandlerMap: WeakMap<interface {}, interface {}> = new WeakMap();

// shall only be called after initRefCounting called
export function mkRefCounted<T: interface {}>(
  o: T,
  disposer: (T) => any,
  uniqueHandler?: (T) => any
): T {
  hdconsole.assert(
    refCount(o) == 0,
    "trying to make an object reference counted twice"
  );
  disposerMap.set(o, disposer);
  if (uniqueHandler != null) uniqueHandlerMap.set(o, uniqueHandler);
  refCountMap.set(o, 1);
  return o;
}

export function setUniqueHandler<T: interface {}>(
  o: T,
  uniqueHandler: (T) => any
): void {
  uniqueHandlerMap.set(o, uniqueHandler);
}

export function retain<T: interface {}>(o: T): T {
  let cnt = refCountMap.get(o);
  if (cnt == null) {
    hdconsole.assert(cnt != null, "trying to retain a non-refcounted object");

    //retain old behaviour (undefined + 1 === NaN)
    refCountMap.set(o, NaN); //TODO Should this be set to NaN? removing line seems better.
  } else {
    refCountMap.set(o, cnt + 1);
  }
  return o;
}

export function release<T: interface {}>(o: T): number {
  let cnt = (refCountMap.get(o): any);
  hdconsole.assert(
    cnt != null && cnt > 0,
    "trying to release an already released object"
  );
  --cnt;
  refCountMap.set(o, cnt);
  if (cnt > 1) return cnt;
  if (cnt === 1) {
    let uniqueHandler: (T) => any = (uniqueHandlerMap.get(o): any);
    if (uniqueHandler != null) uniqueHandler(o);
  } else {
    // cnt === 0
    let disposer: (T) => any = (disposerMap.get(o): any);
    if (disposer != null) disposer(o);
  }
  return cnt;
}
export function refCount<T: interface {}>(o: T): number {
  let cnt = (refCountMap.get(o): any);
  return cnt === undefined ? 0 : cnt;
}
export function isUnique<T: interface {}>(o: T): boolean {
  return refCount(o) === 1;
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class RefList<T: interface {}> {
  _head: ?RefListNode<T>;

  constructor() {
    this._head = null;
  }

  front(): ?T {
    if (this._head != null) return this._head.data;
  }

  pushFront(t: T): RefListNode<T> {
    this._head = new RefListNode(this._head, t);
    return this._head;
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class RefListNode<T: interface {}> {
  next: ?RefListNode<T>;
  data: T;
  constructor(n: ?RefListNode<T>, d: T) {
    console.assert(refCount(d) > 0);
    this.next = n;
    this.data = d;
  }

  releaseTail() {
    let n = this.next;
    while (n != null) {
      release(n.data);
      n = n.next;
    }
    this.next = null;
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class ListNode<T> {
  prev: ?ListNode<T>;
  next: ?ListNode<T>;
  data: T;

  constructor(p: ?ListNode<T>, n: ?ListNode<T>, d: T) {
    this.prev = p;
    this.next = n;
    this.data = d;
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class LinkedList<T> {
  _head: ?ListNode<T>;
  _tail: ?ListNode<T>;
  _size: number;

  constructor() {
    this._head = null;
    this._tail = null;
    this._size = 0;
  }

  insertNode(pos: ?ListNode<T>, n: ListNode<T>): ListNode<T> {
    if (pos == null) {
      n.prev = this._tail;
      n.next = null;
      this._tail = n;
    } else {
      n.prev = pos.prev;
      n.next = pos;
      pos.prev = n;
    }
    if (n.prev == null) this._head = n;
    else n.prev.next = n;

    this._size++;
    return n;
  }
  insert(pos: ?ListNode<T>, v: T): ListNode<T> {
    return this.insertNode(pos, new ListNode(null, null, v));
  }

  pushFront(v: T): ListNode<T> {
    return this.insert(this._head, v);
  }
  pushNodeFront(n: ListNode<T>): ListNode<T> {
    return this.insertNode(this._head, n);
  }

  pushBack(v: T): ListNode<T> {
    return this.insert(null, v);
  }
  pushNodeBack(n: ListNode<T>): ListNode<T> {
    return this.insertNode(null, n);
  }
  removeNode(pos: ListNode<T>): ListNode<T> {
    if (pos.next == null) this._tail = pos.prev;
    else pos.next.prev = pos.prev;

    if (pos.prev == null) this._head = pos.next;
    else pos.prev.next = pos.next;

    this._size--;
    return pos;
  }
  remove(pos: ListNode<T>): T {
    return this.removeNode(pos).data;
  }
  popFront(): ?T {
    return this._head == null ? null : this.remove(this._head);
  }
  popBack(): ?T {
    return this._tail == null ? null : this.remove(this._tail);
  }
  /*:: @@iterator(): Iterator<T> { return ({}: any); } */
  // $FlowFixMe: computed property
  [Symbol.iterator](): Iterator<T> {
    let curr = this._head;

    return {
      next: () => {
        if (curr == null) return { done: true };
        else {
          let r = { value: curr.data, done: false };
          curr = curr.next;
          return r;
        }
      },
    };
  }

  reverseIterator(): Iterator<T> {
    let curr = this._tail;

    return ({
      next: () => {
        if (curr == null) return { done: true };
        else {
          let r = { value: curr.data, done: false };
          curr = curr.prev;
          return r;
        }
      },
    }: any);
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
type PriData<T> = { data: T, priority: number };

export class PriorityList<T> {
  _nodeMap: Map<T, ListNode<PriData<T>>>;
  _list: LinkedList<PriData<T>>;
  _highestPriority: number;
  _lowestPriority: number;

  size(): number {
    return this._list._size;
  }

  constructor() {
    this._nodeMap = new Map();
    this._list = new LinkedList();
    this._highestPriority = -1;
    this._lowestPriority = 0;
  }

  // FIXME: if insert in the middle, may have to assign priorities down (or up) to list
  // For now this is not implemented, as it is unsure whether it will be needed or not
  // insertBefore(t: ?T, u: T) {
  //  let n = t != null ? this._nodeMap.get(t) : null;
  //  this._list.insert(n, u); // inserts as last if t null or not found
  // }

  // Precondition: t is in the priority list
  _access(t: T): ListNode<PriData<T>> {
    let n = this._nodeMap.get(t);
    if (n == null) console.log((t: any).name);
    if (n == null) console.log(this.size());
    hdconsole.assert(n != null, "element not found in priority list");
    if (n == null)
      throw "Should not happen, element not found in priority list"; // this is to appease flow
    return n;
  }

  remove(t: T): void {
    let n = this._nodeMap.get(t);
    if (n != null) {
      // only remove if not already removed
      this._nodeMap.delete(t);
      this._list.remove(n);
    }
  }

  promote(t: T): void {
    let node = this._access(t);
    node.data.priority = ++this._highestPriority;
    this._list.pushNodeFront(this._list.removeNode((node: any)));
  }

  demote(t: T): void {
    let node = this._access(t);
    node.data.priority = --this._lowestPriority;
    this._list.pushNodeBack(this._list.removeNode((node: any)));
  }

  get highestPriority(): number {
    return this._highestPriority;
  }
  get lowestPriority(): number {
    return this._lowestPriority;
  }
  priority(t: T): number {
    return this._access(t).data.priority;
  }

  pushBack(t: T): void {
    this._nodeMap.set(
      t,
      this._list.pushBack({ data: t, priority: --this._lowestPriority })
    );
  }
  pushFront(t: T): void {
    this._nodeMap.set(
      t,
      this._list.pushFront({ data: t, priority: ++this._highestPriority })
    );
  }

  // an iterable that iterates over just the T values, ignores priority value
  entries(): Iterable<T> {
    return map((pd) => pd.data, this);
  }

  /*:: @@iterator(): Iterator<PriData<T>> { return ({}: any); } */
  // $FlowFixMe: computed property
  [Symbol.iterator](): Iterator<PriData<T>> {
    return this._list[Symbol.iterator]();
  }

  hasHigherPriority(a: T, b: T): boolean {
    return this._access(a).data.priority > this._access(b).data.priority;
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class ObservableReference<T> {
  _value: ?T;
  _subject: Obs.Subject<any>; // holds observers
  _subscription: ?Obs.Subscription; // link to another reference

  constructor(value: ?T = null) {
    this._value = value;
    this._subject = new Obs.Subject((o) => {
      o.next(this._value);
    });
    this._subscription = null;
  }

  subscribe(s: any): Obs.Subscription {
    return this._subject.subscribe(s);
  }
  get value(): ?T {
    return this._value;
  }
  set value(v: ?T): void {
    if (this._subscription != null) {
      this._subscription.unsubscribe();
      this._subscription = null;
    }
    this._setValue(v);
  }

  _setValue(v: ?T): void {
    if (this._value !== null) this._subject.sendNext(null);
    this._value = v;
    if (v !== null) this._subject.sendNext(v);
  }
  link(target: ObservableReference<T>): void {
    this.value = null; // unsubscribes, if already linked
    this._subscription = target.subscribe((v: ?T): void => {
      this._setValue(v);
    });
  }
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export function throw_on_null<T>(t: ?T): T {
  if (t == null) throw "Expected non-null";
  return t;
}
export function nonnull_cast<T>(t: ?T): T {
  return ((t: any): T);
}
// ends here
// [[[[file:~/Git/hd4/org/utilities.org::*Tangle][Tangle]]][]]
export class BinaryHeap<T> {
  _cmp: (T, T) => boolean;
  _data: Array<T>;

  constructor(cmp: (T, T) => boolean) {
    this._cmp = cmp;
    this._data = [((null: any): T)]; // empty unused element at 0 allows simpler index math
  }

  size(): number {
    return this._data.length - 1;
  }
  empty(): boolean {
    return this.size() === 0;
  }

  _swap(i: number, j: number): void {
    let tmp = this._data[i];
    this._data[i] = this._data[j];
    this._data[j] = tmp;
  }

  _percolateUp(i: number): void {
    while (i >= 2) {
      let parent = Math.floor(i / 2);
      if (this._cmp(this._data[i], this._data[parent])) {
        this._swap(i, parent);
        i = parent;
      } else break;
    }
  }

  push(t: T): void {
    this._data.push(t);
    this._percolateUp(this.size());
  }

  _percolateDown(i: number): void {
    while (2 * i <= this.size()) {
      let smallerChild = this._minOfSiblings(2 * i);
      if (!this._cmp(this._data[i], this._data[smallerChild])) {
        this._swap(i, smallerChild);
        i = smallerChild;
      } else break;
    }
  }

  _minOfSiblings(i: number): number {
    if (i >= this.size() || this._cmp(this._data[i], this._data[i + 1]))
      return i;
    else return i + 1;
  }

  pop(): T {
    let ret = this._data[1];
    let last = this._data.pop();
    if (this.size() > 0) {
      this._data[1] = last;
      this._percolateDown(1);
    }
    return ret;
  }
}
// ends here
// Tangle:1 ends here
