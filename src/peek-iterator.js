// [[file:../org/peek-iterator.org::*Tangle][Tangle:1]]
// @flow

import * as util from "./utilities";

// [[[[file:~/Git/hd4/org/peek-iterator.org::*Tangle][Tangle]]][]]
export default class PeekIterator<T, CheckpointData> {
  _iter: Iterator<T>;
  _currentIndex: number;
  _firstIndex: number;
  _buffer: Array<T>;
  _checkpoints: Array<{ index: number, value: CheckpointData }>;

  constructor(iter: Iterator<T>) {
    this._iter = iter;
    this._currentIndex = 0;
    this._firstIndex = 0;
    this._buffer = [];
    this._checkpoints = [];
  }

  next(): IteratorResult<T, any> {
    if (this._fetch(1) === 0) return { done: true };
    let r = { value: ((this._at(this._currentIndex++): any): T), done: false };
    this._collect();
    return r;
  }
  _ind2buf(i: number): number {
    return i - this._firstIndex;
  }
  _at(i: number): ?T {
    return this._buffer[this._ind2buf(i)];
  }
  _fetch(n: number): number {
    let currentlyRead = this._buffer.length - this._ind2buf(this._currentIndex);
    let toRead = n - currentlyRead;
    while (toRead > 0) {
      let v = this._iter.next();
      if (v.done) break;
      toRead--;
      this._buffer.push(v.value);
    }
    return n - toRead;
  }
  _collect(): void {
    let toRemove =
      this._checkpoints.length === 0
        ? this._currentIndex - this._firstIndex
        : this._checkpoints[0].index - this._firstIndex;
    this._firstIndex += toRemove;
    this._buffer.splice(0, toRemove);
  }
  checkpoint(d: CheckpointData): void {
    this._checkpoints.push({ index: this._currentIndex, value: d });
  }

  forgetCheckpoint(): void {
    if (this._checkpoints.pop() === undefined) {
      throw "Trying to forget a nonexisting checkpoint in PeekIterator";
    }
    this._collect();
  }

  reset(): CheckpointData {
    if (this._checkpoints.length === 0) {
      throw "Trying to reset to a nonexisting checkpoint in PeekIterator";
    }
    let r = this._checkpoints.pop();
    this._currentIndex = r.index;
    this._collect();
    return r.value;
  }
  peek(): ?T {
    this._fetch(1);
    return this._at(this._currentIndex);
  }

  peekMany(n: number): Array<T> {
    this._fetch(n);
    return this._buffer.slice(
      this._ind2buf(this._currentIndex),
      this._ind2buf(this._currentIndex + n)
    );
  }
}
// ends here
// Tangle:1 ends here
