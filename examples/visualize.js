let cmp;

window.onload = () => {

  cmp = hd.pComponent(hd.hdl`
  component C {
    var a, b, c, d, e;
    constraint c1 {
      m1(a, b -> a) => [b];
      m2(a, b -> b) => [a];
    }
    constraint c2 {
      n1(a, b -> c) => [a+b];
      n2(a, c -> b) => [c-a];
      n3(b, c -> a) => [c-b];
    }
  }
  `).value;
  let cs = new hd.ConstraintSystem();
  cmp.connectSystem(cs);
  cmp.vs.a.set(0);
  
//  let m1 = new hd.Method(2, [0, 1], [0], (a, b) => { return [b]; }, "m1");
//  let m2 = new hd.Method(2, [0, 1], [1], (a, b) => { return [a]; }, "m2");
//  let c1 = new hd.ConstraintSpec([m1, m2]);
// 
//  let n1 = new hd.Method(3, [0, 1], [2], (a, b) => { return [a+b]; }, "n1");
//  let n2 = new hd.Method(3, [0, 2], [1], (a, b) => { return [b-a]; }, "n2");
//  let n3 = new hd.Method(3, [1, 2], [0], (a, b) => { return [b-a]; }, "n3");
//  let c2 = new hd.ConstraintSpec([n1, n2, n3]);
// 
//  let cs = new hd.ConstraintSystem();
// 
//  let a = new hd.Variable(0, "a");
//  let b = new hd.Variable(1, "b");
//  let c = new hd.Variable(2, "c");
//  let d = new hd.Variable(3, "d");
//  let e = new hd.Variable(4, "e");
//  
//  cs.addConstraint(new hd.Constraint(c1, [a, b], "c1"));
//  cs.addConstraint(new hd.Constraint(c2, [b, c, d], "c2"));

  
  let [nodes, edges] = hd.constraintSystemInVisNodesAndEdges(cs);
  
  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: new vis.DataSet(nodes),
    edges: new vis.DataSet(edges)
  };
  var options = {
  };
  
  var network = new vis.Network(container, data, options);

  network.on("selectNode", function(params) {
    if (params.nodes.length == 1) {
      if (network.isCluster(params.nodes[0]) == true) {
        network.openCluster(params.nodes[0]);
      } else {
        let n = data.nodes.get(params.nodes[0]);
        if (n.cid != null) clusterByCid(n.cid);
      }
    }
  });

  function clusterByCid(cid) {
//      network.setData(data);
      var clusterOptionsByData = {
          joinCondition:function(childOptions) {
              return childOptions.cid == cid;
          },
          clusterNodeProperties: {id: cid, label: cid, borderWidth:3, shape:'diamond'}
      };
      network.cluster(clusterOptionsByData);
  }
}
