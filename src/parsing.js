// @flow

import * as lex from "../src/lexing-tools";
type Position = lex.Position;
export type { Position };
import PeekIterator from "../src/peek-iterator";
import { some } from "../src/utilities";
import { hdconsole } from "../src/hdconsole";
export class ParseState {
  _input: PeekIterator<string, lex.Position>;
  pos: lex.Position;
  _context: Array<[string, lex.Position]>;
  _splices: Array<any>;

  constructor(
    input: Iterator<string>,
    splices: Array<any> = [],
    pos: lex.Position = new lex.Position(0, 1, 0)
  ) {
    this._input = new PeekIterator(input);
    this._context = [];
    this._splices = splices;
    this.pos = pos;
  }
  pushContext(str: string): void {
    this._context.push([str, this.pos.clone()]);
  }
  popContext(): void {
    this._context.pop();
  }
  checkpoint(): void {
    this._input.checkpoint(this.pos.clone());
  }
  forgetCheckpoint(): void {
    this._input.forgetCheckpoint();
  }
  resetCheckpoint(): void {
    this.pos = this._input.reset();
  }
  peek(): ?string {
    return this._input.peek();
  }

  advance(): ?string {
    let i = this._input.next();
    if (!i.done) this.pos.advance(i.value);
    return i.value;
  }
  getSplice(i: number): any {
    return this._splices[i];
  }
  parseErrorF(expF: () => Array<string>, msg?: string): ParseError {
    // extract the topmost context, if any
    let ctxName, ctxPos;
    if (this._context.length > 0)
      [ctxName, ctxPos] = this._context[this._context.length - 1];
    this.checkpoint();
    let r = lift(
      (spc, nonspc) => spc + nonspc.join(""),
      manyws,
      many(sat((c) => !lex.isWhitespace(c), ""))
    )(this);
    this.resetCheckpoint();
    let unexpected = "<?>";
    if (r.success) unexpected = r.value;
    return new ParseError(
      this.pos.clone(),
      expF,
      unexpected,
      ctxName,
      ctxPos,
      msg
    );
  }

  parseError(exp: Array<string>, msg?: string): ParseError {
    return this.parseErrorF(() => exp, msg);
  }
}
export function mkParseState(
  s: string,
  splices: Array<string> = []
): ParseState {
  // $FlowFixMe
  return new ParseState(s[Symbol.iterator](), splices);
}
export type ParseSuccess<T> = {|
  success: true,
  value: T,
  consumed: boolean,
  expecting?: () => Array<string>,
|};
export type ParseFailure = {|
  success: false,
  expecting: () => Array<string>,
  consumed: boolean,
|};

export type ParseResult<T> = ParseSuccess<T> | ParseFailure;
export type Parser<T> = (ParseState) => ParseResult<T>;
export type SuccessParser<T> = (ParseState) => ParseSuccess<T>;
export type FailureParser<T> = (ParseState) => ParseFailure;
export type NamedParser<T> = ((ParseState) => ParseResult<T>) & {
  parserName: string,
};
export function ok<T>(t: T, c: boolean = true): ParseSuccess<T> {
  return { success: true, value: t, consumed: c };
}
export function fail(es: Array<string>, c: boolean = false): ParseFailure {
  return failF(() => es, c);
}
export function failF(
  ef: () => Array<string>,
  c: boolean = false
): ParseFailure {
  return { success: false, expecting: ef, consumed: c };
}
export class ParseError extends Error {
  _message: string;

  _pos: lex.Position;
  _expecting: () => Array<string>;
  _unexpected: ?string;
  _ctxName: ?string;
  _ctxPos: ?lex.Position;
  _msg: ?string;

  constructor(
    pos: lex.Position,
    expF: () => Array<string>,
    unexp: ?string,
    ctxName?: string,
    ctxPos?: lex.Position,
    msg?: string
  ) {
    super();
    this._pos = pos;
    this._expecting = expF;
    this._unexpected = unexp;
    this._ctxName = ctxName;
    this._ctxPos = ctxPos;
    this._msg = msg;
  }

  get name(): string {
    return "HotDrink Parse Error";
  }
  set name(s: string): void {
    throw Error("Cannot set the name of ParseError");
  }
  get message(): string {
    let msg = this._pos.toString() + "\n";
    if (this._msg != null) msg += this._msg + "\n";

    let e = this._formatExpecting();
    let u = this._unexpected != null ? this._unexpected : "<eof>";
    if (e !== "") msg += "Expecting " + e + ". ";
    msg += "Unexpected " + u + "\n";
    if (this._ctxName != null && this._ctxPos != null) {
      msg +=
        "\n" +
        "  while parsing " +
        this._ctxName +
        " (" +
        this._ctxPos.toString() +
        ")\n";
    }
    return msg;
  }
  set message(s: string): void {
    throw Error("Cannot set the message of ParseError");
  }

  _formatExpecting(): string {
    let es = this._expecting();
    if (es.length == 1) return es[0];
    if (es.length > 1) return "one of (" + es.join(", ") + ")";
    return "";
  }
}
export function item(ps: ParseState): ParseResult<string> {
  let v = ps.advance();
  if (v == null) return fail(["<item>"]);
  else return ok(v);
}

export function peekItem(ps: ParseState): ParseResult<string> {
  let v = ps.peek();
  if (v == null) return fail(["<peek-item>"]);
  else return ok(v, false);
}
export function failure(ps: ParseState): ParseFailure {
  return fail([]);
}
export function ret<T>(v: T, consumed: boolean = false): SuccessParser<T> {
  return (ps: ParseState) => ok(v, consumed);
}
export function eof(ps: ParseState): ParseResult<null> {
  return peekItem(ps).success ? fail(["<eof>"]) : ok(null, false);
}
export function sat(
  predicate: (string) => boolean,
  name?: string = "<sat: ?>"
): NamedParser<string> {
  let f = (ps: ParseState) => {
    let r = peekItem(ps);
    if (r.success && predicate(r.value)) return item(ps);
    else return fail([name]);
  };
  f.parserName = name;
  return f;
}
export function pChar(c: string): NamedParser<string> {
  return sat((x) => x === c, c);
}
export function pChars(cs: string): NamedParser<string> {
  return sat((x) => some((v) => v === x, cs), `<any of '${cs}'>`);
}
const alphaPlus: NamedParser<string> = sat(lex.isAlphaPlus, "<letter>");
const alphaNum: NamedParser<string> = sat(lex.isAlphaNum, "<letter or digit>");
const digit: NamedParser<string> = sat(lex.isDigit, "<digit>");

export { alphaPlus, alphaNum, digit };
export function many<T>(p: Parser<T>): Parser<Array<T>> {
  return oneOf(many1(p), ret([]));
}
export function many1<T>(p: Parser<T>): Parser<Array<T>> {
  return (ps: ParseState) => {
    return lift(
      (v, vs) => {
        vs.unshift(v);
        return vs;
      },
      p,
      many(p)
    )(ps);
  };
}
export function exactString(s: string): NamedParser<string> {
  let result = (ps: ParseState): ParseResult<string> => {
    ps.checkpoint();
    for (let c of s) {
      let r = item(ps);
      if ((r.success && r.value != c) || !r.success) {
        ps.resetCheckpoint();
        return fail([s]);
      }
    }
    ps.forgetCheckpoint();
    return ok(s, s.length > 0); // consumes if nonzero length
  };
  result.parserName = s;
  return result;
}
const lineComment: Parser<string> = lift(
  (pre, rest, nl) => pre + rest.join("") + nl,
  exactString("//"),
  many(sat((t) => t !== "\n")), // fails at '\n' and also at <eof>
  oneOf(pChar("\n"), pBind_(eof, ret("")))
);

const blockComment: Parser<string> = lift(
  (pre, rest, post) => pre + rest.join("") + post,
  exactString("/*"),
  many(pBind_(pNot(exactString("*/")), item)),
  oneOf(exactString("*/"), pBind_(eof, ret("")))
);

export { lineComment, blockComment };
const onews: Parser<string> = sat(lex.isWhitespace, "<whitespace>");
const manyws: Parser<string> = lift(
  (a) => a.join(""),
  many(oneOf(onews, lineComment, blockComment))
);

export { onews, manyws };
export function pBind<T, U>(p: Parser<T>, f: (T) => Parser<U>): Parser<U> {
  return (ps: ParseState) => {
    const r1: ParseResult<T> = p(ps);
    if (!r1.success) return r1; // p failed, hence fail
    let r2: ParseResult<U> = f(r1.value)(ps);
    if (!r2.consumed) r2.consumed = r1.consumed; // consumed is true if either consumed
    return r2;
  };
}
export function pBind_<T, U>(p: Parser<T>, q: Parser<U>): Parser<U> {
  return pBind(p, (_) => q);
}
export function opt<T, U>(p: Parser<T>, v: U): SuccessParser<T | U> {
  return (ps: ParseState): ParseSuccess<T | U> => {
    ps.checkpoint();
    let r = p(ps);
    if (r.success) {
      ps.forgetCheckpoint();
      return ok(r.value, r.consumed);
    } else {
      ps.resetCheckpoint();
      return ok(v, false);
    }
  };
}
opaque type ignoreTag = {};
const _ignoreTag = ({}: ignoreTag);

export function ignore<T>(p: Parser<T>): Parser<ignoreTag> {
  return lift(() => _ignoreTag, p);
}
export function lift<T>(
  f: (...vs: any) => T,
  ...parsers: Array<Parser<any>>
): Parser<T> {
  return (ps: ParseState) => {
    let rs = [];
    let consumed = false;
    for (let p of parsers) {
      let r = p(ps);
      r.consumed = r.consumed || consumed;
      consumed = r.consumed;
      if (!r.success) return r;
      if (r.value !== _ignoreTag) rs.push(r.value);
    }
    return ok(f(...rs));
  };
}
export function lift2<T1, T2, U>(
  f: (T1, T2) => U
): (Parser<T1>, Parser<T2>) => Parser<U> {
  return (p1: Parser<T1>, p2: Parser<T2>) => lift(f, p1, p2);
}
export function lift3<T1, T2, T3, U>(
  f: (T1, T2, T3) => U
): (Parser<T1>, Parser<T2>, Parser<T3>) => Parser<U> {
  return (p1: Parser<T1>, p2: Parser<T2>, p3: Parser<T3>) =>
    lift(f, p1, p2, p3);
}
export function lift4<T1, T2, T3, T4, U>(
  f: (T1, T2, T3, T4) => U
): (Parser<T1>, Parser<T2>, Parser<T3>, Parser<T4>) => Parser<U> {
  return (p1: Parser<T1>, p2: Parser<T2>, p3: Parser<T3>, p4: Parser<T4>) =>
    lift(f, p1, p2, p3, p4);
}
export function lift5<T1, T2, T3, T4, T5, U>(
  f: (T1, T2, T3, T4, T5) => U
): (Parser<T1>, Parser<T2>, Parser<T3>, Parser<T4>, Parser<T5>) => Parser<U> {
  return (
    p1: Parser<T1>,
    p2: Parser<T2>,
    p3: Parser<T3>,
    p4: Parser<T4>,
    p5: Parser<T5>
  ) => lift(f, p1, p2, p3, p4, p5);
}

export function go<T>(pg: Generator<Parser<any>, T, any>): Parser<T> {
  return function (ps: ParseState) {
    let consumed = false;
    let pr = pg.next();
    while (!pr.done) {
      let r = pr.value(ps);
      r.consumed = r.consumed || consumed;
      consumed = r.consumed;
      if (!r.success) return r;
      pr = pg.next(r.value);
    }

    if (pr.value === undefined) {
      // if this happens, the generator is faulty
      hdconsole.assert(false, "Incorrect yield parser");
      return fail(["<?>"], consumed);
    } else return ok(pr.value, consumed);
  };
}
export function inBetween<T, U, V>(
  p: Parser<T>,
  open: Parser<U>,
  close: Parser<V>
): Parser<T> {
  return go(
    (function* () {
      yield open;
      const v = yield p;
      yield close;
      return v;
    })()
  );
}

export function parens<T>(p: Parser<T>): Parser<T> {
  return inBetween(p, word("("), word(")"));
}
export function oneOf<T>(...qs: Array<Parser<T>>): Parser<T> {
  return (ps: ParseState) => {
    let es: Array<() => Array<string>> = [];
    for (let q: Parser<T> of qs) {
      let r = q(ps);
      if (r.success || r.consumed) return r;
      else es.push(r.expecting);
    }
    // all failed without consuming, concatenate expecting info
    return failF(() => Array.prototype.concat(...es.map((f) => f())));
    // one level flatten (Array.flat() not yet in many browsers)
    // FIXME: remove duplicates
  };
}
export function pNot<T>(p: NamedParser<T>): Parser<null> {
  return (ps: ParseState) => {
    ps.checkpoint();
    let r = p(ps);
    if (!r.success && !r.consumed) {
      ps.resetCheckpoint();
      return ok(null, false);
    }
    let consumed = r.consumed;
    if (r.success) {
      ps.resetCheckpoint();
      consumed = false;
    } else {
      ps.forgetCheckpoint();
    }
    return fail(["not(" + p.parserName + ")"], consumed);
  };
}
export function until<T>(p: NamedParser<T>): Parser<string> {
  return lift((ls) => ls.join(""), many(pBind_(pNot(p), item)));
}
export function notFollowedBy<T, U>(
  q1: NamedParser<T>,
  q2: NamedParser<U>
): Parser<T> {
  return (ps: ParseState): ParseResult<T> => {
    ps.checkpoint();
    let r1 = q1(ps);
    if (!r1.success) {
      ps.forgetCheckpoint();
      return r1;
    }

    let r2 = pNot(q2)(ps);
    if (r2.success) {
      ps.forgetCheckpoint();
      return r1;
    }

    if (r2.consumed) {
      ps.forgetCheckpoint();
      return r2;
    } else {
      ps.resetCheckpoint();
      return fail([q1.parserName + " not followed by " + q2.parserName]);
    }
  };
}
export function pTry<T>(q: NamedParser<T>): Parser<T> {
  return (ps: ParseState): ParseResult<T> => {
    ps.checkpoint();
    let r = q(ps);
    if (r.success) {
      ps.forgetCheckpoint();
      return r;
    }
    ps.resetCheckpoint();
    return fail([q.parserName]);
  };
}
export function must<T>(p: Parser<T>, msg?: string): SuccessParser<T> {
  return (ps: ParseState): ParseSuccess<T> => {
    let r = p(ps);
    if (r.success) return r;
    else throw ps.parseErrorF(r.expecting, msg);
  };
}
export function named<T>(p: Parser<T>, name: string = "<?>"): NamedParser<T> {
  let q = (ps) => {
    let r = p(ps);
    if (!r.success) r.expecting = () => [name];
    return r;
  };
  q.parserName = name;
  return q;
}
export function token<T>(q: Parser<T>): Parser<T> {
  return lift((v, _) => v, q, manyws);
}
export function word(s: string): Parser<string> {
  return token(exactString(s));
}
function keyword(s: string): Parser<string> {
  return named(
    token(notFollowedBy(exactString(s), named(alphaNum, "<alphanum>"))),
    `<keyword: ${s}>`
  );
}
export function identifier(ps: ParseState): ParseResult<string> {
  return lift(
    (h, t, _) => h + t.join(""),
    sat(lex.isAlphaPlus, "<identifier>"),
    many(sat(lex.isAlphaNum)),
    manyws
  )(ps);
}
let nat: Parser<number> = lift(
  (ds) => Number(ds.join("")),
  named(
    token(
      notFollowedBy(
        named(many1(digit), "<nat>"),
        named(alphaPlus, "<alphaplus>")
      )
    ),
    "<nat>"
  )
);

export { nat };
export function sepList<Element, Sep>(
  element: Parser<Element>,
  sep: Parser<Sep>
): Parser<Array<Element>> {
  return (ps: ParseState): ParseResult<Array<Element>> => {
    let r = element(ps);
    if (!r.success) return r.consumed ? r : ok([], false); // if element parser consumed and failed, then fail

    let rs = many(pBind_(sep, element))(ps);
    rs.consumed = r.consumed || rs.consumed;
    if (rs.success) rs.value.unshift(r.value);
    return rs;
  };
}
const currentPos: SuccessParser<lex.Position> = (
  ps: ParseState
): ParseSuccess<lex.Position> => ok(ps.pos.clone(), false);
export { currentPos };
export function context<T>(str: string, p: Parser<T>): Parser<T> {
  return (ps: ParseState) => {
    ps.pushContext(str);
    try {
      return p(ps);
    } finally {
      ps.popContext();
    }
  };
}
export function hdl(strs: Array<string>, ...splices: Array<any>): ParseState {
  let pstr = "";
  for (let i = 0; i < splices.length; ++i) {
    pstr += strs[i];
    pstr += "###" + i + "###";
  }
  if (strs.length > 0) pstr += strs[strs.length - 1];
  let ps = mkParseState(pstr, splices);
  return ps;
}
const pSplice: Parser<any> = (ps: ParseState) =>
  pBind(inBetween(nat, exactString("###"), word("###")), (key) =>
    ret(ps.getSplice(key))
  )(ps);
export { pSplice };
